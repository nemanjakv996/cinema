package klase;



import java.util.*;

/**
 * 
 */
public class User  {
	private int id;
	private String name;
    private String surname;
    private String email;
    private String password;
    private String address;
    private String birthDate;
    private int id_manager;
    private int id_paymentCard;

    private byte[] picture;


    public User() {
    }

    public User(String name, String surname, String email, String password, String address, String birthDate,int id_manager, int id_paymentCard, byte[] picture) {
		super();
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.password = password;
		this.address = address;
		this.birthDate = birthDate;
		this.id_manager=id_manager;
		this.id_paymentCard=id_paymentCard;
		this.picture=picture;
	}

 
	public User(String name, String surname, String email, String password, String address, String birthDate,int id_manager, int id_paymentCard) {
		super();
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.password = password;
		this.address = address;
		this.birthDate = birthDate;
		this.id_manager=id_manager;
		this.id_paymentCard=id_paymentCard;
	}
	
	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	public User(int id, String name, String surname, String email, String password, String address, String birthDate,int id_manager, int id_paymentCard) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.password = password;
		this.address = address;
		this.birthDate = birthDate;
		this.id_manager=id_manager;
		this.id_paymentCard=id_paymentCard;
	}
	

	public User(int id, String name, String surname, String email, String address, int id_manager) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.address = address;
		this.id_manager = id_manager;
	}
	
	public User(int id, String name, String surname, String email, String password, String address) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.password=password;
		this.address = address;
	
	}




	public int getId() {
		return id;
	}




	




	@Override
	public String toString() {
		return  id+" " +  name +" " +surname ;//+"\t"+ email +"\t"+ address+"\n";
				
	}




	public int getId_paymentCard() {
		return id_paymentCard;
	}




	public int getId_manager() {
		return id_manager;
	}




	public String getName() {
		return name;
	}




	public String getSurname() {
		return surname;
	}




	public String getEmail() {
		return email;
	}




	public String getPassword() {
		return password;
	}




	public String getAddress() {
		return address;
	}




	public String getBirthDate() {
		return birthDate;
	}




	public void reserveTicket() {
        // TODO implement here
    }

    /**
     * 
     */
    public void buyATicket() {
        // TODO implement here
    }

    /**
     * @param id 
     * @param name 
     * @param surname 
     * @param email 
     * @param password 
     * @param card
     */
    public void User(int id, String name, String surname, String email, String password, PaymentCard card) {
        // TODO implement here
    }

    /**
     * 
     */
    public void cancelTicket() {
        // TODO implement here
    }

    /**
     * 
     */
    public void viewMovies() {
        // TODO implement here
    }

    /**
     * 
     */
    public void updateInfo() {
        // TODO implement here
    }

}