package klase;


import java.util.*;

/**
 * 
 */
public class Projection {

    /**
     * Default constructor
     */
    public Projection() {
    }
    private int id_hall;
    
    private String name_movie;
    
    public String getName_movie() {
		return name_movie;
	}


	public void setName_movie(String name_movie) {
		this.name_movie = name_movie;
	}


	public String getName_cinema() {
		return name_cinema;
	}


	public void setName_cinema(String name_cinema) {
		this.name_cinema = name_cinema;
	}
	private String name_cinema;

    public Projection(int id, String date_time, String name_movie, String name_cinema) {
		super();
		this.id = id;
		this.date_time = date_time;
		this.name_movie = name_movie;
		this.name_cinema = name_cinema;
	}

    public Projection(int id, int id_hall, boolean active, int id_movie, String date_time) {
		super();
		this.id_hall = id_hall;
		this.id = id;
		this.active = active;
		this.id_movie = id_movie;
		this.date_time = date_time;
	}


	public Projection(int id, boolean active, String date_time, String name_movie, String name_cinema) {
		super();
		this.id = id;
		this.active = active;
		this.date_time = date_time;
		this.name_movie = name_movie;
		this.name_cinema = name_cinema;
	}
    
   	public Projection(int id_hall, boolean active, int id_movie, String date_time) {
		super();
		this.id_hall = id_hall;
		this.active = active;
		this.id_movie = id_movie;
		this.date_time = date_time;
	}

	public int getId_hall() {
		return id_hall;
	}

	public void setId_hall(int id_hall) {
		this.id_hall = id_hall;
	}
	private int id;

    private boolean active;

    private int id_movie;
    
    private String date_time;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getId_movie() {
		return id_movie;
	}

	public void setId_movie(int id_movie) {
		this.id_movie = id_movie;
	}

	public String getDate_time() {
		return date_time;
	}

	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}

	public Projection(boolean active, int id_movie, String date_time) {
		super();
		this.active = active;
		this.id_movie = id_movie;
		this.date_time = date_time;
	}
    
    
}