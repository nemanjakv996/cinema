package klase;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.sql.Date;
import gui.WorkerCinema;

/**
 * 
 */
public class DAO {
    /**
     * Default constructor
     */
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;

	private void connect() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		connect =  (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/cineplex", "root", "");
	}

	
	
    public DAO() {
    }
	
	private void close() {
		try {
			if (resultSet != null) {
				resultSet.close();
			}

			if (statement != null) {
				((java.sql.Statement) statement).close();
			}

			if (connect != null) {
				connect.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    /**
     * @return
     */
	
    public Manager returnManager() {
        // TODO implement here
        return null;
    }
	
	 public int getIdPaymentCard(String password) throws ClassNotFoundException, SQLException {
    	 connect();
 		preparedStatement = connect.prepareStatement("insert into payment_card(status, value, password) values (?,?,?)",statement.RETURN_GENERATED_KEYS);

 		// DOPUNJAVANJE SQL STRINGA, SVAKI ? SE MORA PODESITI 
 		preparedStatement.setBoolean(1, true);
 		preparedStatement.setFloat(2, 1000);
 		preparedStatement.setString(3, password);
 		preparedStatement.execute();
 				
 			
 		resultSet  = preparedStatement.getGeneratedKeys();
 		long id = -1;
 		if (resultSet.next()) {
 		    id = resultSet.getLong(1);
 		    System.out.println("Inserted ID :" + id); // display inserted record
 		}
 		close();
 	
 		return (int) id;
    }

    /**
     * @param worker
     */
    public void passwordUpdateWorker(Worker worker) {
        // TODO implement here
    }

    /**
     * @param userIn 
     * @return
     */
    public ArrayList<User> updateUser(int id, String name, String surname, String email, String address, int id_manager, byte[] imageUser) {
    	ArrayList<User> users = null;
    	try {
    		users=getAllUsers(id_manager);
			connect();
			String query= "UPDATE user SET name=?, surname=?, email=?, address=?, id_manager=?, picture=? where id=?";
			preparedStatement= connect.prepareStatement(query);
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, surname);
			preparedStatement.setString(3, email);
			preparedStatement.setString(4,address);	
			preparedStatement.setInt(5, id_manager);
			preparedStatement.setBytes(6, imageUser);
			preparedStatement.setInt(7, id);
			preparedStatement.executeUpdate();
			close();
			return users;
			}
     catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    	return users;
    }

    /**
     * @param workerIn 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public void addWorker(Worker workerIn) throws ClassNotFoundException, SQLException {
    	 connect();
 		preparedStatement = connect.prepareStatement("insert into worker(name, surname, email, password, salary, workingHours, picture, address, id_manager) values (?,?,?,?,?,?,?,?,?)");

 		// DOPUNJAVANJE SQL STRINGA, SVAKI ? SE MORA PODESITI 
 		preparedStatement.setString(1, workerIn.getName());
 		preparedStatement.setString(2, workerIn.getSurname());
 		preparedStatement.setString(3, workerIn.getEmail());
 		preparedStatement.setString(4,workerIn.getPassword());
 		preparedStatement.setFloat(5,workerIn.getSalary());
 		preparedStatement.setInt(6,workerIn.getWorkingHours());
 		preparedStatement.setBytes(7, workerIn.getMyPicture());
 		preparedStatement.setString(8, workerIn.getAddress());
 		preparedStatement.setInt(9, workerIn.getManagerId());
 		preparedStatement.execute();
 				
 		close();
    }

    /**
     * @param email 
     * @return
     */
    public Manager returnManager(String email) {
        // TODO implement here
        return null;
    }

    /**
     * @param id 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public ArrayList <Worker> deleteWorker(int id, int manager_id) throws ClassNotFoundException, SQLException {
    	connect();
    	preparedStatement = connect.prepareStatement("DELETE FROM worker WHERE id=?");
    	preparedStatement.setInt(1, id);
    	preparedStatement.execute();
    	close();
    	ArrayList<Worker>workers=getAllWorkers(manager_id);
    	return workers;
    }

    public byte[] getUserPicture(int id){
    	byte[] photo=null;
        try {
        connect();
    	String sqlSelect= "Select picture from user where id=?";
		preparedStatement= connect.prepareStatement(sqlSelect);
		preparedStatement.setInt(1, id);
		resultSet= preparedStatement.executeQuery();
		if(resultSet.next()) {
		photo=resultSet.getBytes("picture");
		return photo;
		}
		}
        catch(Exception e) {
			e.printStackTrace();
		}
        return photo;
    }
    
    public byte[] getWorkerPicture(int id){
    	byte[] photo=null;
        try {
        connect();
    	String sqlSelect= "Select picture from worker where id=?";
		preparedStatement= connect.prepareStatement(sqlSelect);
		preparedStatement.setInt(1, id);
		resultSet= preparedStatement.executeQuery();
		if(resultSet.next()) {
		photo=resultSet.getBytes("picture");
		return photo;
		}
		}
        catch(Exception e) {
			e.printStackTrace();
		}
        return photo;
    }
    
    /**
     * @param worker 
     * @return
     */
    public ArrayList <Worker> updateWorker(Worker worker, int id_manager, int id) {
    	ArrayList<Worker> workers = null;
    	try {
    		workers=getAllWorkers(id_manager);
			connect();
			String query= "UPDATE worker SET name=?, surname=?, email=?, address=?, id_manager=?, salary=?, picture=? where id=?";
			preparedStatement= connect.prepareStatement(query);
			preparedStatement.setString(1, worker.getName());
			preparedStatement.setString(2, worker.getSurname());
			preparedStatement.setString(3, worker.getEmail());
			preparedStatement.setString(4, worker.getAddress());	
			preparedStatement.setInt(5, worker.getManagerId());
			preparedStatement.setFloat(6, worker.getSalary());
			preparedStatement.setBytes(7, worker.getMyPicture());
			preparedStatement.setInt(8, id);
			preparedStatement.executeUpdate();
			close();
			return workers;
			}
     catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    	return workers;
    }

    /**
     * @return
     */
    public ArrayList<Worker> getAllWorkers(int manager_id) {
    	ArrayList<Worker> workers = new ArrayList<Worker>();
    	try {
		connect();
		preparedStatement = connect.prepareStatement("Select * from worker where id_manager = ?");

		// DOPUNJAVANJE SQL STRINGA, SVAKI ? SE MORA PODESITI 
		//preparedStatement.setString(1, o.getJmbg());
		preparedStatement.setInt(1, manager_id);
		preparedStatement.execute();
		
		//****POCETAK	AKO UPIT VRACA REZULTAT TREBA SLEDECI DEO 
		resultSet = preparedStatement.getResultSet();
		
		while (resultSet.next()) {
			int id = resultSet.getInt("id");
			String name = resultSet.getString("name");
			String surname = resultSet.getString("surname");
			String email = resultSet.getString("email");
			String password = resultSet.getString("password");
			String address = resultSet.getString("address");
			int id_manager = resultSet.getInt("id_manager");
			float salary=resultSet.getFloat("salary");
			int workingHours=resultSet.getInt("workingHours");
			
			Worker worker = new Worker(id, name, surname, email, password, salary, workingHours, null, address, id_manager);

			workers.add(worker);
		}
		}catch(Exception ex) {
			JOptionPane.showMessageDialog(null, ex);
		}
		//****KRAJ		KRAJ OBRADE ResultSet-a	
		
		close();
		return workers;
    }

    /**
     * @param email 
     * @param password 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public boolean checkEmail(String emailCheck) throws SQLException, ClassNotFoundException {
    ArrayList<String> emails = null;
    boolean desicion = false;
	connect();
	preparedStatement = connect.prepareStatement("Select email from user");

	// DOPUNJAVANJE SQL STRINGA, SVAKI ? SE MORA PODESITI 
	//preparedStatement.setString(1, o.getJmbg());
	preparedStatement.execute();
	
	//****POCETAK	AKO UPIT VRACA REZULTAT TREBA SLEDECI DEO 
	resultSet = preparedStatement.getResultSet();
	
	
	while (resultSet.next()) {
		String email = resultSet.getString("id");
		emails.add(email);
	}
	
	for(String mail:emails) {
		if(mail.equals(emailCheck)) {
			desicion=true;
			return desicion;
		}else {
			desicion=false;
		}
	}
	return desicion;
    }
    
    public Manager loginManager(String email, String password) {
    	Manager manager = null;
    	try {
    		connect();
			String sql= "Select * from manager where email=? and password=?";
			preparedStatement= connect.prepareStatement(sql);
			preparedStatement.setString(1, email);
			preparedStatement.setString(2, password);
			resultSet= preparedStatement.executeQuery();
			if(resultSet.next()) {
				int id=resultSet.getInt("id");
				String name=resultSet.getString("name");
				String surname=resultSet.getString("surname");
				String emailManager=resultSet.getString("email");
				String passwordManager=resultSet.getString("password");
				float salary=resultSet.getFloat("salary");
				int workingHours=resultSet.getInt("workingHours");
				String address=resultSet.getString("address");
				String position=resultSet.getString("position");
				manager=new Manager(id, name, surname, emailManager, passwordManager, position, salary, workingHours, null, address);
				return manager;
			}
			connect.close();
    		}catch(Exception ex) {
    			JOptionPane.showMessageDialog(null, ex);
    		}
			return manager;
    }
    
   

    /**
     * @param id 
     * @return
     */
    public Manager getManager(String email) {
    	Manager manager = null;
    	try {
			connect();
			String sql= "Select * from manager where email=?";
			preparedStatement= connect.prepareStatement(sql);
			preparedStatement.setString(1, email);
			resultSet= preparedStatement.executeQuery();
			if(resultSet.next()) {
			int id=resultSet.getInt("id");
			String name=resultSet.getString("name");
			String surname=resultSet.getString("surname");
			String emailManager=resultSet.getString("email");
			String passwordManager=resultSet.getString("password");
			float salary=resultSet.getFloat("salary");
			int workingHours=resultSet.getInt("workingHours");
			String address=resultSet.getString("address");
			String position=resultSet.getString("position");
			byte[] picture=resultSet.getBytes("picture");
			manager=new Manager(id, name, surname, emailManager, passwordManager, position, salary, workingHours, picture, address);
			return manager;
			}
			close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return null;
    }

    /**
     * @return
     */
    
    
    public Worker getWorker(String email) {
    	
    	try {
			connect();
			String sql= "Select * from worker where email=?";
			preparedStatement= connect.prepareStatement(sql);
			preparedStatement.setString(1, email);
			resultSet= preparedStatement.executeQuery();
			if(resultSet.next()) {
			int id=resultSet.getInt("id");
			String name=resultSet.getString("name");
			String surname=resultSet.getString("surname");
			String emailWorker=resultSet.getString("email");
			String passwordWorker=resultSet.getString("password");
			
			int workingHours=resultSet.getInt("workingHours");
			byte[] picture=resultSet.getBytes("picture");
			Worker worker  = new Worker(id, name, surname, emailWorker, passwordWorker, workingHours,picture);
			return worker;
			}
			close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return null;
    }
    
    public ArrayList <Projection> getAllProjection(String search) throws ClassNotFoundException, SQLException {
    if(search.equals("all")) {
    	connect();
    	ArrayList<Projection> projections = new ArrayList<Projection>();
		String sql= "SELECT p.id, p.date_time, p.active , c.name, m.movieName FROM projection p INNER JOIN cinema c on p.id_hall = c.id INNER JOIN movie m on p.id_movie = m.id";
		preparedStatement= connect.prepareStatement(sql);
		resultSet= preparedStatement.executeQuery();
		while(resultSet.next()) {
		int id=resultSet.getInt("p.id");
		String date_time=resultSet.getString("p.date_time");
		String cinema=resultSet.getString("c.name");
		String movie=resultSet.getString("m.movieName");
		boolean active=resultSet.getBoolean("p.active");
		Projection projection=new Projection(id, date_time, movie, cinema);
		if(active) {
		projections.add(projection);
		}
		}
		close();
        return projections;
    }
    else {
    	connect();
    	ArrayList<Projection> projections = new ArrayList<Projection>();
		String sql= "SELECT p.id, p.date_time, p.active , c.name, m.movieName FROM projection p INNER JOIN cinema c on p.id_hall = c.id INNER JOIN movie m on p.id_movie = m.id where m.movieName=? or c.name=?";
		preparedStatement= connect.prepareStatement(sql);
		preparedStatement.setString(1, search);
		preparedStatement.setString(2, search);
		resultSet= preparedStatement.executeQuery();
		while(resultSet.next()) {
		int id=resultSet.getInt("p.id");
		String date_time=resultSet.getString("p.date_time");
		String cinema=resultSet.getString("c.name");
		String movie=resultSet.getString("m.movieName");
		boolean active=resultSet.getBoolean("p.active");
		Projection projection=new Projection(id, date_time, movie, cinema);
		if(active) {
		projections.add(projection);
		}
		}
		close();
		return projections;
    }
    }
    
    public ArrayList <Projection> getAllProjection() throws ClassNotFoundException, SQLException {
        
        	connect();
        	ArrayList<Projection> projections = new ArrayList<Projection>();
    		String sql= "SELECT p.id, p.date_time, p.active , c.name, m.movieName FROM projection p INNER JOIN cinema c on p.id_hall = c.id INNER JOIN movie m on p.id_movie = m.id";
    		preparedStatement= connect.prepareStatement(sql);
    		resultSet= preparedStatement.executeQuery();
    		while(resultSet.next()) {
    		int id=resultSet.getInt("p.id");
    		String date_time=resultSet.getString("p.date_time");
    		String cinema=resultSet.getString("c.name");
    		String movie=resultSet.getString("m.movieName");
    		boolean active=resultSet.getBoolean("p.active");
    		Projection projection=new Projection(id, active, date_time, movie, cinema);
    		projections.add(projection);
    		}
    		close();
            return projections;
        }
    
    public ArrayList <Projection> searchAllProjectionId(int id) throws ClassNotFoundException, SQLException {
        	connect();
        	ArrayList<Projection> projections = new ArrayList<Projection>();
    		String sql= "SELECT p.id, p.date_time, p.active , c.name, m.movieName FROM projection p INNER JOIN cinema c on p.id_hall = c.id INNER JOIN movie m on p.id_movie = m.id where p.id=?";
    		preparedStatement= connect.prepareStatement(sql);
    		preparedStatement.setInt(1, id);
    		resultSet= preparedStatement.executeQuery();
    		while(resultSet.next()) {
    		int idRet=resultSet.getInt("p.id");
    		String date_time=resultSet.getString("p.date_time");
    		String cinema=resultSet.getString("c.name");
    		String movie=resultSet.getString("m.movieName");
    		boolean active=resultSet.getBoolean("p.active");
    		Projection projection=new Projection(idRet, active, date_time, movie, cinema);
    		projections.add(projection);
    		}
    		close();
    		return projections;
        }

    /**
     * @return
     */
    public List <User> allRegistrations() {
        // TODO implement here
        return null;
    }

    /**
     * @param hallIn 
     * @return
     */
    public Hall addHall(Hall hallIn) {
        // TODO implement here
        return null;
    }

    /**
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public ArrayList<Hall> getAllHalls(String search) throws ClassNotFoundException, SQLException {
    	ArrayList<Hall> halls = new ArrayList<Hall>();

		connect();
		if (search.equals("all")) {
		preparedStatement = connect.prepareStatement("Select * from cinema");

		// DOPUNJAVANJE SQL STRINGA, SVAKI ? SE MORA PODESITI 
		//preparedStatement.setString(1, o.getJmbg());
		preparedStatement.execute();
		
		//****POCETAK	AKO UPIT VRACA REZULTAT TREBA SLEDECI DEO 
		resultSet = preparedStatement.getResultSet();
		
		while (resultSet.next()) {
			int id = resultSet.getInt("id");
			String name=resultSet.getString("name");
			boolean taken=resultSet.getBoolean("taken");
			int id_cinema=resultSet.getInt("id_cinema");

			
			Hall hall=new Hall(id, name, taken, id_cinema);
			halls.add(hall);
		}
    }else {
    	preparedStatement = connect.prepareStatement("Select * from cinema where name= ?");

		// DOPUNJAVANJE SQL STRINGA, SVAKI ? SE MORA PODESITI 
		//preparedStatement.setString(1, o.getJmbg());
    	preparedStatement.setString(1, search);
		preparedStatement.execute();
		
		//****POCETAK	AKO UPIT VRACA REZULTAT TREBA SLEDECI DEO 
		resultSet = preparedStatement.getResultSet();
		
		while (resultSet.next()) {
			int id = resultSet.getInt("id");
			String name=resultSet.getString("name");
			boolean taken=resultSet.getBoolean("active");
			int id_cinema=resultSet.getInt("id_cinema");

			
			Hall hall=new Hall(id, name, taken, id_cinema);

			

			halls.add(hall);
    }
    }
		close();
		return halls;
    }
    /**
     * @param hall 
     * @return
     */
    public List <Hall> updateHall(Hall hall) {
        // TODO implement here
        return null;
    }

    /**
     * @param managerIn 
     * @return
     */
    public Manager updateManagerProfile(int id, String name, String surname, String email, String password, String address, byte[] picture) {
    Manager manager =  null;
    	try {
			connect();
			String sql= "Update manager set name=? , surname=? , email=? , password= ?, picture=? where id=?";
			preparedStatement= connect.prepareStatement(sql);
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, surname);
			preparedStatement.setString(3, email);
			preparedStatement.setString(4, password);
			preparedStatement.setBytes(5, picture);
			preparedStatement.setInt(6, id);
			preparedStatement.executeUpdate();
			
			
			String sqlSelect= "Select * from manager where id=?";
			preparedStatement= connect.prepareStatement(sqlSelect);
			preparedStatement.setInt(1, id);
			resultSet= preparedStatement.executeQuery();
			if(resultSet.next()) {
			int idReturn=resultSet.getInt("id");
			String nameReturn=resultSet.getString("name");
			String surnameReturn=resultSet.getString("surname");
			String emailManagerReturn=resultSet.getString("email");
			String passwordManagerReturn=resultSet.getString("password");
			float salaryReturn=resultSet.getFloat("salary");
			int workingHoursReturn=resultSet.getInt("workingHours");
			String addressReturn=resultSet.getString("address");
			String positionReturn=resultSet.getString("position");
			manager=new Manager(id, name, surname, emailManagerReturn, passwordManagerReturn, positionReturn, salaryReturn, workingHoursReturn, null, address);
			return manager;
			}
			close();
    } catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    	 return null;
    	}

    /**
     * @return
     */
    public List <Projection> getActiveProjections() {
        // TODO implement here
        return null;
    }

    /**
     * @param id 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public User getUserId(int id) throws SQLException, ClassNotFoundException {
    	connect();
    
    	String sqlSelect= "Select * from user where id=?";
		preparedStatement= connect.prepareStatement(sqlSelect);
		preparedStatement.setInt(1, id);
		resultSet= preparedStatement.executeQuery();
		if(resultSet.next()) {
		int userId=resultSet.getInt("id");
		String name=resultSet.getString("name");
		String surname=resultSet.getString("surname");
		String email=resultSet.getString("email");
		String password=resultSet.getString("password");
		String address=resultSet.getString("address");
		User u = new User(userId, name, surname, email, password, address);
		return u;
		}
		
		close();
		return null;
		
    }

    /**
     * @param currentUserId 
     * @return
     */
    public List <Ticket> getUserTicketList(int currentUserId) {
        // TODO implement here
        return null;
    }

    /**
     * @param seatIn 
     * @return
     */
    public Seat buy(Seat seatIn) {
        // TODO implement here
        return null;
    }

    /**
     * @param seat
     */
    public void setTaken(Seat seat) {
        // TODO implement here
    }

    /**
     * @param email 
     * @return
     */
    public Worker returnCashier(String email) {
        // TODO implement here
        return null;
    }

    /**
     * @param email 
     * @param password 
     * @return
     */
    public Worker loginWorker(String email, String password, JFrame frame) {
    	Worker worker = null;
    	try {
    		connect();
			String sql= "Select * from worker where email=? and password=?";
			preparedStatement= connect.prepareStatement(sql);
			preparedStatement.setString(1, email);
			preparedStatement.setString(2, password);
			resultSet= preparedStatement.executeQuery();
			if(resultSet.next()) {
				int id=resultSet.getInt("id");
				String name=resultSet.getString("name");
				String surname=resultSet.getString("surname");
				String emailWorker=resultSet.getString("email");
				String passwordWorker=resultSet.getString("password");
				float salary=resultSet.getFloat("salary");
				int workingHours=resultSet.getInt("workingHours");
				String address=resultSet.getString("address");
				int managerId=resultSet.getInt("id_manager");
				worker=new Worker(id, name, surname, emailWorker, passwordWorker, salary, workingHours, null, address, managerId);
				return worker;
			}
			close();
    		}catch(Exception ex) {
    			JOptionPane.showMessageDialog(null, ex);
    		}
			return worker;
    		}

    /**
     * @param manager
     */
    public void passwordUpdateManager(Manager manager) {
        // TODO implement here
    }

    /**
     * @param id 
     * @return
     */
    public Worker getCashier(String email) {
    	
    	try {
			connect();
			String sql= "Select * from worker where email=?";
			preparedStatement= connect.prepareStatement(sql);
			preparedStatement.setString(1, email);
			resultSet= preparedStatement.executeQuery();
			if(resultSet.next()) {
			int id=resultSet.getInt("id");
			String name=resultSet.getString("name");
			String surname=resultSet.getString("surname");
			String workerEmail=resultSet.getString("email");
			String password=resultSet.getString("password");
		//	float salary=resultSet.getFloat("salary");
			int workingHours=resultSet.getInt("workingHours");
			//String address=resultSet.getString("address");
			//String position=resultSet.getString("position");
			Worker w = new Worker(id, name, surname, workerEmail, password, workingHours);
			return (Worker) w;
			}
			close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return null;
    }

    /**
     * @param image_worker 
     * @param workerIn 
     * @return
     */
    public Worker updateWorkerProfile(int id, String name, String surname, String email, String password, byte[] image_worker) {
    	 
     	try {
 			connect();
 			String sql= "Update worker set  name=? , surname=? , email=? , password= ? , picture= ? where id=?";
 			preparedStatement= connect.prepareStatement(sql);
 			
 			preparedStatement.setString(1, name);
 			preparedStatement.setString(2, surname);
 			preparedStatement.setString(3, email);
 			preparedStatement.setString(4, password);
 			preparedStatement.setBytes(5, image_worker);
 			preparedStatement.setInt(6, id);
 			preparedStatement.executeUpdate();
 			
 			
 			String sqlSelect= "Select * from worker where id=?";
 			preparedStatement= connect.prepareStatement(sqlSelect);
 			preparedStatement.setInt(1, id);
 			resultSet= preparedStatement.executeQuery();
 			if(resultSet.next()) {
 			int idReturn=resultSet.getInt("id");
 			String nameReturn=resultSet.getString("name");
 			String surnameReturn=resultSet.getString("surname");
 			String emailWorkerRet=resultSet.getString("email");
 			String passwordWorkerRet=resultSet.getString("password");
 			//float salaryReturn=resultSet.getFloat("salary");
 			int workingHoursReturn=resultSet.getInt("workingHours");
 			//String addressReturn=resultSet.getString("address");
 			//String positionReturn=resultSet.getString("position");
 			Worker worker =  new Worker(idReturn, nameReturn, surnameReturn, emailWorkerRet, passwordWorkerRet);
 			
 			
 			return worker;
 			}
 			close();
     } catch (ClassNotFoundException e) {
 		// TODO Auto-generated catch block
 		e.printStackTrace();
 	} catch (SQLException e) {
 		// TODO Auto-generated catch block
 		e.printStackTrace();
 	}
     	 return null;
     	}
    

   
    /**
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public ArrayList <User> getAllUsers(int manager_id) throws SQLException, ClassNotFoundException {
    	// POMOCNE PROMENLJIVE ZA KONKRETNU METODU
    			ArrayList<User> lista = new ArrayList<User>();

    			connect();
    			preparedStatement = connect.prepareStatement("Select * from user where id_manager = ?");

    			// DOPUNJAVANJE SQL STRINGA, SVAKI ? SE MORA PODESITI 
    			//preparedStatement.setString(1, o.getJmbg());
    			preparedStatement.setInt(1, manager_id);
    			preparedStatement.execute();
    			
    			//****POCETAK	AKO UPIT VRACA REZULTAT TREBA SLEDECI DEO 
    			resultSet = preparedStatement.getResultSet();
    			
    			while (resultSet.next()) {
    				int id = resultSet.getInt("id");
    				String name = resultSet.getString("name");
    				String surname = resultSet.getString("surname");
    				String email = resultSet.getString("email");
    				String password = resultSet.getString("password");
    				String address = resultSet.getString("address");
    				String birthDay = resultSet.getString("birthDay");
    				int id_manager = resultSet.getInt("id_manager");
    				int id_paymentCard = resultSet.getInt("id_paymentCard");

    				
    				User u = new User(id, name, surname, email, password, address, birthDay, id_manager, id_paymentCard);

    				

    				lista.add(u);
    			}
    			//****KRAJ		KRAJ OBRADE ResultSet-a	
    			
    			close();
    			return lista;
    }
    
    public ArrayList <User> getAllUsers() throws SQLException, ClassNotFoundException {
    	// POMOCNE PROMENLJIVE ZA KONKRETNU METODU
    			ArrayList<User> lista = new ArrayList<User>();

    			connect();
    			preparedStatement = connect.prepareStatement("Select * from user");

    			// DOPUNJAVANJE SQL STRINGA, SVAKI ? SE MORA PODESITI 
    			//preparedStatement.setString(1, o.getJmbg());
    			preparedStatement.execute();
    			
    			//****POCETAK	AKO UPIT VRACA REZULTAT TREBA SLEDECI DEO 
    			resultSet = preparedStatement.getResultSet();
    			
    			while (resultSet.next()) {
    				int id = resultSet.getInt("id");
    				String name = resultSet.getString("name");
    				String surname = resultSet.getString("surname");
    				String email = resultSet.getString("email");
    				String password = resultSet.getString("password");
    				String address = resultSet.getString("address");
    				String birthDay = resultSet.getString("birthDay");
    				int id_manager = resultSet.getInt("id_manager");
    				int id_paymentCard = resultSet.getInt("id_paymentCard");

    				
    				User u = new User(id, name, surname, email, password, address, birthDay, id_manager, id_paymentCard);

    				

    				lista.add(u);
    			}
    			//****KRAJ		KRAJ OBRADE ResultSet-a	
    			
    			close();
    			return lista;
    }
    
    public ArrayList <User> getAllUsers(String search) throws SQLException, ClassNotFoundException {
    			ArrayList<User> lista = new ArrayList<User>();
    			connect();
    			if (search.equals("all")) {
    			preparedStatement = connect.prepareStatement("Select * from user ");
    			preparedStatement.execute();	   			
    			resultSet = preparedStatement.getResultSet();
    			
    			while (resultSet.next()) {
    				int id = resultSet.getInt("id");
    				String name = resultSet.getString("name");
    				String surname = resultSet.getString("surname");
    				String email = resultSet.getString("email");
    				String password = resultSet.getString("password");
    				String address = resultSet.getString("address");
    				String birthDay = resultSet.getString("birthDay");
    				int id_manager = resultSet.getInt("id_manager");
    				int id_paymentCard = resultSet.getInt("id_paymentCard");

    				
    				User u = new User(id, name, surname, email, password, address, birthDay, id_manager, id_paymentCard);

    				

    					lista.add(u);
    				}
    			}else{
				
        			
					preparedStatement = connect.prepareStatement("Select * from user where name=?");
					preparedStatement.setString(1, search);
	    			preparedStatement.execute();	   			
	    			resultSet = preparedStatement.getResultSet();
	    			
	    			while (resultSet.next()) {
	    				int id = resultSet.getInt("id");
	    				String name = resultSet.getString("name");
	    				String surname = resultSet.getString("surname");
	    				String email = resultSet.getString("email");
	    				String password = resultSet.getString("password");
	    				String address = resultSet.getString("address");
	    				String birthDay = resultSet.getString("birthDay");
	    				int id_manager = resultSet.getInt("id_manager");
	    				int id_paymentCard = resultSet.getInt("id_paymentCard");

	    				
	    				User u = new User(id, name, surname, email, password, address, birthDay, id_manager, id_paymentCard);

	    				

	    				lista.add(u);
        			}
    				}
    			
    			close();
    			return lista;
    }
    
    public ArrayList <User> searchAllUsers(int id) throws SQLException, ClassNotFoundException {
			ArrayList<User> lista = new ArrayList<User>();
			connect();
			preparedStatement = connect.prepareStatement("Select * from user where id=?");
			preparedStatement.setInt(1, id);
			preparedStatement.execute();	   			
			resultSet = preparedStatement.getResultSet();
			
			while (resultSet.next()) {
				int idRet = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String surname = resultSet.getString("surname");
				String email = resultSet.getString("email");
				String password = resultSet.getString("password");
				String address = resultSet.getString("address");
				String birthDay = resultSet.getString("birthDay");
				int id_manager = resultSet.getInt("id_manager");
				int id_paymentCard = resultSet.getInt("id_paymentCard");

				
				User u = new User(idRet, name, surname, email, password, address, birthDay, id_manager, id_paymentCard);

				

				lista.add(u);
			}
		
		close();
		return lista;
}
    
    public ArrayList <Worker> getAllWorkers(String search) throws SQLException, ClassNotFoundException {
		ArrayList<Worker> workers = new ArrayList<Worker>();
		connect();
		if (search.equals("all")) {
		preparedStatement = connect.prepareStatement("Select * from worker ");
		preparedStatement.execute();	   			
		resultSet = preparedStatement.getResultSet();
		
		while (resultSet.next()) {
			int id = resultSet.getInt("id");
			String name = resultSet.getString("name");
			String surname = resultSet.getString("surname");
			String email = resultSet.getString("email");
			String password = resultSet.getString("password");
			float salary = resultSet.getFloat("salary");
			int workingHours = resultSet.getInt("workingHours");
			String address = resultSet.getString("address");
			int id_manager = resultSet.getInt("id_manager");
			

			
			Worker worker = new Worker(id, name, surname, email, password, salary, workingHours,null, address, id_manager);

			

				workers.add(worker);
			}
		}else{
		
			
			preparedStatement = connect.prepareStatement("Select * from worker where name=?");
			preparedStatement.setString(1, search);
			preparedStatement.execute();	   			
			resultSet = preparedStatement.getResultSet();
			
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String surname = resultSet.getString("surname");
				String email = resultSet.getString("email");
				String password = resultSet.getString("password");
				float salary = resultSet.getFloat("salary");
				int workingHours = resultSet.getInt("workingHours");
				String address = resultSet.getString("address");
				int id_manager = resultSet.getInt("id_manager");
				

				
				Worker worker = new Worker(id, name, surname, email, password, salary, workingHours,null, address, id_manager);

				

				workers.add(worker);
			}
			}
		
		close();
		return workers;
}
    
    public ArrayList <Worker> searchAllWorkersId(int id) throws SQLException, ClassNotFoundException {
			ArrayList<Worker> workers = new ArrayList<Worker>();
			connect();
			preparedStatement = connect.prepareStatement("Select * from worker where id=?");
			preparedStatement.setInt(1, id);
			preparedStatement.execute();	   			
			resultSet = preparedStatement.getResultSet();
			
			while (resultSet.next()) {
				int idRet = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String surname = resultSet.getString("surname");
				String email = resultSet.getString("email");
				String password = resultSet.getString("password");
				float salary = resultSet.getFloat("salary");
				int workingHours = resultSet.getInt("workingHours");
				String address = resultSet.getString("address");
				int id_manager = resultSet.getInt("id_manager");
				

				
				Worker worker = new Worker(idRet, name, surname, email, password, salary, workingHours,null, address, id_manager);

				

				workers.add(worker);
			}
		
		close();
		return workers;
}
    
    public ArrayList <Movie> getAllMovies(String search) throws SQLException, ClassNotFoundException {
    	
    			ArrayList<Movie> lista = new ArrayList<Movie>();
    			connect();
    			if (search.equals("all")) {
    				preparedStatement = connect.prepareStatement("Select * from movie ");
        			preparedStatement.execute();
        			resultSet = preparedStatement.getResultSet();
        	 			while (resultSet.next()) {
        				String name = resultSet.getString("movieName");
        				String genre = resultSet.getString("genre");
        				String duration = resultSet.getString("duration");
        				int id=resultSet.getInt("id");
        				Movie m = new Movie(id, name, genre, duration);
        				
        				lista.add(m);
        			}
					
				}else
				{
    			
    			preparedStatement = connect.prepareStatement("Select * from movie where movieName=?");
    			preparedStatement.setString(1, search);
    			preparedStatement.execute();
    			resultSet = preparedStatement.getResultSet();
    	 			while (resultSet.next()) {
    				String name = resultSet.getString("movieName");
    				String genre = resultSet.getString("genre");
    				String duration = resultSet.getString("duration");
    				int id=resultSet.getInt("id");
    				Movie m = new Movie(id, name, genre, duration);
    				
    				lista.add(m);
    			}
				}
    		
    			close();
    			return lista;
    }
    
    public ArrayList <Cinema> getAllCinema(String search) throws SQLException, ClassNotFoundException {
    	
		ArrayList<Cinema> lista = new ArrayList<Cinema>();
		connect();
		if (search.equals("all")) {
			preparedStatement = connect.prepareStatement("Select * from cinema");
			preparedStatement.execute();
			resultSet = preparedStatement.getResultSet();
	 			while (resultSet.next()) {
	 			int id=resultSet.getInt("id");
				String name = resultSet.getString("name");
				String location = resultSet.getString("location");
				String city = resultSet.getString("city");
				
				Cinema c = new Cinema(id, name, location, city);
				
				lista.add(c);
			}
			
		}else
		{
		
		preparedStatement = connect.prepareStatement("Select * from cinema where name=? or city=?");
		preparedStatement.setString(1, search);
		preparedStatement.setString(2, search);
		preparedStatement.execute();
		resultSet = preparedStatement.getResultSet();
 			while (resultSet.next()) {
 				int id=resultSet.getInt("id");
 				String name = resultSet.getString("name");
				String location = resultSet.getString("location");
				String city = resultSet.getString("city");
				
				Cinema c = new Cinema(name, location, city);
			
			lista.add(c);
		}
		}
	
		close();
		return lista;
}

    
    public int getMovieId(String movieName) throws SQLException, ClassNotFoundException {
    	connect();
    	preparedStatement = connect.prepareStatement("Select id from movie where movieName=?");
		preparedStatement.setString(1, movieName);
		preparedStatement.execute();
		resultSet = preparedStatement.getResultSet();
 			while (resultSet.next()) {
 				int id=resultSet.getInt("id");
 				return id;
    }
 			return 0;
    }
    
    public int getHallId(String hallName) throws SQLException, ClassNotFoundException {
    	connect();
    	preparedStatement = connect.prepareStatement("Select id from cinema where name=?");
		preparedStatement.setString(1, hallName);
		preparedStatement.execute();
		resultSet = preparedStatement.getResultSet();
 			while (resultSet.next()) {
 				int id=resultSet.getInt("id");
 				return id;
    }
 			return 0;
    }
    /**
     * @param projection 
     * @return
     */
    public int[][] getSeatsStatus(Projection projection) {
        // TODO implement here
        return null;
    }

    /**
     * @param ticketIn 
     * @return
     */
    public Ticket printTicket(Ticket ticketIn) {
        // TODO implement here
        return null;
    }

    /**
     * @param paymentIn 
     * @return
     */
    public MakePayment paymentInfo(MakePayment paymentIn) {
        // TODO implement here
        return null;
    }

    /**
     * @param projectionInactive 
     * @return
     */
    public Projection setInactive(Projection projectionInactive) {
        // TODO implement here
        return null;
    }

    /**
     * @param user 
     * @return
     */
    public int calculateDiscount(User user) {
        // TODO implement here
        return 0;
    }

    /**
     * @param email 
     * @param user
     */
    public void returnUser(String email, User user) {
        // TODO implement here
    }

    /**
     * @param user
     */
    public void passwordUpdateUser(User user) {
        // TODO implement here
    }

    /**
     * @param email 
     * @param password 
     * @return
     */
    public User loginUser(String email, String password) {
        // TODO implement here
        return null;
    }

    /**
     * @param user 
     * @return
     */
    public List<Ticket> getUserTicketList(User user) {
        // TODO implement here
        return null;
    }

    /**
     * @param user 
     * @return
     */
    public List <Projection> getUserProjectionList(User user) {
        // TODO implement here
        return null;
    }

    /**
     * @param user 
     * @param projection
     */
    public void like(User user, Projection projection) {
        // TODO implement here
    }

    /**
     * @param user 
     * @param commentText 
     * @param dateTime 
     * @return
     */
    public String addComment(User user, String commentText, Date dateTime) {
        // TODO implement here
        return "";
    }

    /**
     * @param userIn 
     * @return
     */
    public User updateUserProfile(User userIn) {
        // TODO implement here
        return null;
    }

    /**
     * @param userIn 
     * @return
     */
    public User sendRegistrationRequest(User userIn) {
        // TODO implement here
        return null;
    }

    /**
     * @param userIn 
     * @return
     */
    /**
     * @param userIn 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    
    
    
    
    public void addUser(User userIn) throws ClassNotFoundException, SQLException {
    	connect();
		preparedStatement = connect.prepareStatement("insert into user(name, surname, email, password, address, birthDay, id_manager,id_paymentCard, picture) values (?,?,?,?,?,?,?,?,?)");

		// DOPUNJAVANJE SQL STRINGA, SVAKI ? SE MORA PODESITI 
		preparedStatement.setString(1, userIn.getName());
		preparedStatement.setString(2, userIn.getSurname());
		preparedStatement.setString(3, userIn.getEmail());
		preparedStatement.setString(4,userIn.getPassword());
		preparedStatement.setString(5,userIn.getAddress());
		preparedStatement.setString(6,userIn.getBirthDate());
		preparedStatement.setInt(7, userIn.getId_manager());
		preparedStatement.setInt(8, userIn.getId_paymentCard());	
		preparedStatement.setBytes(9, userIn.getPicture());
		preparedStatement.execute();
				
		close();
    }

    /**
     * @param id 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public ArrayList <User> deleteUser(int id, int manager_id) throws ClassNotFoundException, SQLException {
    	connect();
    	preparedStatement = connect.prepareStatement("DELETE FROM user WHERE id=?");
    	preparedStatement.setInt(1, id);
    	preparedStatement.execute();
    	close();
    	ArrayList<User>users=getAllUsers(manager_id);
    	return users;
    }
    
   
    /**
     * @param user 
     * @return
     */
    public List <User> updateUserRetList(User user) {
        // TODO implement here
        return null;
    }

    /**
     * @param user 
     * @param amount 
     * @param ticket 
     * @return
     */
    public boolean approveCancelation(User user, float amount, Ticket ticket) {
        // TODO implement here
        return false;
    }

    /**
     * @param user 
     * @param projection
     */
    public void dislike(User user, Projection projection) {
        // TODO implement here
    }

    /**
     * @param projecionIn 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public Projection addProjection(Projection projectionIn) throws SQLException, ClassNotFoundException {
    	connect();
		preparedStatement = connect.prepareStatement("insert into projection(active, date_time, id_movie, id_hall) values (?,?,?,?)");

		// DOPUNJAVANJE SQL STRINGA, SVAKI ? SE MORA PODESITI 
		preparedStatement.setBoolean(1, projectionIn.isActive());
		preparedStatement.setString(2, projectionIn.getDate_time());
		preparedStatement.setInt(3, projectionIn.getId_movie());
		preparedStatement.setInt(4, projectionIn.getId_hall());
		preparedStatement.execute();
				
		close();
		return projectionIn;
    }

    /**
     * @param id 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public ArrayList <Projection> deleteProjection(int id) throws ClassNotFoundException, SQLException {
    	connect();
    	preparedStatement = connect.prepareStatement("DELETE FROM projection WHERE id=?");
    	preparedStatement.setInt(1, id);
    	preparedStatement.execute();
    	close();
    	ArrayList<Projection> projections=getAllProjection("all");
    	return projections;
    }

    /**
     * @param projection 
     * @return
     */
    public ArrayList <Projection> updateProjection(Projection projection) {
    	ArrayList<Projection> projections = null;
    	try {
			connect();
			String query= "UPDATE projection SET active=?, date_time=?, id_movie=?, id_hall=? where id=?";
			preparedStatement= connect.prepareStatement(query);
			preparedStatement.setBoolean(1, projection.isActive());
			preparedStatement.setString(2, projection.getDate_time());
			preparedStatement.setInt(3, projection.getId_movie());
			preparedStatement.setInt(4, projection.getId_hall());
			preparedStatement.setInt(5, projection.getId());
			preparedStatement.executeUpdate();
			close();
			projections=getAllProjection();
			return projections;
			}
     catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    	return projections;
    }



	public ArrayList<Integer> getAllSeats(int selectedRow) throws ClassNotFoundException, SQLException {
		
		ArrayList<Integer> myList = new ArrayList<Integer>();
    	try {
		connect();
		preparedStatement = connect.prepareStatement("SELECT tiket.id_seat from tiket where tiket.id_projection=?");
		preparedStatement.setInt(1, selectedRow);
		preparedStatement.execute();
	
		resultSet = preparedStatement.getResultSet();
		
		while (resultSet.next()) {
			int id = resultSet.getInt("tiket.id_seat");
			myList.add(id);
		
		}
		}catch(Exception ex) {
			JOptionPane.showMessageDialog(null, ex);
		}
		//****KRAJ		KRAJ OBRADE ResultSet-a	
		
		close();
		return myList;
	
		
	}

	public void addMovie(Movie movie,int id_manager) throws ClassNotFoundException, SQLException {
		 connect();
	 		preparedStatement = connect.prepareStatement("insert into movie(movieName,genre,duration,id_manager) values (?,?,?,?)");

	 		// DOPUNJAVANJE SQL STRINGA, SVAKI ? SE MORA PODESITI 
	 		preparedStatement.setString(1, movie.getMovieName());
	 		preparedStatement.setString(2, movie.getGenre());
	 		preparedStatement.setString(3, movie.getDuration()+" min");
	 		preparedStatement.setInt(4, id_manager);
	 		preparedStatement.execute();
	 				
	 		close();
	}
	public void updateMovie(Movie movie,int id_manager) throws ClassNotFoundException, SQLException{
		try {
			connect();
			String query= "UPDATE movie SET movieName=?, genre=?, duration=?, id_manager=? where id=?";
			preparedStatement= connect.prepareStatement(query);
			preparedStatement.setString(1, movie.getMovieName());
			preparedStatement.setString(2, movie.getGenre());
			preparedStatement.setString(3, movie.getDuration()+" min");
			preparedStatement.setInt(4, id_manager);
			preparedStatement.setInt(5, movie.getId());
			preparedStatement.executeUpdate();
			close();
			}
     catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    }
	
	public Projection getProjection(int selectedRow) {
		
		try {
			connect();
			String sql= "SELECT p.id, p.date_time, p.active , c.name, m.movieName FROM projection p INNER JOIN cinema c on p.id_hall = c.id INNER JOIN movie m on p.id_movie = m.id where p.id=?";			preparedStatement= connect.prepareStatement(sql);
			preparedStatement.setInt(1, selectedRow);
			resultSet= preparedStatement.executeQuery();
			if(resultSet.next()) {
				int id=resultSet.getInt("p.id");
				String date_time=resultSet.getString("p.date_time");
				String cinema=resultSet.getString("c.name");
				String movie=resultSet.getString("m.movieName");
				boolean active=resultSet.getBoolean("p.active");
				Projection projection=new Projection(id, date_time, movie, cinema);
				return projection;
			}
			close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return null;
	}
	
	public void addTicket(Ticket t) throws SQLException, ClassNotFoundException {
		connect();
		preparedStatement = connect.prepareStatement("insert into tiket(price, id_user, id_projection, id_movie, id_seat) values (?,?,?,?,?)");

		// DOPUNJAVANJE SQL STRINGA, SVAKI ? SE MORA PODESITI 
		preparedStatement.setInt(1, t.getPrice());
		preparedStatement.setInt(2, t.getId_user());
		preparedStatement.setInt(3, t.getId_projection());
		preparedStatement.setInt(4,t.getId_movie());
		preparedStatement.setInt(5,t.getId_seat());
				
		preparedStatement.execute();
				
		close();
		
	}
}