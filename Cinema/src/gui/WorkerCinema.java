package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;


import klase.Cinema;
import klase.Controller;
import klase.Movie;
import klase.Projection;
import klase.Ticket;
import klase.User;
import klase.Worker;

import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.ScrollPane;
import java.awt.Toolkit;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;

public class WorkerCinema {

	private JFrame frame;
	private JTextField textField_search;
	String fileName;
	byte[] image_worker= null;
	private JScrollPane menuScrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WorkerCinema window = new WorkerCinema();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public WorkerCinema() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Controller controler=new Controller();
		JTable table = new JTable();
		
		//frame setup
		frame = new JFrame("Cashier");
		frame.setResizable(false);
		frame.setBounds(100, 100, 467, 322);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
	    frame.setLocation(x, y);
		
	    //panel setup
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 431, 261);
		tabbedPane.setBackground(new Color(220, 20, 60));
		tabbedPane.setForeground(Color.BLACK);
		frame.getContentPane().add(tabbedPane);
		
		JPanel frontPanel = new JPanel();
		tabbedPane.addTab("Front Page", null, frontPanel, null);
		frontPanel.setLayout(null);
		
		JPanel addPanel = new JPanel();
		addPanel.setVisible(false);
		addPanel.setBounds(0, 0, 451, 283);
		frame.getContentPane().add(addPanel);
		
		JPanel CRUDpanel = new JPanel();
		tabbedPane.addTab("CRUD User", null, CRUDpanel, null);
		CRUDpanel.setLayout(null);
		
		ScrollPane scrollPane = new ScrollPane();
		scrollPane.setBounds(10, 11, 259, 212);
		CRUDpanel.add(scrollPane);
	
		
		
		
		JPanel salesPanel = new JPanel();
		tabbedPane.addTab("Sales", null, salesPanel, null);
		salesPanel.setLayout(null);

		JPanel updatePanel = new JPanel();
		updatePanel.setBounds(0, 0, 451, 297);
		frame.getContentPane().add(updatePanel);
		updatePanel.setVisible(false);
		updatePanel.setEnabled(false);

		JPanel myProfilePanel = new JPanel();
		myProfilePanel.setBounds(0, 0, 451, 283);
		frame.getContentPane().add(myProfilePanel);
		myProfilePanel.setVisible(false);
		myProfilePanel.setEnabled(false);

		JPanel getCardPanel = new JPanel();
		getCardPanel.setBounds(0, 0, 900, 395);
		frame.getContentPane().add(getCardPanel);
		getCardPanel.setVisible(false);
		getCardPanel.setEnabled(false);
		
//textField search-----------------------------------------------------------------------
				JTextField textField_searchTxt = new JTextField();
				textField_searchTxt.setBounds(146, 194, 154, 25);
				frontPanel.add(textField_searchTxt);
				textField_searchTxt.setColumns(10);
				
				
		
//radioButton setup-----------------------------------------------------------------------------------
		JRadioButton radioBtnUser = new JRadioButton("Users");
		radioBtnUser.setForeground(Color.GREEN);
		radioBtnUser.setBounds(36, 134, 84, 25);
		frontPanel.add(radioBtnUser);
		
			
				
		JRadioButton radioBtnMovies = new JRadioButton("Movies");
		radioBtnMovies.setForeground(Color.GREEN);
		radioBtnMovies.setBounds(36, 104, 84, 25);
		frontPanel.add(radioBtnMovies);
		
		JRadioButton radioBtnCinema = new JRadioButton("Cinema");
		radioBtnCinema.setForeground(Color.GREEN);
		radioBtnCinema.setBounds(36, 74, 84, 25);
		frontPanel.add(radioBtnCinema);
		radioBtnCinema.setSelected(true);
				
		ButtonGroup radioButtons = new ButtonGroup();
		radioButtons.add(radioBtnUser);
		radioButtons.add(radioBtnMovies);
		radioButtons.add(radioBtnCinema);
		
//ScrollPane setup--------------------------------------------------------------------------------------
		ScrollPane scrollPaneCRUDFrontPanel = new ScrollPane();
		scrollPaneCRUDFrontPanel.setBounds(136, 0, 277, 183);
		frontPanel.add(scrollPaneCRUDFrontPanel);
		
		
//label setup-------------------------------------------------------------------------------------------
		JLabel lblLoggedUserProfile = new JLabel("Logged user profile");
		lblLoggedUserProfile.setBounds(12, 13, 118, 14);
		frontPanel.add(lblLoggedUserProfile);
		
		JLabel lblSearch = new JLabel("Search projection:");
		lblSearch.setBounds(10, 14, 116, 14);
		salesPanel.add(lblSearch);
		
//button setup--------------------------------------------------------------------------------------
				JButton btnMyProfile = new JButton("My Profile");
				btnMyProfile.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\profile-icon.png"));
				btnMyProfile.setForeground(Color.WHITE);
				btnMyProfile.setBackground(Color.BLUE);
				btnMyProfile.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						//frame setup for My Profile
						tabbedPane.setVisible(false);
						frame.getContentPane().add(myProfilePanel);
						frame.setTitle("My profile");
						frame.setBounds(100, 100, 356, 275);
						Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
					    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
					    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
					    frame.setLocation(x, y);
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						myProfilePanel.setLayout(null);
						
						//JLabel setup
						JLabel lblName = new JLabel("Name");
						lblName.setBounds(23, 44, 72, 14);
						myProfilePanel.add(lblName);
						
						JLabel lblSurname = new JLabel("Surname");
						lblSurname.setBounds(23, 75, 72, 14);
						myProfilePanel.add(lblSurname);
						
						JLabel lblId = new JLabel("id");
						lblId.setBounds(23, 16, 46, 14);
						myProfilePanel.add(lblId);
						
						JLabel lblEmail = new JLabel("Email");
						lblEmail.setBounds(23, 106, 72, 14);
						myProfilePanel.add(lblEmail);
						
						JLabel lblPassword = new JLabel("Password");
						lblPassword.setBounds(23, 137, 96, 14);
						myProfilePanel.add(lblPassword);
						
						JLabel lblNewLabel = new JLabel("Working hours:");
						lblNewLabel.setBounds(23, 195, 98, 14);
						myProfilePanel.add(lblNewLabel);
						
						JLabel lblTime = new JLabel("Time");
						lblTime.setBounds(129, 195, 86, 14);
						myProfilePanel.add(lblTime);
						
						JLabel lblPassAgain = new JLabel("Pass again");
						lblPassAgain.setBounds(23, 168, 96, 14);
						myProfilePanel.add(lblPassAgain);
						
						JLabel lblPicture = new JLabel("Picture");
						lblPicture.setBounds(237, 16, 93, 104);
						myProfilePanel.add(lblPicture);
						
//JText setup--------------------------------------------------------------------------------------
						
						JTextField textFieldId = new JTextField();
						textFieldId.setBounds(129, 13, 86, 20);
						myProfilePanel.add(textFieldId);
						textFieldId.setColumns(10);
						
						JTextField textFieldName = new JTextField();
						textFieldName.setBounds(129, 41, 86, 20);
						myProfilePanel.add(textFieldName);
						textFieldName.setColumns(10);
						
						JTextField textFieldSurenam = new JTextField();
						textFieldSurenam.setBounds(129, 72, 86, 20);
						myProfilePanel.add(textFieldSurenam);
						textFieldSurenam.setColumns(10);
						
						
						
						JTextField textFieldEmail = new JTextField();
						textFieldEmail.setBounds(129, 103, 86, 20);
						myProfilePanel.add(textFieldEmail);
						textFieldEmail.setColumns(10);
						
						JPasswordField textFieldPassword = new JPasswordField();
						textFieldPassword.setBounds(129, 134, 86, 20);
						myProfilePanel.add(textFieldPassword);
						textFieldPassword.setColumns(10);
						
						JPasswordField textFieldPasswordAgain = new JPasswordField();
						textFieldPasswordAgain.setBounds(129, 165, 86, 20);
						myProfilePanel.add(textFieldPasswordAgain);
						textFieldPasswordAgain.setColumns(10);
						
						
						//JcheckBox setup
						JCheckBox chckbxShow = new JCheckBox("Show");
						chckbxShow.setBounds(221, 133, 65, 23);
						myProfilePanel.add(chckbxShow);
						chckbxShow.addItemListener(new ItemListener() {
							@Override
							public void itemStateChanged(ItemEvent arg0) {
								// TODO Auto-generated method stub
								if(chckbxShow.isSelected()) {
									(  textFieldPassword).setEchoChar((char)0);;
									(textFieldPasswordAgain).setEchoChar((char)0);;
									
								}else {
									( textFieldPassword).setEchoChar('*');
									(textFieldPasswordAgain).setEchoChar('*');
								}
							}
						
						});
						
						
						
//JButton setup--------------------------------------------------------------------------------------
						
						JButton btnBack = new JButton("Back");
						btnBack.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Go-back-icon.png"));
						btnBack.setBounds(237, 211, 89, 23);
						myProfilePanel.add(btnBack);
						btnBack.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent arg0) {
								frame.setTitle("Cashier");
								tabbedPane.setEnabled(true);
								tabbedPane.setVisible(true);
								myProfilePanel.setVisible(false);
								myProfilePanel.setEnabled(false);
								frame.setBounds(100, 100, 467, 322);
								Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
							    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
							    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
							    frame.setLocation(x, y);
							}
						});
						
						JButton btnSave = new JButton("Save");
						btnSave.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\SaveUpdate.png"));
						btnSave.setBackground(Color.ORANGE);
						btnSave.setBounds(237, 185, 89, 23);
						myProfilePanel.add(btnSave);

//----Update worker button------------------------------------------------------------------------------
						
						AbstractAction updateDetailsWorker = new AbstractAction() {
							public void actionPerformed(ActionEvent arg0) {
								
								String idWokrer=textFieldId.getText();
								textFieldId.setEditable(false);
								String name=textFieldName.getText();
								String surname=textFieldSurenam.getText();
								String email=textFieldEmail.getText();
								String password=textFieldPassword.getText();
								String passagain=textFieldPasswordAgain.getText();
								int id=Integer.parseInt(idWokrer);
								
								
								
								if(textFieldPassword.isEnabled() && password.equals(passagain)) {
									controler.updateWorkerProfile(id, name, surname, email, password,image_worker);
								}else if(textFieldPassword.isEnabled()){
									controler.updateWorkerProfile(id, name, surname, email, password,image_worker);
								}else if(textFieldPassword.isEnabled() && !password.equals(passagain)){
									JOptionPane.showMessageDialog(null,"Passwords must match.");
								}else {
									JOptionPane.showMessageDialog(null,"Try again.");
								}
								
							}
						};
						btnSave.addActionListener(updateDetailsWorker);
						
//add image on MY PROFILE-------------------------------------------------------------------------------------------------------
						Action saveImage=new AbstractAction() {
							
							@Override
							public void actionPerformed(ActionEvent arg0) {
								// TODO Auto-generated method stub
								JFileChooser choosePicture = new JFileChooser();
								choosePicture.showOpenDialog(null);
								File file=choosePicture.getSelectedFile();
								fileName=file.getAbsolutePath();
								ImageIcon imageIcon=new ImageIcon(new ImageIcon(fileName).getImage().getScaledInstance(lblPicture.getWidth(), lblPicture.getHeight(), Image.SCALE_SMOOTH));
								lblPicture.setIcon(imageIcon);
								try {
									File image=new File(fileName);
									FileInputStream fis= new FileInputStream(image);
									ByteArrayOutputStream bos=new ByteArrayOutputStream();
									byte[] buf=new byte[1024];
									for(int readNum; (readNum=fis.read(buf)) != -1;) {
										bos.write(buf,0,readNum);
									}
									image_worker=bos.toByteArray();
								}
								catch(Exception e) {
									JOptionPane.showMessageDialog(null, e);
								}
						}
						};
						
						btnSave.addActionListener(updateDetailsWorker);
						
						JButton btnAddPicture = new JButton("Image");
						btnAddPicture.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\picture.png"));

						btnAddPicture.addActionListener(saveImage);
						btnAddPicture.setBounds(237, 158, 89, 23);
						myProfilePanel.add(btnAddPicture);
						
						myProfilePanel.setVisible(true);
						myProfilePanel.setEnabled(true);
						
						
//--set worker page-----------------------	---------------------------------------------------------
						
						Worker worker = controler.getWorker();
						if(worker!=null) {
						textFieldId.setText(worker.getId()+"");
						textFieldId.setEditable(false);
						textFieldName.setText(worker.getName());
						textFieldSurenam.setText(worker.getSurname());
						textFieldEmail.setText(worker.getEmail());
						textFieldPassword.setText(worker.getPassword());
						textFieldPasswordAgain.setEnabled(false);
						lblTime.setText(worker.getWorkingHours()+"h");
						if(worker.getMyPicture()!=null) {
							ImageIcon imageIcon=new ImageIcon(new ImageIcon(worker.getMyPicture()).getImage().getScaledInstance(lblPicture.getWidth(), lblPicture.getHeight(), Image.SCALE_SMOOTH));
							lblPicture.setIcon(imageIcon);
							}else {
								lblPicture.setText("Insert image here");
							}
						
						}else if(worker==null) {
							textFieldId.setText("");
							textFieldName.setText("");
							textFieldSurenam.setText("");	
							textFieldEmail.setText("");
							textFieldPassword.setText("");
							textFieldPasswordAgain.setText("");
							lblTime.setText(0+"h");
						}
						
					}
				});
				
				
				
				btnMyProfile.setBounds(12, 35, 120, 30);
				frontPanel.add(btnMyProfile);
				
				
//Show button from front panel--------------------------------------------------------------------------------------
		JButton btnSearch = new JButton("Search");
		btnSearch.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Search-icon.png"));

		btnSearch.setBackground(Color.GREEN);

		Action actionSearch=new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String search = textField_searchTxt.getText(); 
				if(radioBtnUser.isSelected()) {
					if (textField_searchTxt.getText().length()==0) {
						JOptionPane.showMessageDialog(null, "Enter 'all' to SHOW ALL or \n enter the user name!");
						}
					else {
							try {
								ArrayList <User> users=controler.getAllUsers(search);
								if(users.isEmpty()==false) {
									showAllUsers(table, users);
									scrollPaneCRUDFrontPanel.add(table);
									textField_searchTxt.setText("");
								}
									else {
											JOptionPane.showMessageDialog(null, "Name of the user does not exist!");
											textField_searchTxt.setText("");
										}
											}catch(Exception e) {
											JOptionPane.showMessageDialog(null, e);
										}
									}
								}
							else
								if(radioBtnMovies.isSelected()==true){
									if (textField_searchTxt.getText().length()==0) {
										JOptionPane.showMessageDialog(null, "Enter 'all' to SHOW ALL or \n enter the movie name!");
									}
									else {
										try {
											ArrayList <Movie> movies=controler.getAllMovies(search);
							
											if(movies.isEmpty()==false) {
												showAllMovies(table, movies);
												scrollPaneCRUDFrontPanel.add(table);
												textField_searchTxt.setText("");
											}
											else {
												JOptionPane.showMessageDialog(null, "Name of the movie does not exist!");
												textField_searchTxt.setText("");
										}
										}catch(Exception e) {
							 JOptionPane.showMessageDialog(null, e);
											}	
										}
									}
								else
									if(radioBtnCinema.isSelected()==true){
										if (textField_searchTxt.getText().length()==0) {
											JOptionPane.showMessageDialog(null, "Enter 'all' to SHOW ALL or\n enter the cinema name or City!");
										}
										else {
											try {
												ArrayList <Cinema> cinema=controler.getAllCinema(search);
								
												if(cinema.isEmpty()==false) {
													showAllCinema(table, cinema);
													scrollPaneCRUDFrontPanel.add(table);
													textField_searchTxt.setText("");
												}
												else {
													JOptionPane.showMessageDialog(null, "Name of the cinema or City does not exist!");
													textField_searchTxt.setText("");
											}
											}catch(Exception e) {
								 JOptionPane.showMessageDialog(null, e);
						}	
							}}
				
				
	  	}
	};
	
		btnSearch.addActionListener(actionSearch);
		btnSearch.setBounds(305, 195, 108, 23);
		frontPanel.add(btnSearch);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Button-Log-Off-icon.png"));
		btnLogout.setForeground(Color.WHITE);

		btnLogout.setBackground(Color.ORANGE);
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to logout? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
					CinemaFront cinema = new CinemaFront();
					cinema.main(null);
					frame.dispose();

				}
			}
		});
		btnLogout.setBounds(12, 194, 122, 25);
		frontPanel.add(btnLogout);
		
		JButton buttonAdd = new JButton("Add");
		buttonAdd.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Add-User-icon.png"));
		buttonAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
//frame and panel setup for adding new users--------------------------------------------------------------------------------------
				tabbedPane.setVisible(false);
				tabbedPane.setEnabled(false);
				frame.setTitle("Add User");
				addPanel.setLayout(null);
				
//label setup for adding new users--------------------------------------------------------------------------------------
				JLabel lblId = new JLabel("Manager:");
				lblId.setBounds(10, 36, 86, 14);
				addPanel.add(lblId);
				
				JComboBox comboBoxManager = new JComboBox();
				comboBoxManager.addItem("Stefan Stanojlovic");
				comboBoxManager.addItem("Nikola Sekulic");
				comboBoxManager.addItem("Nikoleta Simovic");
				comboBoxManager.addItem("Sasa Velimirovic");
				
				comboBoxManager.setBackground(Color.LIGHT_GRAY);
				comboBoxManager.setBounds(128, 33, 130, 20);
				
				addPanel.add(comboBoxManager);
				
				JLabel lblName = new JLabel("name:");
				lblName.setBounds(10, 61, 66, 14);
				addPanel.add(lblName);
				
				JLabel lblSurname = new JLabel("surname:");
				lblSurname.setBounds(10, 86, 86, 14);
				addPanel.add(lblSurname);
				
				JLabel lblPassword = new JLabel("password:");
				lblPassword.setBounds(10, 136, 66, 14);
				addPanel.add(lblPassword);
				
				JLabel lblAddress = new JLabel("address:");
				lblAddress.setBounds(10, 161, 108, 14);
				addPanel.add(lblAddress);
				
				JLabel lblEmail = new JLabel("email:");
				lblEmail.setBounds(10, 111, 66, 14);
				addPanel.add(lblEmail);
				
				JLabel lblAddNewUser = new JLabel("Add new user");
				lblAddNewUser.setBounds(85, 11, 80, 14);
				addPanel.add(lblAddNewUser);
				
				JLabel lblBirthDate = new JLabel("birth date:");
				lblBirthDate.setBounds(10, 193, 86, 14);
				addPanel.add(lblBirthDate);
				
				
				JTextField textFieldName = new JTextField();
				textFieldName.setBounds(128, 58, 113, 20);
				addPanel.add(textFieldName);
				textFieldName.setColumns(10);
				
				JTextField textFieldSurname = new JTextField();
				textFieldSurname.setBounds(128, 83, 113, 20);
				addPanel.add(textFieldSurname);
				textFieldSurname.setColumns(10);
				
				JTextField textFieldEmail = new JTextField();
				textFieldEmail.setBounds(128, 108, 113, 20);
				addPanel.add(textFieldEmail);
				textFieldEmail.setColumns(10);
				
				JTextField textFieldAddress = new JTextField();
				textFieldAddress.setBounds(128, 158, 113, 20);
				addPanel.add(textFieldAddress);
				textFieldAddress.setColumns(10);
				
				//JPassword setup
				JPasswordField passwordField = new JPasswordField();
				passwordField.setBounds(128, 134, 113, 17);
				addPanel.add(passwordField);
				
				JLabel picture = new JLabel("Take a photo");
				picture.setBounds(288, 22, 143, 130);
				addPanel.add(picture);
				
				JButton btnTakeAPhoto = new JButton("Image");
				btnTakeAPhoto.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\picture.png"));
				btnTakeAPhoto.setBounds(310, 155, 113, 20);
				addPanel.add(btnTakeAPhoto);
				
				JButton btnBack = new JButton("Back");
				btnBack.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Go-back-icon.png"));
				btnBack.setBackground(Color.LIGHT_GRAY);
				btnBack.setBounds(237, 248, 89, 23);
				addPanel.add(btnBack);
				btnBack.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						frame.setTitle("Cashier");
						Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
					    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
					    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
					    frame.setLocation(x, y);
						tabbedPane.setEnabled(true);
						tabbedPane.setVisible(true);
						addPanel.setEnabled(false);
						addPanel.setVisible(false);
						
					}
				});
				
//JCheckBox setup--------------------------------------------------------------------------------------
				JCheckBox chckbxShowPass = new JCheckBox("show");
				chckbxShowPass.setBounds(247, 155, 66, 27);
				addPanel.add(chckbxShowPass);
				
				chckbxShowPass.addItemListener(new ItemListener() {
					
					@Override
					public void itemStateChanged(ItemEvent arg0) {
						// TODO Auto-generated method stub
						if(chckbxShowPass.isSelected()) {
							(  passwordField).setEchoChar((char)0);;
						}else {
							( passwordField).setEchoChar('*');
						}
					}
				
				});
				
				JComboBox comboBoxDay = new JComboBox();
				comboBoxDay.addItem("01");
				comboBoxDay.addItem("02");
				comboBoxDay.addItem("03");
				comboBoxDay.addItem("04");
				comboBoxDay.addItem("05");
				comboBoxDay.addItem("06");
				comboBoxDay.addItem("07");
				comboBoxDay.addItem("08");
				comboBoxDay.addItem("09");
				comboBoxDay.addItem("10");
				comboBoxDay.addItem("11");
				comboBoxDay.addItem("12");
				comboBoxDay.addItem("13");
				comboBoxDay.addItem("14");
				comboBoxDay.addItem("15");
				comboBoxDay.addItem("16");
				comboBoxDay.addItem("17");
				comboBoxDay.addItem("18");
				comboBoxDay.addItem("19");
				comboBoxDay.addItem("20");
				comboBoxDay.addItem("21");
				comboBoxDay.addItem("22");
				comboBoxDay.addItem("23");
				comboBoxDay.addItem("24");
				comboBoxDay.addItem("25");
				comboBoxDay.addItem("26");
				comboBoxDay.addItem("27");
				comboBoxDay.addItem("28");
				comboBoxDay.addItem("29");
				comboBoxDay.addItem("30");
				comboBoxDay.addItem("31");
				comboBoxDay.setBackground(Color.LIGHT_GRAY);
				comboBoxDay.setBounds(128, 189, 48, 22);
				addPanel.add(comboBoxDay);
				
				
				JComboBox comboBoxMonth = new JComboBox();
				comboBoxMonth.addItem("01");
				comboBoxMonth.addItem("02");
				comboBoxMonth.addItem("03");
				comboBoxMonth.addItem("04");
				comboBoxMonth.addItem("05");
				comboBoxMonth.addItem("06");
				comboBoxMonth.addItem("07");
				comboBoxMonth.addItem("08");
				comboBoxMonth.addItem("09");
				comboBoxMonth.addItem("10");
				comboBoxMonth.addItem("11");
				comboBoxMonth.addItem("12");
			
				comboBoxMonth.setBackground(Color.LIGHT_GRAY);
				comboBoxMonth.setBounds(186, 189, 48, 22);
				addPanel.add(comboBoxMonth);
			
				
				JComboBox comboBoxYear = new JComboBox();
				for(int i=1950;i<=2010;i++) {
					comboBoxYear.addItem(i);
				}
				comboBoxMonth.setBackground(Color.LIGHT_GRAY);
				comboBoxYear.setBounds(247, 189, 80, 22);
				addPanel.add(comboBoxYear);
				
				
				JLabel lblDay = new JLabel("dd/mm/yyyy");			
				lblDay.setBounds(336, 193, 75, 14);
				addPanel.add(lblDay);
				
				
				
				
//frame positioning after all components are loaded--------------------------------------------------------------------------
				frame.setBounds(100, 100, 473, 339);
				Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
			    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
			    frame.setLocation(x, y);
				addPanel.setVisible(true);
	
				
//JButton setup--------------------------------------------------------------------------------------
				JButton btnRegister = new JButton("Register");
				btnRegister.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\userUpdate.png"));
				btnRegister.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						if(textFieldName.getText().length()!=0 && textFieldSurname.getText().length()!=0 && textFieldEmail.getText().length()!=0 || 
								textFieldAddress.getText().length()!=0 ) {
							
							String name = textFieldName.getText();
							String surname = textFieldSurname.getText();
							String email = textFieldEmail.getText();
							String password = passwordField.getText();
							String address = textFieldAddress.getText();
							String birthDay =  comboBoxYear.getItemAt(comboBoxYear.getSelectedIndex())+"-" +
							comboBoxMonth.getItemAt(comboBoxMonth.getSelectedIndex())+"-"+ comboBoxDay.getItemAt(comboBoxDay.getSelectedIndex());
							int id_patmentCard;
							int id_manager=comboBoxManager.getSelectedIndex()+1;
							System.out.println("id menadzera je"+id_manager);
								try {					
										id_patmentCard = controler.getIdPaymentCard(password);
																	
									User u = new User(name, surname, email, password, address, birthDay, id_manager, id_patmentCard);
									
									controler.addUser(u);
									System.out.println("Uspesan unos");
									
									textFieldName.setText("");
									textFieldSurname.setText("");
									textFieldEmail.setText("");
									passwordField.setText("");
									textFieldAddress.setText("");
									
									
									
																		
								}catch (ClassNotFoundException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
										
						   
						}else {
							JOptionPane.showMessageDialog(null, "All fields must be filled out!");
						}
						
					}
				});
				btnRegister.setBackground(Color.ORANGE);
				btnRegister.setBounds(76, 248, 120, 23);
				addPanel.add(btnRegister);
				
				
				
				

				
				
				
			}
		});
		buttonAdd.setBackground(new Color(102, 205, 170));
		buttonAdd.setBounds(298, 60, 116, 23);
		CRUDpanel.add(buttonAdd);
		
		table.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				
			}
		});
		
		JButton buttonRemove = new JButton("Remove");
		buttonRemove.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\userRemove.png"));
		buttonRemove.addActionListener(new ActionListener() {
			
				@Override
				public void actionPerformed(ActionEvent arg0) {
					
					
						int rowNumber=table.getSelectedRow();
						if(rowNumber>=0) {
						String idStr=String.valueOf(table.getValueAt(rowNumber, 0));
						String name=String.valueOf(table.getValueAt(rowNumber, 1));
						String surname=String.valueOf(table.getValueAt(rowNumber, 2));
						String email=String.valueOf(table.getValueAt(rowNumber, 3));
						String address=String.valueOf( table.getValueAt(rowNumber, 4));
						String managerIdStr=String.valueOf(table.getValueAt(rowNumber, 5));
						int id=Integer.parseInt(idStr);
						int manager_id=Integer.parseInt(managerIdStr);
						User user=new User(id, name, surname, email, address, manager_id);
						try {
							int dialogButton = JOptionPane.YES_NO_OPTION;
							int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to delete "+user.getName()+" "+user.getSurname()+"?","Warning",dialogButton);
							if(dialogResult == JOptionPane.YES_OPTION){
							ArrayList<User> users=controler.deleteUser(user);
							DefaultTableModel model=new DefaultTableModel(); 
							Object[] columnsName=new Object[6];
							columnsName[0]="Id";
							columnsName[1]="Name";
							columnsName[2]="Surname";
							columnsName[3]="Email";
							columnsName[4]="Address";
							columnsName[5]="Manager id";
							model.setColumnIdentifiers(columnsName);
							Object[] rowData=new Object[6];
							for(int i=0;i<users.size();i++) {
								rowData[0]=users.get(i).getId();
								rowData[1]=users.get(i).getName();
								rowData[2]=users.get(i).getSurname();
								rowData[3]=users.get(i).getEmail();
								rowData[4]=users.get(i).getAddress();
								rowData[5]=users.get(i).getId_manager();
								model.addRow(rowData);}
							table.setModel(model);
							TableColumnModel columnModel = table.getColumnModel();
							columnModel.getColumn(0).setPreferredWidth(50);
							columnModel.getColumn(1).setPreferredWidth(120);
							columnModel.getColumn(2).setPreferredWidth(130);
							columnModel.getColumn(3).setPreferredWidth(220);
							columnModel.getColumn(4).setPreferredWidth(180);
							columnModel.getColumn(5).setPreferredWidth(50);
							}
							scrollPane.add(table);
						} catch (ClassNotFoundException | SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}else {
						JOptionPane.showMessageDialog(null, "You have to choose an user!\n Click on Show all");
						
					}
					
				}
			
		});
		buttonRemove.setBackground(new Color(255, 0, 0));
		buttonRemove.setBounds(298, 94, 116, 23);
		CRUDpanel.add(buttonRemove);
		
		JButton buttonUpdate = new JButton("Update");
		buttonUpdate.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\userUpdate.png"));
		buttonUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				int rowNumber=table.getSelectedRow();
				if(rowNumber>=0) {
					
					tabbedPane.setVisible(false);
					frame.getContentPane().add(updatePanel);
					frame.setTitle("Update");
				    frame.setBounds(100, 100, 443, 336);
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					updatePanel.setLayout(null);

//JText setup-------------------------------------------------------------------------------------------------------
					JTextField txtAdress = new JTextField();
					txtAdress.setBounds(147, 45, 126, 20);
					updatePanel.add(txtAdress);
					txtAdress.setColumns(10);
					
					JTextField txtId = new JTextField();
					txtId.setBounds(147, 20, 86, 20);
					updatePanel.add(txtId);
					txtId.setColumns(10);
					
					JTextField txtName = new JTextField();
					txtName.setBounds(147, 70, 126, 20);
					updatePanel.add(txtName);
					txtName.setColumns(10);
					
					JTextField txtSurname = new JTextField();
					txtSurname.setBounds(147, 95, 126, 20);
					updatePanel.add(txtSurname);
					txtSurname.setColumns(10);
					
					JTextField txtEmail = new JTextField();
					txtEmail.setBounds(147, 120, 126, 20);
					updatePanel.add(txtEmail);
					txtEmail.setColumns(10);
					
					JPasswordField txtPassword = new JPasswordField();
					txtPassword.setBounds(147, 145, 126, 20);
					updatePanel.add(txtPassword);
					txtPassword.setColumns(10);
					
					JPasswordField txtPassAgain = new JPasswordField();
					txtPassAgain.setBounds(147, 170, 126, 20);
					updatePanel.add(txtPassAgain);
					txtPassAgain.setColumns(10);
					
					JComboBox comboBoxManager = new JComboBox();
					comboBoxManager.addItem("Stefan Stanojlovic");
					comboBoxManager.addItem("Nikola Sekulic");
					comboBoxManager.addItem("Nikoleta Simovic");
					comboBoxManager.addItem("Sasa Velimirovic");
					comboBoxManager.setBackground(Color.LIGHT_GRAY);
					comboBoxManager.setBounds(147, 195, 135, 20);
					updatePanel.add(comboBoxManager);
					
//JLabel setup----------------------------------------------------------------------------------------------------------
					JLabel lblAdress = new JLabel("adress");
					lblAdress.setBounds(40, 48, 73, 14);
					updatePanel.add(lblAdress);
					
					JLabel lblId = new JLabel("id");
					lblId.setBounds(40, 23, 46, 14);
					updatePanel.add(lblId);
					
					JLabel lblNewLabel = new JLabel("name");
					lblNewLabel.setBounds(40, 73, 46, 14);
					updatePanel.add(lblNewLabel);
					
					JLabel lblSurname = new JLabel("surname");
					lblSurname.setBounds(40, 98, 73, 14);
					updatePanel.add(lblSurname);
					
					JLabel lblEmail = new JLabel("email");
					lblEmail.setBounds(40, 123, 73, 14);
					updatePanel.add(lblEmail);
					
					JLabel lblPassword = new JLabel("password");
					lblPassword.setBounds(40, 148, 73, 14);
					updatePanel.add(lblPassword);
					
					JLabel lblConfirmPassword = new JLabel("pass again");
					lblConfirmPassword.setBounds(40, 173, 73, 14);
					updatePanel.add(lblConfirmPassword);
					
					/*JLabel lblPaymentCart = new JLabel("payment cart");
					lblPaymentCart.setBounds(40, 198, 97, 14);
					updatePanel.add(lblPaymentCart);*/
					
					JLabel lblBirthDate = new JLabel("birth date");
					lblBirthDate.setBounds(40, 223, 73, 14);
					updatePanel.add(lblBirthDate);
					
					JLabel lblIdManager = new JLabel("Manager");
					lblIdManager.setBounds(40, 198, 73, 14);
					updatePanel.add(lblIdManager);
				
					
					JButton btnPhoto = new JButton("Photo");
					btnPhoto.setBounds(298, 23, 112, 114);
					updatePanel.add(btnPhoto);
					
//JComboBox setup-----------------------------------------------------------------------------------------------------------------
					JComboBox comboBoxDay = new JComboBox();
					comboBoxDay.addItem("01");
					comboBoxDay.addItem("02");
					comboBoxDay.addItem("03");
					comboBoxDay.addItem("04");
					comboBoxDay.addItem("05");
					comboBoxDay.addItem("06");
					comboBoxDay.addItem("07");
					comboBoxDay.addItem("08");
					comboBoxDay.addItem("09");
					comboBoxDay.addItem("10");
					comboBoxDay.addItem("11");
					comboBoxDay.addItem("12");
					comboBoxDay.addItem("13");
					comboBoxDay.addItem("14");
					comboBoxDay.addItem("15");
					comboBoxDay.addItem("16");
					comboBoxDay.addItem("17");
					comboBoxDay.addItem("18");
					comboBoxDay.addItem("19");
					comboBoxDay.addItem("20");
					comboBoxDay.addItem("21");
					comboBoxDay.addItem("22");
					comboBoxDay.addItem("23");
					comboBoxDay.addItem("24");
					comboBoxDay.addItem("25");
					comboBoxDay.addItem("26");
					comboBoxDay.addItem("27");
					comboBoxDay.addItem("28");
					comboBoxDay.addItem("29");
					comboBoxDay.addItem("30");
					comboBoxDay.addItem("31");
					comboBoxDay.setBackground(Color.LIGHT_GRAY);
					comboBoxDay.setBounds(147, 219, 51, 22);
					updatePanel.add(comboBoxDay);
					
					JComboBox comboBoxMonth = new JComboBox();
					comboBoxMonth.addItem("01");
					comboBoxMonth.addItem("02");
					comboBoxMonth.addItem("03");
					comboBoxMonth.addItem("04");
					comboBoxMonth.addItem("05");
					comboBoxMonth.addItem("06");
					comboBoxMonth.addItem("07");
					comboBoxMonth.addItem("08");
					comboBoxMonth.addItem("09");
					comboBoxMonth.addItem("10");
					comboBoxMonth.addItem("11");
					comboBoxMonth.addItem("12");
					comboBoxMonth.setBackground(Color.LIGHT_GRAY);
					comboBoxMonth.setBounds(208, 219, 51, 22);
					updatePanel.add(comboBoxMonth);
					
					JComboBox comboBoxYear = new JComboBox();
					comboBoxYear.addItem("1990");
					comboBoxYear.addItem("1991");
					comboBoxYear.addItem("1992");
					comboBoxYear.addItem("1993");
					comboBoxYear.addItem("1994");
					comboBoxYear.addItem("1995");
					comboBoxYear.addItem("1996");
					comboBoxYear.addItem("1997");
					comboBoxYear.addItem("1998");
					comboBoxYear.addItem("1999");
					comboBoxYear.addItem("2000");
					comboBoxMonth.setBackground(Color.LIGHT_GRAY);
					comboBoxYear.setBounds(269, 219, 97, 22);
					updatePanel.add(comboBoxYear);
					
//JcheckBox--------------------------------------------------------------------------------------------------------------
					JCheckBox chckbxShow = new JCheckBox("Show");
					chckbxShow.setBounds(279, 144, 97, 23);
					updatePanel.add(chckbxShow);
					chckbxShow.addItemListener(new ItemListener() {
						@Override
						public void itemStateChanged(ItemEvent arg0) {
							if(chckbxShow.isSelected()) {
								( (JPasswordField) txtPassword).setEchoChar((char)0);;
								((JPasswordField) txtPassAgain).setEchoChar((char)0);;
							}else {
								((JPasswordField) txtPassword).setEchoChar('*');
								((JPasswordField) txtPassAgain).setEchoChar('*');
							}
						}
					});
					
//Code for get user----------------------------------------------------------------------------------------------------------
					String idStr = table.getValueAt(rowNumber, 0).toString();
					int id=Integer.parseInt(idStr);
					System.out.println(rowNumber+"");
					User user;
					try {
						user = controler.getUser(id);
						txtId.setText(user.getId()+" ");
						txtId.setEditable(false);
						txtName.setText(user.getName());
						txtSurname.setText(user.getSurname());
						txtEmail.setText(user.getEmail());
						txtAdress.setText(user.getAddress());
						txtPassword.setText(user.getPassword());
						txtPassAgain.setText(user.getPassword());
						
					} catch (ClassNotFoundException | SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			
				
				
//JButton setup--------------------------------------------------------------------------------------------
				JButton btnUpdate = new JButton("Update");
				btnUpdate.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\userUpdate.png"));
				btnUpdate.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						//int rowNumber=table.getSelectedRow();
						String idStr=String.valueOf(table.getValueAt(rowNumber, 0));
						int id=Integer.parseInt(idStr);
						if(txtName.getText().length()!=0 && txtSurname.getText().length()!=0 && txtEmail.getText().length()!=0 && 
								txtAdress.getText().length()!=0 &&	txtPassword.getText().length()!=0 && txtPassAgain.getText().length()!=0) {

								String name = txtName.getText();
								String surname = txtSurname.getText();
								String email = txtEmail.getText();	
								String password = txtPassword.getText();
								String passAgain=txtPassAgain.getText();
								String address = txtAdress.getText();
								String birthDay =  comboBoxYear.getItemAt(comboBoxYear.getSelectedIndex())+"-" +
								comboBoxMonth.getItemAt(comboBoxMonth.getSelectedIndex())+"-"+ comboBoxDay.getItemAt(comboBoxDay.getSelectedIndex());
								int id_patmentCard;
								int id_manager=comboBoxManager.getSelectedIndex()+1;
								System.out.println("id menadzera je"+id_manager);
								try {					
													
									if(password.equals(passAgain)) {
												id_patmentCard = controler.getIdPaymentCard(password);
										
												System.out.println(name+" "+surname+" "+email+" "+password+" "+address+" "+birthDay+" "+id_patmentCard+" "+id_manager );
												controler.updateUser(id, name, surname, email, address, id_manager, null);
												JOptionPane.showMessageDialog(null, "Successfully inserted!");
										
										}
											else {
												JOptionPane.showMessageDialog(null, "Incorect password!\n Try again!");
												}
																			
											}catch (ClassNotFoundException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (SQLException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								
							}else {
								JOptionPane.showMessageDialog(null, "All fields must be filled out!");
							}
			
					}
				});		
				btnUpdate.setBackground(Color.ORANGE);
				btnUpdate.setBounds(65, 262, 120, 23);
				updatePanel.add(btnUpdate);
				
//JButton Back--------------------------------------------------------------------------------------------

				JButton btnBack = new JButton("Back");
				btnBack.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Go-back-icon.png"));
				btnBack.setBackground(Color.LIGHT_GRAY);
				btnBack.setBounds(277, 262, 120, 23);
				updatePanel.add(btnBack);
				btnBack.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						frame.setTitle("Cashier");
						frame.setBounds(100, 100, 467, 322);
						Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
						   int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
						   int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
						   frame.setLocation(x, y);
						tabbedPane.setEnabled(true);
						tabbedPane.setVisible(true);
						updatePanel.setVisible(false);
						updatePanel.setEnabled(false);
				
						
					}
				});
				
				
//frame positioning--------------------------------------------------------------------------------------
				updatePanel.setVisible(true);
			    Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
			    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
			    frame.setLocation(x, y);
		
				}else {
					JOptionPane.showMessageDialog(null,"You have to show all and select user first.");
				}
			
			}
		});
		buttonUpdate.setBackground(new Color(210, 105, 30));
		buttonUpdate.setBounds(298, 128, 116, 23);
		CRUDpanel.add(buttonUpdate);
		
		JButton buttonShowAllUsers = new JButton("Show all");
		buttonShowAllUsers.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\showUser.png"));
		buttonShowAllUsers.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
			
				try {
				ArrayList <User> users=controler.getAllUsers();
				DefaultTableModel model=new DefaultTableModel(); 
				Object[] columnsName=new Object[6];
				columnsName[0]="Id";
				columnsName[1]="Name";
				columnsName[2]="Surname";
				columnsName[3]="Email";
				columnsName[4]="Address";
				columnsName[5]="Manager id";
				model.setColumnIdentifiers(columnsName);
				Object[] rowData=new Object[6];
				for(int i=0;i<users.size();i++) {
					rowData[0]=users.get(i).getId();
					rowData[1]=users.get(i).getName();
					rowData[2]=users.get(i).getSurname();
					rowData[3]=users.get(i).getEmail();
					rowData[4]=users.get(i).getAddress();
					rowData[5]=users.get(i).getId_manager();
					model.addRow(rowData);}
				table.setModel(model);
				TableColumnModel columnModel = table.getColumnModel();
				columnModel.getColumn(0).setPreferredWidth(50);
				columnModel.getColumn(1).setPreferredWidth(120);
				columnModel.getColumn(2).setPreferredWidth(130);
				columnModel.getColumn(3).setPreferredWidth(220);
				columnModel.getColumn(4).setPreferredWidth(180);
				columnModel.getColumn(5).setPreferredWidth(50);
				scrollPane.add(table);
				}catch(Exception e) {
					JOptionPane.showMessageDialog(null, e);
				}
			
				
		}
			
			
			
		});
		buttonShowAllUsers.setBackground(new Color(123, 104, 238));
		buttonShowAllUsers.setBounds(298, 162, 116, 23);
		CRUDpanel.add(buttonShowAllUsers);
		
		ScrollPane scrollPanaProjection = new ScrollPane();
		scrollPanaProjection.setBounds(20, 39, 366, 150);
		salesPanel.add(scrollPanaProjection);
		
		JButton btnShowAllProjections = new JButton("Show all");
		btnShowAllProjections.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\showUser.png"));
		btnShowAllProjections.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				try {
					ArrayList <Projection> projections=controler.getAllProjection();
					DefaultTableModel model=new DefaultTableModel(); 
					Object[] columnsName=new Object[4];
					columnsName[0]="Id";
					columnsName[1]="Date";
					columnsName[2]="Movie";
					columnsName[3]="Cinema";
					model.setColumnIdentifiers(columnsName);
					Object[] rowData=new Object[6];
					for(int i=0;i<projections.size();i++) {
						rowData[0]=projections.get(i).getId();
						rowData[1]=projections.get(i).getDate_time();
						rowData[2]=projections.get(i).getName_movie();
						rowData[3]=projections.get(i).getName_cinema();
						
						model.addRow(rowData);}
					table.setModel(model);
					TableColumnModel columnModel = table.getColumnModel();
					columnModel.getColumn(0).setPreferredWidth(50);
					columnModel.getColumn(1).setPreferredWidth(120);
					columnModel.getColumn(2).setPreferredWidth(130);
					columnModel.getColumn(3).setPreferredWidth(220);
					scrollPanaProjection.add(table);
					}catch(Exception e) {
						JOptionPane.showMessageDialog(null, e);
					}
				
				
			}
		});
		btnShowAllProjections.setBackground(new Color(123, 104, 238));
		btnShowAllProjections.setBounds(30, 195, 143, 23);
		salesPanel.add(btnShowAllProjections);
		
		JButton btnGetCard = new JButton("Get card");
		btnGetCard.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\get-money-icon.png"));
		btnGetCard.setBackground(Color.MAGENTA);
		btnGetCard.setForeground(Color.BLACK);
		btnGetCard.setBounds(243, 195, 143, 23);
		salesPanel.add(btnGetCard);
		btnGetCard.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			int selectedRow=table.getSelectedRow()+1;
			if(selectedRow!=0) {
			
		//frame setup
		tabbedPane.setVisible(false);
		frame.setTitle("Booking");
		frame.setBounds(100, 100, 877, 393);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
	    frame.setLocation(x, y);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getCardPanel.setLayout(null);
			
		
//txtField seats-------------------------------------------------------------------------------------------------------------
		JTextField txtSeats= new JTextField();
		txtSeats.setBounds(578, 194, 114, 77);
		getCardPanel.add(txtSeats);
		txtSeats.setEditable(false);
		
//Choose seats buttons:-------------------------------------------------------------------------------------------------------
		JButton button = new JButton("1A");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
						int dialogButton = JOptionPane.YES_NO_OPTION;
						int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
						if(dialogResult == JOptionPane.YES_OPTION){
						txtSeats.setText("01 1A");
						button.setBackground(Color.red);
						button.setEnabled(false);
	
				 }
					
			}
			
		});
		button.setBounds(38, 36, 50, 30);
		getCardPanel.add(button);
		button.setBackground(Color.GREEN);
		
		JButton button_1 = new JButton("2A");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("02 2A");
				button_1.setBackground(Color.red);
				button_1.setEnabled(false);

		 }
			}
			
		});
		button_1.setBounds(94, 36, 50, 30);
		getCardPanel.add(button_1);
		button_1.setBackground(Color.GREEN);;

		
		JButton btnNewButton = new JButton("3A");
		
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					int dialogButton = JOptionPane.YES_NO_OPTION;
					int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
					if(dialogResult == JOptionPane.YES_OPTION){
					txtSeats.setText("03 3A");
					btnNewButton.setBackground(Color.red);
					btnNewButton.setEnabled(false);

			 }
				}
				
			});
		
		btnNewButton.setBounds(150, 36, 50, 30);
		getCardPanel.add(btnNewButton);
		btnNewButton.setBackground(Color.GREEN);;

		
		JButton button_2 = new JButton("4A");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("04 4A");
				button_2.setBackground(Color.red);
				button_2.setEnabled(false);

		 }
			}
			
		});
		button_2.setBounds(206, 36, 50, 30);
		getCardPanel.add(button_2);
		button_2.setBackground(Color.GREEN);;

		
		JButton button_3 = new JButton("5A");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("05 5A");
				button_3.setBackground(Color.red);
				button_3.setEnabled(false);

		 }
			}
			
		});
		button_3.setBounds(262, 36, 50, 30);
		getCardPanel.add(button_3);
		button_3.setBackground(Color.GREEN);;

		
		JButton button_4 = new JButton("6A");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("06 6A");
				button_4.setBackground(Color.red);
				button_4.setEnabled(false);

		 }
			}
			
		});
		button_4.setBounds(318, 36, 50, 30);
		getCardPanel.add(button_4);
		button_4.setBackground(Color.GREEN);;

		
		JButton button_5 = new JButton("1B");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("7 1B");
				button_5.setBackground(Color.red);
				button_5.setEnabled(false);

		 }
			}
			
		});
		button_5.setBounds(38, 77, 50, 30);
		getCardPanel.add(button_5);
		button_5.setBackground(Color.GREEN);;

		
		JButton button_6 = new JButton("2B");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("8 2B");
				button_6.setBackground(Color.red);
				button_6.setEnabled(false);

		 }
			}
			
		});
		button_6.setBounds(94, 77, 50, 30);
		getCardPanel.add(button_6);
		button_6.setBackground(Color.GREEN);;

		
		JButton button_7 = new JButton("3B");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("9 3B");
				button_7.setBackground(Color.red);
				button_7.setEnabled(false);

		 }
			}
			
		});
		button_7.setBounds(150, 77, 50, 30);
		getCardPanel.add(button_7);
		button_7.setBackground(Color.GREEN);

		
		JButton button_8 = new JButton("4B");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("10 4B");
				button_8.setBackground(Color.red);
				button_8.setEnabled(false);

		 }
			}
			
		});
		button_8.setBounds(206, 77, 50, 30);
		getCardPanel.add(button_8);
		button_8.setBackground(Color.GREEN);;

		
		JButton button_9 = new JButton("5B");
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("11 5B");
				button_9.setBackground(Color.red);
				button_9.setEnabled(false);

		 }
			}
			
		});
		button_9.setBounds(262, 77, 50, 30);
		getCardPanel.add(button_9);
		button_9.setBackground(Color.GREEN);;

		
		JButton button_10 = new JButton("6B");
		button_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("12 6B");
				button_10.setBackground(Color.red);
				button_10.setEnabled(false);

		 }
			}
			
		});
		button_10.setBounds(318, 77, 50, 30);
		getCardPanel.add(button_10);
		button_10.setBackground(Color.GREEN);;

		
		JButton button_11 = new JButton("1C");
		button_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("13 1C");
				button_11.setBackground(Color.red);
				button_11.setEnabled(false);

		 }
			}
			
		});
		button_11.setBounds(38, 118, 50, 30);
		getCardPanel.add(button_11);
		button_11.setBackground(Color.GREEN);;

		
		JButton button_12 = new JButton("2C");
		button_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("14 2C");
				button_12.setBackground(Color.red);
				button_12.setEnabled(false);

		 }
			}
			
		});
		button_12.setBounds(94, 118, 50, 30);
		getCardPanel.add(button_12);
		button_12.setBackground(Color.GREEN);;

		
		JButton button_13 = new JButton("3C");
		button_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("15 3C");
				button_13.setBackground(Color.red);
				button_13.setEnabled(false);

		 }
			}
			
		});
		button_13.setBounds(150, 118, 50, 30);
		getCardPanel.add(button_13);
		button_13.setBackground(Color.GREEN);;

		
		JButton button_14 = new JButton("4C");
		button_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("16 4C");
				button_14.setBackground(Color.red);
				button_14.setEnabled(false);

		 }
			}
			
		});
		button_14.setBounds(206, 118, 50, 30);
		getCardPanel.add(button_14);
		button_14.setBackground(Color.GREEN);;

		
		JButton button_15 = new JButton("5C");
		button_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("17 5C");
				button_15.setBackground(Color.red);
				button_15.setEnabled(false);

		 }
			}
			
		});
		button_15.setBounds(262, 118, 50, 30);
		getCardPanel.add(button_15);
		button_15.setBackground(Color.GREEN);;

		
		JButton button_16 = new JButton("6C");
		button_16.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("18 6C");
				button_16.setBackground(Color.red);
				button_16.setEnabled(false);

		 }
			}
			
		});
		button_16.setBounds(318, 118, 50, 30);
		getCardPanel.add(button_16);
		button_16.setBackground(Color.GREEN);

		
		JButton button_17 = new JButton("1D");
		button_17.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("19 1D");
				button_17.setBackground(Color.red);
				button_17.setEnabled(false);

		 }
			}
			
		});
		button_17.setBounds(38, 159, 50, 30);
		getCardPanel.add(button_17);
		button_17.setBackground(Color.GREEN);
		
		JButton button_18 = new JButton("2D");
		button_18.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("20 2D");
				button_18.setBackground(Color.red);
				button_18.setEnabled(false);

		 }
			}
			
		});
		button_18.setBounds(94, 159, 50, 30);
		getCardPanel.add(button_18);
		button_18.setBackground(Color.GREEN);
		
		JButton button_19 = new JButton("3D");
		button_19.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("21 3D");
				button_19.setBackground(Color.red);
				button_19.setEnabled(false);

		 }
			}
			
		});
		button_19.setBounds(150, 159, 50, 30);
		getCardPanel.add(button_19);
		button_19.setBackground(Color.GREEN);
		
		JButton button_20 = new JButton("4D");
		button_20.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("22 4D");
				button_20.setBackground(Color.red);
				button_20.setEnabled(false);

		 }
			}
			
		});
		button_20.setBounds(206, 159, 50, 30);
		getCardPanel.add(button_20);
		button_20.setBackground(Color.GREEN);
		
		JButton button_21 = new JButton("5D");
		button_21.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("23 5D");
				button_21.setBackground(Color.red);
				button_21.setEnabled(false);

		 }
			}
			
		});
		button_21.setBounds(262, 159, 50, 30);
		getCardPanel.add(button_21);
		button_21.setBackground(Color.GREEN);
		
		JButton button_22 = new JButton("6D");
		button_22.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("24 6D");
				button_22.setBackground(Color.red);
				button_22.setEnabled(false);

		 }
			}
			
		});
		button_22.setBounds(318, 159, 50, 30);
		getCardPanel.add(button_22);
		button_22.setBackground(Color.GREEN);
		
		JButton button_23 = new JButton("1E");
		button_23.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("25 1E");
				button_23.setBackground(Color.red);
				button_23.setEnabled(false);

		 }
			}
			
		});
		button_23.setBounds(38, 200, 50, 30);
		getCardPanel.add(button_23);
		button_23.setBackground(Color.GREEN);
		
		JButton button_24 = new JButton("2E");
		button_24.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("26 2E");
				button_24.setBackground(Color.red);
				button_24.setEnabled(false);

		 }
			}
			
		});
		button_24.setBounds(94, 200, 50, 30);
		getCardPanel.add(button_24);
		button_24.setBackground(Color.GREEN);
		
		JButton button_25 = new JButton("3E");
		button_25.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("27 3E");
				button_25.setBackground(Color.red);
				button_25.setEnabled(false);

		 }
			}
			
		});
		button_25.setBounds(150, 200, 50, 30);
		getCardPanel.add(button_25);
		button_25.setBackground(Color.GREEN);
		
		JButton button_26 = new JButton("4E");
		button_26.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("28 4E");
				button_26.setBackground(Color.red);
				button_26.setEnabled(false);

		 }
			
			}
			
		});
		button_26.setBounds(206, 200, 50, 30);
		getCardPanel.add(button_26);
		button_26.setBackground(Color.GREEN);
		
		JButton button_27 = new JButton("5E");
		button_27.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("29 5E");
				button_27.setBackground(Color.red);
				button_27.setEnabled(false);

		 }
			}
			
		});
		button_27.setBounds(262, 200, 50, 30);
		getCardPanel.add(button_27);
		button_27.setBackground(Color.GREEN);
		
		JButton button_28 = new JButton("6E");
		button_28.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("30 6E");
				button_28.setBackground(Color.red);
				button_28.setEnabled(false);

		 }
			}
			
		});
		button_28.setBounds(318, 200, 50, 30);
		getCardPanel.add(button_28);
		button_28.setBackground(Color.GREEN);
		
		JButton button_29 = new JButton("1F");
		button_29.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("31 1F");
				button_29.setBackground(Color.red);
				button_29.setEnabled(false);

		 }
			}
			
		});
		button_29.setBounds(38, 241, 50, 30);
		getCardPanel.add(button_29);
		button_29.setBackground(Color.GREEN);
		
		JButton button_30 = new JButton("2F");
		button_30.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("32 2F");
				button_30.setBackground(Color.red);
				button_30.setEnabled(false);

		 }
			}
			
		});
		button_30.setBounds(94, 241, 50, 30);
		getCardPanel.add(button_30);
		button_30.setBackground(Color.GREEN);
		
		JButton button_31 = new JButton("3F");
		button_31.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("33 3F");
				button_31.setBackground(Color.red);
				button_31.setEnabled(false);

		 }
			}
			
		});
		button_31.setBounds(150, 241, 50, 30);
		getCardPanel.add(button_31);
		button_31.setBackground(Color.GREEN);
		
		JButton button_32 = new JButton("4F");
		button_32.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("34 4F");
				button_32.setBackground(Color.red);
				button_32.setEnabled(false);

		 }
			}
			
		});
		button_32.setBounds(206, 241, 50, 30);
		getCardPanel.add(button_32);
		button_32.setBackground(Color.GREEN);
		
		JButton button_33 = new JButton("5F");
		button_33.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("35 5F");
				button_33.setBackground(Color.red);
				button_33.setEnabled(false);

		 }
			}
			
		});
		button_33.setBounds(262, 241, 50, 30);
		getCardPanel.add(button_33);
		button_33.setBackground(Color.GREEN);
		
		JButton button_34 = new JButton("6F");
		button_34.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("36 6F");
				button_34.setBackground(Color.red);
				button_34.setEnabled(false);

		 }
			}
			
		});
		button_34.setBounds(318, 241, 50, 30);
		getCardPanel.add(button_34);
		button_34.setBackground(Color.GREEN);
		
		JButton btnNewButton_1 = new JButton("7A");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("37 7A");
				btnNewButton_1.setBackground(Color.red);
				btnNewButton_1.setEnabled(false);

				}
				
			}
			
		});
		btnNewButton_1.setBounds(374, 36, 50, 30);
		getCardPanel.add(btnNewButton_1);
		btnNewButton_1.setBackground(Color.GREEN);
		
		JButton button_35 = new JButton("7B");
		button_35.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("38 7B");
				button_35.setBackground(Color.red);
				button_35.setEnabled(false);

		 }
			}
			
		});
		button_35.setBounds(374, 77, 50, 30);
		getCardPanel.add(button_35);
		button_35.setBackground(Color.GREEN);
		
		JButton button_36 = new JButton("7C");
		button_36.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("39 7C");
				button_36.setBackground(Color.red);
				button_36.setEnabled(false);

		 }
			}
			
		});
		button_36.setBounds(374, 118, 50, 30);
		getCardPanel.add(button_36);
		button_36.setBackground(Color.GREEN);
		
		JButton button_37 = new JButton("7D");
		button_37.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("40 7D");
				button_37.setBackground(Color.red);
				button_37.setEnabled(false);

		 }
			}
			
		});
		button_37.setBounds(374, 159, 50, 30);
		getCardPanel.add(button_37);
		button_37.setBackground(Color.GREEN);
		
		JButton button_38 = new JButton("7E");
		button_38.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("41 7E");
				button_38.setBackground(Color.red);
				button_38.setEnabled(false);

		 }
			}
			
		});
		button_38.setBounds(374, 200, 50, 30);
		getCardPanel.add(button_38);
		button_38.setBackground(Color.GREEN);
		
		JButton button_39 = new JButton("7F");
		button_39.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("42 7F");
				button_39.setBackground(Color.red);
				button_39.setEnabled(false);

		 }
			}
			
		});
		button_39.setBounds(374, 241, 50, 30);
		getCardPanel.add(button_39);
		button_39.setBackground(Color.GREEN);
		
		JButton button_40 = new JButton("8C");
		button_40.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("43 8C");
				button_40.setBackground(Color.red);
				button_40.setEnabled(false);

		 }
			}
			
		});
		button_40.setBounds(430, 118, 50, 30);
		getCardPanel.add(button_40);
		button_40.setBackground(Color.GREEN);
		
		JButton button_41 = new JButton("8D");
		button_41.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("44 8D");
				button_41.setBackground(Color.red);
				button_41.setEnabled(false);

		 }
			}
			
		});
		button_41.setBounds(430, 159, 50, 30);
		getCardPanel.add(button_41);
		button_41.setBackground(Color.GREEN);
		
		JButton button_42 = new JButton("8E");
		button_42.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("45 8E");
				button_42.setBackground(Color.red);
				button_42.setEnabled(false);

		 }
			}
			
		});
		button_42.setBounds(430, 200, 50, 30);
		getCardPanel.add(button_42);
		button_42.setBackground(Color.GREEN);
		
		JButton button_43 = new JButton("8F");
		button_43.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want this ticket? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
				txtSeats.setText("46 8F");
				button_43.setBackground(Color.red);
				button_43.setEnabled(false);

		 }
			}
			
		});
		button_43.setBounds(430, 241, 50, 30);
		getCardPanel.add(button_43);
		button_43.setBackground(Color.GREEN);
		
		//JLabel setup
		JLabel label = new JLabel("A");
		label.setForeground(Color.MAGENTA);
		label.setBounds(485, 36, 31, 14);
		getCardPanel.add(label);
		
		JLabel label_1 = new JLabel("B");
		label_1.setForeground(Color.MAGENTA);
		label_1.setBounds(485, 77, 31, 14);
		getCardPanel.add(label_1);
		
		JLabel label_2 = new JLabel("C");
		label_2.setForeground(Color.MAGENTA);
		label_2.setBounds(485, 118, 31, 14);
		getCardPanel.add(label_2);
		
		JLabel label_3 = new JLabel("D");
		label_3.setForeground(Color.MAGENTA);
		label_3.setBounds(485, 159, 31, 14);
		getCardPanel.add(label_3);
		
		JLabel label_4 = new JLabel("E");
		label_4.setForeground(Color.MAGENTA);
		label_4.setBounds(485, 200, 31, 14);
		getCardPanel.add(label_4);
		
		JLabel label_5 = new JLabel("F");
		label_5.setForeground(Color.MAGENTA);
		label_5.setBounds(485, 241, 31, 14);
		getCardPanel.add(label_5);
		
//LABEL FOR BACKGROUND--------------------------------------------------------------------------------------------
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 502, 332);
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\sstan\\Desktop\\Stefan_Stanojlovic_96-2015\\images\\36_big.jpg"));
		getCardPanel.add(lblNewLabel);
		
		JLabel lblMovie = new JLabel("Movie:");
		lblMovie.setBounds(522, 36, 46, 14);
		getCardPanel.add(lblMovie);
		
		JLabel lblTime = new JLabel("Date/Time:");
		lblTime.setBounds(522, 61, 80, 14);
		getCardPanel.add(lblTime);
		
		JLabel lblProjection = new JLabel("Projection id:");
		lblProjection.setBounds(522, 100, 80, 14);
		getCardPanel.add(lblProjection);
		
		JLabel lblTotal = new JLabel("Total $:");
		lblTotal.setBounds(522, 167, 46, 14);
		getCardPanel.add(lblTotal);
		
		JLabel lblSeats = new JLabel("Seats:");
		lblSeats.setBounds(522, 195, 46, 14);
		getCardPanel.add(lblSeats);
		
		JLabel lblHall = new JLabel("Cinema:");
		lblHall.setBounds(522, 134, 46, 14);
		getCardPanel.add(lblHall);
		
		//JTextField setup
		 JTextField textFieldMovie = new JTextField();
		textFieldMovie.setBounds(578, 33, 114, 20);
		getCardPanel.add(textFieldMovie);
		textFieldMovie.setColumns(10);
		textFieldMovie.setEditable(false);
		
		JTextField textFielProjection = new JTextField();
		textFielProjection.setBounds(600, 100, 20, 20);
		getCardPanel.add(textFielProjection);
		textFielProjection.setColumns(10);
		textFielProjection.setEditable(false);
		
		JTextField textFieldCost = new JTextField();
		textFieldCost.setBounds(578, 164, 114, 20);
		getCardPanel.add(textFieldCost);
		textFieldCost.setColumns(10);
		
		//JList setup
		
		JList listUser = new JList();
		listUser.setBounds(702, 33, 149, 156);
		listUser.setVisibleRowCount(4);
		listUser.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getCardPanel.add(listUser);
		
		JTextField txtTime = new JTextField();
		txtTime.setBounds(578, 57, 114, 22);
		getCardPanel.add(txtTime);
		txtTime.setEditable(false);
		
		JTextField txtCinema = new JTextField();
		txtCinema.setBounds(578, 131, 114, 22);
		getCardPanel.add(txtCinema);
		txtCinema.setEditable(false);
		
		getCardPanel.setVisible(true);
		getCardPanel.setEnabled(true);
		
		
		
		//buttons book and back
		JButton btnShowUsers = new JButton("Show users");
		btnShowUsers.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\showUser.png"));

		btnShowUsers.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				int rowNumber=table.getSelectedRow();
				//Projection p = controler.getProjection(rowNumber);
					
					try {
						ArrayList<User> user = controler.getAllUsers();
						DefaultListModel DLM = new DefaultListModel();
						for(User u:user){
							DLM.addElement(u);
						}
						listUser.setModel(DLM);
						
						
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		});
		btnShowUsers.setBounds(720, 195, 132, 28);
		getCardPanel.add(btnShowUsers);
		
		JButton btnBookNow = new JButton("Book now");
		btnBookNow.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\booknow.png"));
		btnBookNow.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//CODE--------------------------------------------------------------------------------------------------
				int selectedRow=table.getSelectedRow()+1;
				Projection p = controler.getProjection(selectedRow);
					if(txtSeats.getText().length()>0) {
				
					if(textFieldCost.getText().length()>0 && listUser.getSelectedValue().toString()!="") {
						String selected = listUser.getSelectedValue().toString();
						try {
							
					int price=Integer.parseInt(textFieldCost.getText());
					String user_id =selected.substring(0,1);
					int id_user=Integer.parseInt(user_id);
					
					int id_projection=Integer.parseInt(textFielProjection.getText());
					int id_movie=controler.getMovieID(p.getName_movie());
					String id_seat=txtSeats.getText().substring(0, 2);
					int seat=Integer.parseInt(id_seat);
					Ticket t = new Ticket(price, id_user, id_projection, id_movie,seat);
					System.out.println("Test uspesan usera:"+t.toString());
					try {
						controler.addTicket(t);
						System.out.println("Uspesan unos tiketa");
						frame.setBounds(100, 100, 630, 293);
						Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
					    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
					    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
					    frame.setLocation(x, y);
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						frame.getContentPane().setLayout(null);
						
						JPanel panelTiket = new JPanel();
						panelTiket.setBackground(new Color(127, 255, 0));
						panelTiket.setBounds(0, 0, 612, 246);
						frame.getContentPane().add(panelTiket);
						panelTiket.setLayout(null);
						getCardPanel.setVisible(false);
						panelTiket.setVisible(true);

						
						JButton btnBack = new JButton("Back");
						btnBack.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Seminarski\\cineplex\\images\\Go-back-icon.png"));

				
							btnBack.addActionListener(new ActionListener() {
								
								@Override
								public void actionPerformed(ActionEvent arg0) {
									frame.setTitle("Cashier");
									frame.setBounds(100, 100, 467, 322);
									Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
									   int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
									   int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
									   frame.setLocation(x, y);
									tabbedPane.setVisible(true);
									tabbedPane.setEnabled(true);
									getCardPanel.setVisible(false);
									getCardPanel.setEnabled(false);
									panelTiket.setVisible(false);
									salesPanel.revalidate();
									salesPanel.repaint();
									textFieldCost.setText("");
									txtSeats.setText("");

									
								}
							});
						btnBack.setBounds(510, 216, 97, 25);
						panelTiket.add(btnBack);

						
						JLabel lblcinemaCard = new JLabel("***CINEMA TICKET***");
						lblcinemaCard.setForeground(new Color(245, 255, 250));
						lblcinemaCard.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 24));
						lblcinemaCard.setBounds(199, 25, 257, 33);
						panelTiket.add(lblcinemaCard);
						
						JLabel lblNewLabel = new JLabel("STANDARD/3D");
						lblNewLabel.setForeground(new Color(255, 255, 255));
						lblNewLabel.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 16));
						lblNewLabel.setBounds(12, 91, 129, 22);
						panelTiket.add(lblNewLabel);
						
						JSeparator separator = new JSeparator();
						separator.setOrientation(SwingConstants.VERTICAL);
						separator.setBackground(Color.BLUE);
						separator.setBounds(153, 13, 2, 228);
						panelTiket.add(separator);
						
						JLabel lblSeat = new JLabel("SEAT:");
						lblSeat.setForeground(new Color(255, 255, 255));
						lblSeat.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
						lblSeat.setBounds(12, 144, 56, 16);
						panelTiket.add(lblSeat);
						
						JLabel lblDatetime = new JLabel("DATE/TIME");
						lblDatetime.setForeground(new Color(255, 255, 255));
						lblDatetime.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
						lblDatetime.setBounds(12, 196, 129, 16);
						panelTiket.add(lblDatetime);
						
						JLabel lblNumber = new JLabel("NUMBER:");
						lblNumber.setForeground(Color.WHITE);
						lblNumber.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
						lblNumber.setBounds(199, 225, 64, 16);
						panelTiket.add(lblNumber);
						
						JLabel lblMovie = new JLabel("MOVIE");
						lblMovie.setForeground(Color.WHITE);
						lblMovie.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 19));
						lblMovie.setBounds(199, 93, 129, 16);
						panelTiket.add(lblMovie);
						
						JLabel lblCinema = new JLabel("CINEMA");
						lblCinema.setForeground(Color.WHITE);
						lblCinema.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
						lblCinema.setBounds(199, 144, 56, 16);
						panelTiket.add(lblCinema);
						
						JLabel lblSeat_1 = new JLabel("SEAT");
						lblSeat_1.setForeground(Color.WHITE);
						lblSeat_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
						lblSeat_1.setBounds(497, 173, 39, 16);
						panelTiket.add(lblSeat_1);
						
						JLabel lblPrice = new JLabel("PRICE:");
						lblPrice.setForeground(Color.WHITE);
						lblPrice.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
						lblPrice.setBounds(166, 196, 50, 16);
						panelTiket.add(lblPrice);
						
						
						
						JLabel lblDate = new JLabel("DATE:");
						lblDate.setForeground(Color.WHITE);
						lblDate.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
						lblDate.setBounds(167, 173, 56, 16);
						panelTiket.add(lblDate);
						
						JLabel labelSeat = new JLabel("seat");
						labelSeat.setForeground(Color.RED);
						labelSeat.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
						labelSeat.setBounds(58, 145, 39, 16);
						panelTiket.add(labelSeat);
						
						JLabel labelDateTime = new JLabel("datetime");
						labelDateTime.setForeground(Color.RED);
						labelDateTime.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 13));
						labelDateTime.setBounds(12, 225, 129, 16);
						panelTiket.add(labelDateTime);
						
						JLabel labelMovie = new JLabel("movie");
						labelMovie.setForeground(Color.RED);
						labelMovie.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 20));
						labelMovie.setHorizontalAlignment(SwingConstants.CENTER);
						labelMovie.setBounds(277, 93, 264, 16);
						panelTiket.add(labelMovie);
						
						JLabel labelCinema = new JLabel("seat");
						labelCinema.setForeground(Color.RED);
						labelCinema.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
						labelCinema.setBounds(266, 146, 297, 16);
						panelTiket.add(labelCinema);
						
						JLabel labelDateTime1 = new JLabel("datetime");
						labelDateTime1.setForeground(Color.RED);
						labelDateTime1.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
						labelDateTime1.setBounds(230, 175, 167, 16);
						panelTiket.add(labelDateTime1);
						
						JLabel labelPrice = new JLabel("seat");
						labelPrice.setForeground(Color.RED);
						labelPrice.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
						labelPrice.setBounds(217, 196, 56, 16);
						panelTiket.add(labelPrice);
						
						JLabel labelProjection = new JLabel("seat");
						labelProjection.setForeground(Color.RED);
						labelProjection.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
						labelProjection.setBounds(266, 225, 56, 16);
						panelTiket.add(labelProjection);
						
						JLabel labelSeat2 = new JLabel("seat");
						labelSeat2.setForeground(Color.RED);
						labelSeat2.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
						labelSeat2.setBounds(538, 174, 56, 16);
						panelTiket.add(labelSeat2);
						
						labelSeat.setText(txtSeats.getText().substring(2, 5));
						labelSeat2.setText(txtSeats.getText().substring(2, 5));
						labelDateTime.setText(txtTime.getText());
						labelDateTime1.setText(txtTime.getText());
						labelMovie.setText(textFieldMovie.getText());
						labelCinema.setText(txtCinema.getText());
						labelPrice.setText(textFieldCost.getText());
						labelProjection.setText(textFielProjection.getText());
					} catch (ClassNotFoundException | SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null, "The price of ticket must be number");
						}
				
					
					}else {
						JOptionPane.showMessageDialog(null, "All fields must be field out");

					}
					}else {
						JOptionPane.showMessageDialog(null, "Choose an card");
					}
			}
		});
		btnBookNow.setBounds(648, 281, 120, 28);
		getCardPanel.add(btnBookNow);
		
		JButton btnBack = new JButton("Back");
		btnBack.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\backmin.png"));
		btnBack.setBounds(648, 320, 89, 23);
		getCardPanel.add(btnBack);
		btnBack.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.setTitle("Cashier");
				frame.setBounds(100, 100, 467, 322);
				Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
				   int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
				   int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
				   frame.setLocation(x, y);
				tabbedPane.setVisible(true);
				tabbedPane.setEnabled(true);
				getCardPanel.setVisible(false);
				getCardPanel.setEnabled(false);
			}
		});
	
		
		
//code-----------------------------------------------------------------------------------------
	
		
		System.out.println(""+selectedRow);
	
		ArrayList<Integer> niz;
		try {
			niz = controler.getAllSeats(selectedRow);
			System.out.println(""+niz);
			Projection p = controler.getProjection(selectedRow);
			int id_projection=p.getId();
			textFielProjection.setText(""+id_projection);
			textFieldMovie.setText(p.getName_movie());
			txtTime.setText(p.getDate_time());
			txtCinema.setText(p.getName_cinema());
			
			for (Integer in : niz) { 
				 if(in.equals(1)) {
					 button.setBackground(Color.red);
					 button.setEnabled(false);
				 }
				 else if(in.equals(2)) {
						 button_1.setBackground(Color.red);
						 button_1.setEnabled(false);

					 }
				 else if(in.equals(3)) {
					 btnNewButton.setBackground(Color.red);
					 btnNewButton.setEnabled(false);

				 }
				 else if(in.equals(4)) {
					 button_2.setBackground(Color.red);
					 button_2.setEnabled(false);

				 }
				 else if(in.equals(5)) {
					 button_3.setBackground(Color.red);
					 button_3.setEnabled(false);

				 }
				 else if(in.equals(6)) {
					 button_4.setBackground(Color.red);
					 button_4.setEnabled(false);

				 }
				 else if(in.equals(7)) {
					 button_5.setBackground(Color.red);
					 button_5.setEnabled(false);

				 }
				 else if(in.equals(8)) {
					 button_6.setBackground(Color.red);
					 button_6.setEnabled(false);

				 }
				 else if(in.equals(9)) {
					 button_7.setBackground(Color.red);
					 button_7.setEnabled(false);

				 }
				 else if(in.equals(10)) {
					 button_8.setBackground(Color.red);
					 button_8.setEnabled(false);

				 }
				 else if(in.equals(11)) {
					 button_9.setBackground(Color.red);
					 button_9.setEnabled(false);

				 }
				 else if(in.equals(12)) {
					 button_10.setBackground(Color.red);
					 button_10.setEnabled(false);

				 }
				 else if(in.equals(13)) {
					 button_11.setBackground(Color.red);
					 button_11.setEnabled(false);

				 }
				 else if(in.equals(14)) {
					 button_12.setBackground(Color.red);
					 button_12.setEnabled(false);

				 }
				 else if(in.equals(15)) {
					 button_13.setBackground(Color.red);
					 button_13.setEnabled(false);

				 }
				 else if(in.equals(16)) {
					 button_14.setBackground(Color.red);
					 button_14.setEnabled(false);

				 }
				 else if(in.equals(17)) {
					 button_15.setBackground(Color.red);
					 button_15.setEnabled(false);

				 }
				 else if(in.equals(18)) {
					 button_16.setBackground(Color.red);
					 button_16.setEnabled(false);

				 }
				 else if(in.equals(19)) {
					 button_17.setBackground(Color.red);
					 button_17.setEnabled(false);

				 }
				 else if(in.equals(20)) {
					 button_18.setBackground(Color.red);
					 button_18.setEnabled(false);

				 }
				 else if(in.equals(21)) {
					 button_19.setBackground(Color.red);
					 button_19.setEnabled(false);

				 }
				 else if(in.equals(22)) {
					 button_20.setBackground(Color.red);
					 button_20.setEnabled(false);

				 }
				 else if(in.equals(23)) {
					 button_21.setBackground(Color.red);
					 button_21.setEnabled(false);

				 }else if(in.equals(24)) {
					 button_22.setBackground(Color.red);
					 button_22.setEnabled(false);

				 }
				 else if(in.equals(25)) {
					 button_23.setBackground(Color.red);
					 button_23.setEnabled(false);

				 }
				 else if(in.equals(26)) {
					 button_24.setBackground(Color.red);
					 button_24.setEnabled(false);

				 }
				 else if(in.equals(27)) {
					 button_25.setBackground(Color.red);
					 button_25.setEnabled(false);

				 }
				 else if(in.equals(28)) {
					 button_26.setBackground(Color.red);
					 button_26.setEnabled(false);

				 }
				 else if(in.equals(29)) {
					 button_27.setBackground(Color.red);
					 button_27.setEnabled(false);

				 }
				 else if(in.equals(30)) {
					 button_28.setBackground(Color.red);
					 button_28.setEnabled(false);

				 }
				 else if(in.equals(31)) {
					 button_29.setBackground(Color.red);
					 button_29.setEnabled(false);

				 }
				 else if(in.equals(32)) {
					 button_30.setBackground(Color.red);
					 button_30.setEnabled(false);

				 }
				 else if(in.equals(33)) {
					 button_31.setBackground(Color.red);
					 button_31.setEnabled(false);

				 }
				 else if(in.equals(34)) {
					 button_32.setBackground(Color.red);
					 button_32.setEnabled(false);

				 }
				 else if(in.equals(35)) {
					 button_33.setBackground(Color.red);
					 button_33.setEnabled(false);

				 }
				 else if(in.equals(36)) {
					 button_34.setBackground(Color.red);
					 button_34.setEnabled(false);

				 }
				 else if(in.equals(37)) {
					 btnNewButton_1.setBackground(Color.red);
					 button_35.setEnabled(false);

				 }
				 else if(in.equals(38)) {
					 button_35.setBackground(Color.red);
					 button_35.setEnabled(false);

				 }
				 else if(in.equals(39)) {
					 button_36.setBackground(Color.red);
					 button_36.setEnabled(false);

				 }
				 else if(in.equals(40)) {
					 button_37.setBackground(Color.red);
					 button_37.setEnabled(false);

				 }
				 else if(in.equals(41)) {
					 button_38.setBackground(Color.red);
					 button_38.setEnabled(false);

				 }
				 else if(in.equals(42)) {
					 button_39.setBackground(Color.red);
					 button_39.setEnabled(false);

				 }
				 else if(in.equals(43)) {
					 button_40.setBackground(Color.red);
					 button_40.setEnabled(false);

				 }
				 else if(in.equals(44)) {
					 button_41.setBackground(Color.red);
					 button_41.setEnabled(false);

				 }
				 else if(in.equals(45)) {
					 button_42.setBackground(Color.red);
					 button_42.setEnabled(false);

				 }
				 else if(in.equals(46)) {
					 button_43.setBackground(Color.red);
					 button_43.setEnabled(false);

				 }
	 
					 

			   
			   }
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
			}else {
				JOptionPane.showMessageDialog(null, "You have to choose some projection");
			}
		
		
			}
		});
		
		
		
		JTextField txtSearch = new JTextField();
		txtSearch.setBounds(122, 11, 150, 20);
		salesPanel.add(txtSearch);
		txtSearch.setColumns(10);
		
		
		JButton btnSearch_1 = new JButton("Search");
		btnSearch_1.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Search-icon.png"));
		btnSearch_1.setForeground(Color.BLACK);
		btnSearch_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Projection> projections;
				String movieName =txtSearch.getText();
				if(txtSearch.getText().length()>0) {
					
					try {
						projections = controler.getAllProjection(movieName);
						showAllProjectionsTable(table, projections);
						scrollPanaProjection.add(table);
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else {
					JOptionPane.showMessageDialog(null, "Enter movie name or click on Show all");
				}
					
				
				
			}
		});
		btnSearch_1.setBackground(Color.GREEN);
		btnSearch_1.setBounds(277, 9, 109, 25);
		salesPanel.add(btnSearch_1);
		
		
		
		JPanel panelTiket = new JPanel();
		panelTiket.setBounds(0, 0, 10, 10);
		frame.getContentPane().add(panelTiket);
		panelTiket.setVisible(false);
	}
	
	private void showAllProjectionsTable(JTable table, ArrayList<Projection> projections) {
		DefaultTableModel model=new DefaultTableModel(); 
		String[] columnsName=new String[4];
		columnsName[0]="Id";
		columnsName[1]="Date&Time";
		columnsName[2]="Movie";
		columnsName[3]="Cinema";
		model.setColumnIdentifiers(columnsName);
		Object[] rowData=new Object[4];
		for(int i=0;i<projections.size();i++) {
			rowData[0]=projections.get(i).getId();
			rowData[1]=projections.get(i).getDate_time();
			rowData[2]=projections.get(i).getName_movie();
			rowData[3]=projections.get(i).getName_cinema();
			model.addRow(rowData);
			}
		table.setModel(model);
		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(140);
		columnModel.getColumn(2).setPreferredWidth(150);
		columnModel.getColumn(3).setPreferredWidth(150);
	}
	
	
	
	
	private void showAllUsers(JTable table, ArrayList<User> users) {
		DefaultTableModel model=new DefaultTableModel(); 
		Object[] columnsName=new Object[6];
		columnsName[0]="Id";
		columnsName[1]="Name";
		columnsName[2]="Surname";
		columnsName[3]="Email";
		columnsName[4]="Address";
		columnsName[5]="Manager id";
		model.setColumnIdentifiers(columnsName);
		Object[] rowData=new Object[6];
		for(int i=0;i<users.size();i++) {
			rowData[0]=users.get(i).getId();
			rowData[1]=users.get(i).getName();
			rowData[2]=users.get(i).getSurname();
			rowData[3]=users.get(i).getEmail();
			rowData[4]=users.get(i).getAddress();
			rowData[5]=users.get(i).getId_manager();
			model.addRow(rowData);}
		table.setModel(model);
		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(120);
		columnModel.getColumn(2).setPreferredWidth(130);
		columnModel.getColumn(3).setPreferredWidth(220);
		columnModel.getColumn(4).setPreferredWidth(180);
		columnModel.getColumn(5).setPreferredWidth(50);
	}
	
	
	private void showAllMovies(JTable table, ArrayList<Movie> movies) {
		DefaultTableModel model=new DefaultTableModel(); 
		Object[] columnsName=new Object[3];
		columnsName[0]="Name";
		columnsName[1]="Genre";
		columnsName[2]="Duration";
		
		model.setColumnIdentifiers(columnsName);
		Object[] rowData=new Object[3];
		for(int i=0;i<movies.size();i++) {
			rowData[0]=movies.get(i).getMovieName();
			rowData[1]=movies.get(i).getGenre();
			rowData[2]=movies.get(i).getDuration();
			model.addRow(rowData);}
		table.setModel(model);
		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(120);
		columnModel.getColumn(1).setPreferredWidth(120);
		columnModel.getColumn(2).setPreferredWidth(80);
		
	}
	private void showAllCinema(JTable table, ArrayList<Cinema> cinema) {
		DefaultTableModel model=new DefaultTableModel(); 
		Object[] columnsName=new Object[3];
		columnsName[0]="Name";
		columnsName[1]="location";
		columnsName[2]="city";
		
		model.setColumnIdentifiers(columnsName);
		Object[] rowData=new Object[3];
		for(int i=0;i<cinema.size();i++) {
			rowData[0]=cinema.get(i).getName();
			rowData[1]=cinema.get(i).getLocation();
			rowData[2]=cinema.get(i).getCity();
			model.addRow(rowData);}
		table.setModel(model);
		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(120);
		columnModel.getColumn(1).setPreferredWidth(120);
		columnModel.getColumn(2).setPreferredWidth(80);
		
	}
	
	
}
