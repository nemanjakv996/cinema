package klase;


import java.util.*;

/**
 * 
 */
public class Hall {

    /**
     * Default constructor
     */
    public Hall() {
    }

    /**
     * 
     */
    private int id;
    
    public String name;

    public boolean taken;

    private int cinema_id;
    
    public Seat[] seats;
    
    public int getCinema_id() {
		return cinema_id;
	}

	public void setCinema_id(int cinema_id) {
		this.cinema_id = cinema_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isTaken() {
		return taken;
	}

	public void setTaken(boolean taken) {
		this.taken = taken;
	}

	public Seat[] getSeats() {
		return seats;
	}

	public void setSeats(Seat[] seats) {
		this.seats = seats;
	}

	public Hall(String name, boolean taken, int cinema_id) {
		super();
		this.name = name;
		this.taken = taken;
		this.cinema_id = cinema_id;
	}

	public Hall(int id, String name, boolean taken, int cinema_id) {
		super();
		this.id = id;
		this.name = name;
		this.taken = taken;
		this.cinema_id = cinema_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}





}