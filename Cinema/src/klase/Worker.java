package klase;



import java.awt.image.BufferedImage;
import java.io.File;
import java.util.*;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

/**
 * 
 */
public class Worker extends Person {

    /**
     * Default constructor
     */
    public Worker() {
    }

    /**
     * 
     */
    public float salary;

    public int workingHours;
    
    public byte[] myPicture;

    public String address;
    
    public int managerId;
    /**
     * 
     */
    public void Worker() {
        // TODO implement here
    }

    /**
     * @param person
     */
    

    public float getSalary() {
		return salary;
	}
    
	public Worker(int id, String name, String surname, String email, String password, float salary, int workingHours,
		byte[] myPicture, String address, int managerId) {
		super(id, name, surname, email, password);
		this.salary = salary;
		this.workingHours = workingHours;
		this.myPicture = myPicture;
		this.address = address;
		this.managerId = managerId;
	}

	public Worker(int id, String name, String surname, String email, String password) {
		super(id, name, surname, email, password);
		this.salary = salary;
		this.myPicture = myPicture;
		this.address = address;
		this.managerId = managerId;
	}
	
	public Worker(String name, String surname, String email, float salary, byte[] picture, String password,int workingHours, String address) {
		super(name, surname, email, password);
		this.salary = salary;
		this.myPicture = picture;
		this.address = address;
		this.workingHours=workingHours;
	}
	
	public Worker(String name, String surname, String email, float salary, byte[] myPicture, String address, int managerId) {
		super(name, surname, email);
		this.salary = salary;
		this.myPicture = myPicture;
		this.address = address;
		this.managerId = managerId;
	}

	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}

	public Worker(int id, String name, String surname, String email, String password,int workingHouurs, byte[] picture) {
		super(id, name, surname, email, password);
		this.workingHours=workingHouurs;
		this.myPicture=picture;
	}
	
	public Worker(int id, String name, String surname, String email, String password,int workingHouurs) {
		super(id, name, surname, email, password);
		this.salary = salary;
		this.myPicture = myPicture;
		this.address = address;
		this.managerId = managerId;
		this.workingHours=workingHouurs;
	}

	public int getWorkingHours() {
		return workingHours;
	}

	public String getAddress() {
		return address;
	}

	public int getManagerId() {
		return managerId;
	}
	


	

	public Worker(float salary, int workingHours, byte[] myPicture, String address, int managerId) {
		super();
		this.salary = salary;
		this.workingHours = workingHours;
		this.myPicture = myPicture;
		this.address = address;
		this.managerId = managerId;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public byte[] getMyPicture() {
		return myPicture;
	}

	public void setMyPicture(byte[] myPicture) {
		this.myPicture = myPicture;
	}

	/**
     * 
     */
    public void updateInfo() {
        // TODO implement here
    }

}