package klase;


import java.util.*;

/**
 * 
 */
public class Movie {

   
    public Movie() {
    }
    private int id;
    private String movieName;
    private String genre	;
    private String duration;
    private int id_manager;
    
    public int getId_manager() {
		return id_manager;
	}

	public void setId_manager(int id_manager) {
		this.id_manager = id_manager;
	}

	public Movie(int id, String movieName, String genre, String duration) {
		super();
		this.id=id;
		this.movieName = movieName;
		this.genre = genre;
		this.duration = duration;
	}
    
    public Movie(String movieName, String genre, String duration) {
		super();
		this.movieName = movieName;
		this.genre = genre;
		this.duration = duration;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMovieName() {
		return movieName;
	}

	public String getGenre() {
		return genre;
	}

	public String getDuration() {
		return duration;
	}

	public void updateDetails() {
        // TODO implement here
    }

}