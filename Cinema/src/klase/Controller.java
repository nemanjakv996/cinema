package klase;


import java.util.*;

import java.sql.SQLException;
import javax.swing.JFrame;

import gui.ManagerCinema;
import gui.WorkerCinema;

/**
 * 
 */
public class Controller {

    /**
     * Default constructor
     */
	
//DAO
	DAO db= new DAO();//mora da bude samo jedan	
	
	
//Form
	Form form=new Form();
	
	
    public Controller() {
    }

    /**
     * @param id 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public ArrayList <Worker> deleteWorker(int id) throws ClassNotFoundException, SQLException {
        ArrayList <Worker> workers =db.deleteWorker(id, currentId);
        return workers;
    }

    /**
     * @param workerIn 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public void addWorker(Worker workerIn) throws ClassNotFoundException, SQLException {
    	workerIn.setManagerId(currentId);
        db.addWorker(workerIn);
        form.showMessage("Inserted successfully.");
    }

    /**
     * @param id 
     * @return
     */
    public User update(int id) {
        // TODO implement here
        return null;
    }

    /**
     * @param email
     */
    public void sendEmail(String email) {
        // TODO implement here
    }

    /**
     * @param password 
     * @param passwordAgain 
     * @param id
     */
    public void changePassword(String password, String passwordAgain, int id) {
        // TODO implement here
    }

    /**
     * @param worker 
     * @param workerList
     */
    public void updateWorker(Worker worker, int id) {
        ArrayList<Worker> workers = db.updateWorker(worker, currentId, id);
        if(workers!=null) {
        	form.showMessage("Successfully updated");
        }else if(workers==null) {
        	form.showErrorMessage("There's been an error.", " ");
        }
    }

    /**
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public ArrayList <Projection> getAllProjection() throws ClassNotFoundException, SQLException {
       	ArrayList<Projection> projections = db.getAllProjection();
        return projections;
    }
    
    public ArrayList <Projection> getAllProjection(String search) throws ClassNotFoundException, SQLException {
       	ArrayList<Projection> projections = db.getAllProjection(search);
        return projections;
    }
    
    public ArrayList <Projection> searchAllProjectionsId(int id) throws ClassNotFoundException, SQLException {
       	ArrayList<Projection> projections = db.searchAllProjectionId(id);
        return projections;
    }
    
    public int getHallId(String hallName) throws SQLException, ClassNotFoundException {
    	int id=db.getHallId(hallName);
    	return id;
    }
    /**
     * @return
     */
    public List <User> allRegistrations() {
        // TODO implement here
        return null;
    }

    /**
     * @return
     */
    public ArrayList <Worker> getAllWorkers() {
        ArrayList<Worker>workers=new ArrayList<Worker>();
        workers=db.getAllWorkers(currentId);
        return workers;
    }
    
    public ArrayList <Worker> getAllWorkers(String search) throws ClassNotFoundException, SQLException {
        ArrayList<Worker>workers=new ArrayList<Worker>();
        workers=db.getAllWorkers(search);
        return workers;
    }
    
    public ArrayList <Worker> searchAllWorkersId(int id) throws ClassNotFoundException, SQLException {
        ArrayList<Worker>workers=new ArrayList<Worker>();
        workers=db.searchAllWorkersId(id);
        return workers;
    }

    /**
     * @param email 
     * @param password
     */
    static String currentEmail=null;
    static int currentId=0;
    public void loginDataWorker(String email, String password, JFrame frame) {
        Worker worker=db.loginWorker(email, password, frame);
        if(worker!=null) {
        	currentEmail=email;
        	form.showMessage("Email and password are correct!");
        	WorkerCinema workerGui=new WorkerCinema();
			workerGui.main(null);
			frame.setVisible(false);
        }
        else if(worker==null) {
        	form.showErrorMessage("Username and password are not correct!\n Try again.", "Login failed");
        }
    }
    
    public void loginDataManager(String email, String password, JFrame frame) {
        Manager manager=db.loginManager(email, password);
        if(manager!=null) {
        	currentEmail=email;
        	form.showMessage("Email and password are correct!");
        	ManagerCinema managerGui=new ManagerCinema();
        	currentId=manager.getId();
			managerGui.main(null);
			frame.setVisible(false);
        }
        else if(manager==null) {
        	form.showErrorMessage("Username and password are not correct!\n Try again.", "Login failed");
        }
    }
    
    public int getCurrentIdManager() {
    	int id=currentId;
    	return id;
    }
    
    /**
     * @param id 
     * @return
     */
    public Manager getManager() {
    	Manager manager=db.getManager(currentEmail);
    	return manager;
    }
    
    public Worker getWorker() {
    	Worker worker=db.getWorker(currentEmail);
    	return worker;
    }
    
    

    /**
     * @param hallIn 
     * @return
     */
    public Hall addHall(Hall hallIn) {
        // TODO implement here
        return null;
    }

    /**
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public ArrayList<Hall> getAllHalls(String search) throws ClassNotFoundException, SQLException {
        ArrayList <Hall> halls=db.getAllHalls(search);
        return halls;
    }

    /**
     * @param hall 
     * @return
     */
    public List <Hall> updateHall(Hall hall) {
        // TODO implement here
        return null;
    }

    /**
     * @param managerIn 
     * @return
     */
    public void updateManagerProfile(int id, String name, String surname, String email, String password, String address, byte[] image) {
        Manager manager=db.updateManagerProfile(id, name, surname,email,password, address, image);
        if(manager==null) {
        	form.showErrorMessage("There's been an error.", " ");
        }
        else if(manager!=null) {
        	form.showMessage("Successfully updated.");
        }
    }

    /**
     * @return
     */
    public List <Projection> getActiveProjections() {
        // TODO implement here
        return null;
    }

    /**
     * @param id 
     * @return
     */
    public List <Ticket> getUserTicketList(int id) {
        // TODO implement here
        return null;
    }

    /**
     * @param seat
     */
    public void buy(Seat seat) {
        // TODO implement here
    }

    /**
     * 
     */
    public void confirmation() {
        // TODO implement here
    }

    /**
     * @param id 
     * @return
     */
    public Worker getCashier() {
        Worker w=db.getCashier(currentEmail);
    	return w;
    }

    /**
     * @param image_worker 
     * @param worker
     */
    public void updateWorkerProfile(int id, String name, String surname, String email, String password, byte[] image_worker) {


            Worker  worker =db.updateWorkerProfile(id, name, surname, email, password,image_worker);
            if(worker==null) {
            	form.showErrorMessage("There's been an error.", " ");
            }
            else if(worker!=null) {
            	form.showMessage("Successfully updated.");
            }
        }
    	
    	
    
    /**
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public ArrayList <User> getAllUsers(String search) throws ClassNotFoundException, SQLException {
        ArrayList<User> userList = new ArrayList<>();
        userList=db.getAllUsers(search);
        return userList;
     }
    
    public ArrayList <User> searchAllUsersId(int id) throws ClassNotFoundException, SQLException {
        ArrayList<User> userList = new ArrayList<>();
        userList=db.searchAllUsers(id);
        return userList;
     }
    
    public ArrayList <User> getAllUsersManager() throws ClassNotFoundException, SQLException {
       ArrayList<User> userList = new ArrayList<>();
       userList=db.getAllUsers(currentId);
       return userList;
    }
    
    public ArrayList <User> getAllUsers() throws ClassNotFoundException, SQLException {
        ArrayList<User> userList = new ArrayList<>();
        userList=db.getAllUsers();
        return userList;
     }
    
    public ArrayList <Movie> getAllMovies(String search) throws ClassNotFoundException, SQLException {
        ArrayList<Movie> movieList = new ArrayList<>();
        movieList=db.getAllMovies(search);
        return movieList;
     }
    
    public ArrayList<Movie> addMovie(Movie movie) throws ClassNotFoundException, SQLException{
    	db.addMovie(movie,currentId);
    	ArrayList<Movie> movies = getAllMovies("all");
    	if(movies==null){
    		form.showErrorMessage("Failed to insert", "Database failed");
    	}
    	return movies;
    }
    
    public ArrayList<Movie> updateMovie(Movie movie) throws ClassNotFoundException, SQLException{
    	db.updateMovie(movie, currentId);
    	ArrayList<Movie> movies=getAllMovies("all");
    	return movies;
    }
    
    public ArrayList <Cinema> getAllCinema(String search) throws ClassNotFoundException, SQLException {
        ArrayList<Cinema> cinemaList = db.getAllCinema(search);
        return cinemaList;
     }

    /**
     * @param id 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public User getUser(int id) throws ClassNotFoundException, SQLException {
     User u = new User();
     u = db.getUserId(id);
        return u;
    }

    /**
     * @param projection 
     * @return
     */
    public int[][] getSeatsStatus(Projection projection) {
        // TODO implement here
        return null;
    }

    /**
     * @param ticketIn 
     * @return
     */
    public Ticket printTicket(Ticket ticketIn) {
        // TODO implement here
        return null;
    }

    /**
     * @param paymentIn 
     * @return
     */
    public MakePayment choosePaymentMethod(MakePayment paymentIn) {
        // TODO implement here
        return null;
    }

    /**
     * @param projectionInactive 
     * @return
     */
    public Projection setInactive(Projection projectionInactive) {
        // TODO implement here
        return null;
    }

    /**
     * @param user 
     * @return
     */
    public int calculateDiscount(User user) {
        // TODO implement here
        return 0;
    }

    /**
     * @param user 
     * @return
     */
    public List<Ticket> getUserTicketList(User user) {
        // TODO implement here
        return null;
    }

    /**
     * @param user 
     * @return
     */
    public List <Projection> getUserProjectionList(User user) {
        // TODO implement here
        return null;
    }

    /**
     * @param user 
     * @param projection
     */
    public void like(User user, Projection projection) {
        // TODO implement here
    }

    /**
     * @param user 
     * @param commentText 
     * @param dateTime 
     * @return
     */
    public String addComment(User user, String commentText, Date dateTime) {
        // TODO implement here
        return "";
    }

    /**
     * @param user
     */
    public void updateUserProfile(User user) {
        
    }
    
    public void updateUser(int id, String name, String surname, String email, String address, int id_manager, byte[] imageUser) {
    	ArrayList<User> users = db.updateUser(id, name, surname, email, address, id_manager, imageUser);
    	if(users!=null) {
    		form.showMessage("Updated successfully");
    	}else {
    		form.showErrorMessage("Some wrong data inserted", "Update failed");
    	}
    }

    /**
     * @param ticket
     */
    public void buyTicket(Ticket ticket) {
        // TODO implement here
    }

    /**
     * @param user
     */
    public void sendRegistrationRequest(User user) {
        // TODO implement here
    }

    /**
     * @param user
     */
    /**
     * @param user
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public void addUser(User user) throws ClassNotFoundException, SQLException {
    	if(user!=null) {
    		form.showMessage("Succesfully added!");
    	db.addUser(user);
    	}else if(user == null) {
    		form.showErrorMessage("Wrong user format!", "Format error");
    	}
    }
	 public int getIdPaymentCard(String password) throws ClassNotFoundException, SQLException {
    	int id=db.getIdPaymentCard(password);
    	return id;
    }

    /**
     * @param id 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public ArrayList <User> deleteUser(User user) throws ClassNotFoundException, SQLException {
        int id=user.getId();
        ArrayList<User>users=db.deleteUser(id, currentId);
        return users;
    }
    
    public byte[] getUserPicture(int id){
    	byte[] picture=db.getUserPicture(id);
    	return picture;
    }

    public byte[] getWorkerPicture(int id){
    	byte[] picture=db.getWorkerPicture(id);
    	return picture;
    }
    /**
     * @param user 
     * @return
     */
    public List <User> updateUser(User user) {
        // TODO implement here
        return null;
    }

    /**
     * @param user 
     * @param amount 
     * @param ticket
     */
    public void refundOnCancelation(User user, float amount, Ticket ticket) {
        // TODO implement here
    }

    /**
     * @param user 
     * @param projection
     */
    public void dislike(User user, Projection projection) {
        // TODO implement here
    }

    /**
     * @param projection
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public void addProjection(Projection projection) throws ClassNotFoundException, SQLException {
        Projection projectionTemp = db.addProjection(projection);
        if(projectionTemp==null) {
        	form.showErrorMessage("Insert aborted", "Database error");
        }else if(projectionTemp!=null) {
        form.showMessage("Insert successfully");
        }
    }

    /**
     * @param id 
     * @return
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public ArrayList <Projection> deleteProjection(int id) throws ClassNotFoundException, SQLException {
        ArrayList<Projection> projections = db.deleteProjection(id);
        return projections;
    }

    /**
     * @param projection 
     * @return
     */
    public ArrayList <Projection> updateProjection(Projection projection) {
       ArrayList<Projection> projections= db.updateProjection(projection);
       if(projections!=null) {
    	   form.showMessage("Successfully updated");
       }else {
    	   form.showErrorMessage("No data inserted", "Database error");
       }
       return projections;
        
    }

	public ArrayList<Integer> getAllSeats(int selectedRow) throws ClassNotFoundException, SQLException {
		ArrayList<Integer> niz =db.getAllSeats(selectedRow);
		return niz;
	}

	public Projection getProjection(int selectedRow) {
		Projection p = db.getProjection(selectedRow);
		return p;
	}

	public int getMovieID(String name_movie) throws ClassNotFoundException, SQLException {
		int id=db.getMovieId(name_movie);
		return id;
	}

	public void addTicket(Ticket t) throws ClassNotFoundException, SQLException {
		if(t!=null) {
    		
    	db.addTicket(t);
    	form.showMessage("Succesfully added in DB!");
    	}else if(t == null) {
    		form.showErrorMessage("Wrong user format!", "Format error");
    	}
		
		
	}

	

}