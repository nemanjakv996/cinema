package klase;


import java.awt.image.BufferedImage;
import java.util.*;

/**
 * 
 */
public class Manager extends Person {

    /**
     * Default constructor
     */
    public Manager() {
    }

    /**
     * 
     */
    private String position;

    /**
     * 
     */
    private float salary;

    private int workingHours;
    

	private byte[] myPicture;

    private String address;
    
    public String getPosition() {
		return position;
	}



	public void setPosition(String position) {
		this.position = position;
	}



	public float getSalary() {
		return salary;
	}



	public void setSalary(float salary) {
		this.salary = salary;
	}



	public int getWorkingHours() {
		return workingHours;
	}



	public void setWorkingHours(int workingHours) {
		this.workingHours = workingHours;
	}



	public byte[] getMyPicture() {
		return myPicture;
	}



	public void setMyPicture(byte[] myPicture) {
		this.myPicture = myPicture;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public Manager(int id, String name, String surname, String email, String password, String position, float salary,
			int workingHours, byte[] myPicture, String address) {
		super(id, name, surname, email, password);
		this.position = position;
		this.salary = salary;
		this.workingHours = workingHours;
		this.myPicture = myPicture;
		this.address = address;
	}



    /**
     * 
     */
    public void Manager() {
        // TODO implement here
    }

    /**
     * @param worker
     */
    public void addWorker(Worker worker) {
        // TODO implement here
    }

    /**
     * 
     */
    public void editWorker() {
        // TODO implement here
    }

    /**
     * 
     */
    public void updateWorker() {
        // TODO implement here
    }

    /**
     * 
     */
    public void deleteWorker() {
        // TODO implement here
    }

    /**
     * @param user
     */
    public void addUser(User user) {
        // TODO implement here
    }

    /**
     * @param user
     */
    public void editUser(User user) {
        // TODO implement here
    }

    /**
     * @param user
     */
    public void updateUser(User user) {
        // TODO implement here
    }

    /**
     * @param user
     */
    public void deleteUser(User user) {
        // TODO implement here
    }

    /**
     * @param person 
     * @param position
     */
    public void Manager(Person person, String position) {
        // TODO implement here
    }

    /**
     * 
     */
    public void addMovieRecords() {
        // TODO implement here
    }

    /**
     * 
     */
    public void updateMovieRecords() {
        // TODO implement here
    }

    /**
     * 
     */
    public void deleteMovieRecords() {
        // TODO implement here
    }

    /**
     * 
     */
    public void approveRegeistration() {
        // TODO implement here
    }

}