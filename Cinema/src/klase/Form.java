package klase;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.*;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;



/**
 * 
 */
public class Form extends JComponent{
	//Controller
	Controller controller;
    /**
     * Default constructor
     */
    public Form() {
    }
    
    /**
     * 
     */


    /**
     * 
     */
    public void showMessage(String message) {
        JOptionPane.showMessageDialog(null,message);
    }

    /**
     * 
     */
    public void showErrorMessage(String message, String header) {
        JOptionPane.showMessageDialog(null, message,header, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * 
     */
    public void showComment() {
        // TODO implement here
    }

}