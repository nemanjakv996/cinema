package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import klase.Controller;
import klase.DAO;
import klase.Form;


public class CinemaFront {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CinemaFront window = new CinemaFront();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CinemaFront() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Form form = new Form();
		frame = new JFrame("Login");
		frame.setBackground(Color.WHITE);
		frame.getContentPane().setBackground(Color.BLACK);
		frame.setBounds(100, 100, 450, 334);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
	    frame.setLocation(x, y);
	    //dao
	    DAO dao=new DAO();
	    //Controller
	    Controller controller=new Controller();
		//tabbedpane
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBackground(new Color(220, 20, 60));
		tabbedPane.setForeground(Color.BLACK);
		tabbedPane.setBounds(10, 11, 414, 273);
		frame.getContentPane().add(tabbedPane);
		
		//panels
		JPanel panel1 = new JPanel();
		panel1.setBorder(new LineBorder(Color.RED));
		panel1.setBackground(Color.LIGHT_GRAY);
		tabbedPane.addTab("Cashier", null, panel1, null);
		panel1.setLayout(null);
		
		JPanel panel2 = new JPanel();
		panel2.setBorder(new LineBorder(Color.RED));
		panel2.setBackground(Color.GRAY);
		tabbedPane.addTab("Manager", null, panel2, null);
		panel2.setLayout(null);
		//labels
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setBounds(156, 32, 63, 14);
		panel1.add(lblEmail);
		
		JLabel lblMail = new JLabel("Email:");
		lblMail.setForeground(Color.WHITE);
		lblMail.setBounds(157, 32, 68, 14);
		panel2.add(lblMail);
		
		JLabel lblStudentPassword = new JLabel("Password");
		lblStudentPassword.setForeground(Color.WHITE);
		lblStudentPassword.setBounds(156, 86, 63, 14);
		panel1.add(lblStudentPassword);
		
		JLabel lblIsEmpty = new JLabel("");
		lblIsEmpty.setForeground(new Color(255, 0, 0));
		lblIsEmpty.setBounds(184, 21, 68, 14);
		panel2.add(lblIsEmpty);
		
		JLabel lblIsEmptyPass = new JLabel("");
		lblIsEmptyPass.setForeground(new Color(255, 0, 0));
		lblIsEmptyPass.setBounds(184, 77, 53, 14);
		panel2.add(lblIsEmptyPass);
		
		JLabel lblLibrarianPassword = new JLabel("Password");
		lblLibrarianPassword.setForeground(Color.WHITE);
		lblLibrarianPassword.setBounds(157, 88, 68, 14);
		panel2.add(lblLibrarianPassword);
		
		JLabel lblIsEmptyEmail = new JLabel("is empty");
		lblIsEmptyEmail.setForeground(Color.RED);
		lblIsEmptyEmail.setBounds(334, 60, 63, 14);
		panel1.add(lblIsEmptyEmail);
		
		JLabel lblIsEmptyPassword = new JLabel("is empty");
		lblIsEmptyPassword.setForeground(Color.RED);
		lblIsEmptyPassword.setBounds(334, 116, 63, 14);
		panel1.add(lblIsEmptyPassword);
		lblIsEmptyPassword.setVisible(false);
		lblIsEmptyEmail.setVisible(false);
		
		JLabel lblIsEmptyManagerEmail = new JLabel("is empty");
		lblIsEmptyManagerEmail.setForeground(Color.RED);
		lblIsEmptyManagerEmail.setBounds(335, 60, 62, 14);
		panel2.add(lblIsEmptyManagerEmail);
		lblIsEmptyManagerEmail.setVisible(false);
		
		JLabel lblIsEmptyManagerPassword = new JLabel("is empty");
		lblIsEmptyManagerPassword.setForeground(Color.RED);
		lblIsEmptyManagerPassword.setBounds(335, 116, 62, 14);
		panel2.add(lblIsEmptyManagerPassword);
		lblIsEmptyManagerPassword.setVisible(false);
		
		//text fields
		JTextField txtCashierEmail = new JTextField();
		txtCashierEmail.setBounds(156, 57, 168, 20);
		panel1.add(txtCashierEmail);
		txtCashierEmail.setColumns(10);
		
		JTextField txtManagerEmail = new JTextField();
		txtManagerEmail.setBounds(157, 57, 168, 20);
		panel2.add(txtManagerEmail);
		txtManagerEmail.setColumns(10);
		//password fields
		JPasswordField txtCashierPassword = new JPasswordField();
		txtCashierPassword.setBounds(156, 113, 168, 20);
		panel1.add(txtCashierPassword);
		
		JPasswordField txtManagerPassword = new JPasswordField();
		txtManagerPassword.setBounds(157, 113, 168, 20);
		panel2.add(txtManagerPassword);
		
		//button
		JButton btnEnterCashier = new JButton("");
		btnEnterCashier.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\User-Interface-Enter-icon.png"));
		btnEnterCashier.setBounds(202, 157, 76, 73);
		panel1.add(btnEnterCashier);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Cinema.png"));
		lblNewLabel.setBounds(0, 0, 316, 256);
		panel1.add(lblNewLabel);
		
		
		
		JButton btnEnterManager = new JButton("");
		btnEnterManager.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\User-Interface-Enter-icon.png"));		
		btnEnterManager.setBounds(203, 157, 76, 73);
		panel2.add(btnEnterManager);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Cinema.png"));
		lblNewLabel_1.setBounds(0, 0, 256, 256);
		panel2.add(lblNewLabel_1);
		
		//action and action listener
		Action actionLoginCashier=new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
			String email=txtCashierEmail.getText();
			String password=txtCashierPassword.getText();
			if(valEmail(email)) {
			controller.loginDataWorker(email, password, frame);
			txtCashierEmail.setText("");
			txtCashierPassword.setText("");
			}else {
				form.showErrorMessage("Wrong email format", "Login failed");
				txtCashierEmail.setText("");
				txtCashierPassword.setText("");
			}
		};
	};
			btnEnterCashier.addActionListener(actionLoginCashier);
			txtCashierEmail.addActionListener(actionLoginCashier);
			txtCashierPassword.addActionListener(actionLoginCashier);
			txtCashierEmail.addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent arg0) {
					
				}
				
				@Override
				public void keyReleased(KeyEvent arg0) {
					if(txtCashierEmail.getText().length()==0) {
						lblIsEmptyEmail.setVisible(true);
					}
					else if(txtCashierEmail.getText().length()!=0) {
						lblIsEmptyEmail.setVisible(false);
					}
				}

				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
			txtCashierPassword.addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent arg0) {
					
				}
				
				@Override
				public void keyReleased(KeyEvent arg0) {
					if(txtCashierPassword.getText().length()==0) {
						lblIsEmptyPassword.setVisible(true);
					}
					else if(txtCashierPassword.getText().length()!=0) {
						lblIsEmptyPassword.setVisible(false);
					}
				}
				
				@Override
				public void keyPressed(KeyEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		Action actionLoginManager=new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String email=txtManagerEmail.getText();
				String password=txtManagerPassword.getText();
				if(valEmail(email)) {
				controller.loginDataManager(email, password, frame);
				txtManagerEmail.setText("");
				txtManagerPassword.setText("");
				}else {
					form.showErrorMessage("Wrong email format", "Login failed");
					txtManagerEmail.setText("");
					txtManagerPassword.setText("");
				}
			}
		};	
		btnEnterManager.addActionListener(actionLoginManager);
		txtManagerEmail.addActionListener(actionLoginManager);
		txtManagerPassword.addActionListener(actionLoginManager);
		txtManagerEmail.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				if(txtManagerEmail.getText().length()==0) {
					lblIsEmptyManagerEmail.setVisible(true);
				}
				else if(txtManagerEmail.getText().length()!=0) {
					lblIsEmptyManagerEmail.setVisible(false);
				}
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		txtManagerPassword.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				if(txtManagerPassword.getText().length()==0) {
					lblIsEmptyManagerPassword.setVisible(true);
				}
				else if(txtManagerPassword.getText().length()!=0) {
					lblIsEmptyManagerPassword.setVisible(false);
				}
				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	public static boolean valEmail(String email) {
		String emailRegex="^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
		Pattern p= Pattern.compile(emailRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcher= p.matcher(email);
		return matcher.find();
	}
}
