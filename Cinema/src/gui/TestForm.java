package gui;

import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import klase.Controller;
import klase.Form;
import klase.Manager;
import klase.User;

import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.Panel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class TestForm {

	private JFrame frame;
	private JTextField textFieldHoursUpdate;
	private JTextField textFieldMinutesUpdate;
	private JTextField textFieldMovieSearchUpdate;
	private JTable tableMovieUpdate;
	private JTextField textFieldHallUpdate;
	private JTable tableHallUpdate;
	private JTextField textFieldIdUpdate;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestForm window = new TestForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TestForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame=new JFrame();
		JPanel updateProjectionPanel=new JPanel();
		updateProjectionPanel.setLayout(null);
		updateProjectionPanel.setBounds(0, 0, 569, 295);
		frame.getContentPane().add(updateProjectionPanel);
		
		JLabel lblDateAndTimeUpdate = new JLabel("date:");
		lblDateAndTimeUpdate.setBounds(10, 11, 54, 14);
		updateProjectionPanel.add(lblDateAndTimeUpdate);
		
		JComboBox comboBoxDayUpdate = new JComboBox();
		comboBoxDayUpdate.setBounds(74, 7, 54, 22);
		updateProjectionPanel.add(comboBoxDayUpdate);
		
		JComboBox comboBoxMonthUpdate = new JComboBox();
		comboBoxMonthUpdate.setBounds(138, 7, 54, 22);
		updateProjectionPanel.add(comboBoxMonthUpdate);
		
		JComboBox comboBoxYearUpdate = new JComboBox();
		comboBoxYearUpdate.setBounds(202, 7, 92, 22);
		updateProjectionPanel.add(comboBoxYearUpdate);
		
		JLabel lblDdmmyyyuUpdate = new JLabel("dd/mm/yyyy");
		lblDdmmyyyuUpdate.setBounds(304, 11, 83, 14);
		updateProjectionPanel.add(lblDdmmyyyuUpdate);
		
		JLabel lblTimeUpdate = new JLabel("Time:");
		lblTimeUpdate.setBounds(10, 43, 46, 14);
		updateProjectionPanel.add(lblTimeUpdate);
		
		textFieldHoursUpdate = new JTextField();
		textFieldHoursUpdate.setBounds(74, 40, 54, 20);
		updateProjectionPanel.add(textFieldHoursUpdate);
		textFieldHoursUpdate.setColumns(10);
		
		textFieldMinutesUpdate = new JTextField();
		textFieldMinutesUpdate.setBounds(138, 40, 54, 20);
		updateProjectionPanel.add(textFieldMinutesUpdate);
		textFieldMinutesUpdate.setColumns(10);
		
		JLabel lblHhmmUpdate = new JLabel("HH:mm");
		lblHhmmUpdate.setBounds(202, 43, 60, 14);
		updateProjectionPanel.add(lblHhmmUpdate);
		
		JLabel lblMovieUpdate = new JLabel("movie:");
		lblMovieUpdate.setBounds(10, 93, 46, 14);
		updateProjectionPanel.add(lblMovieUpdate);
		
		textFieldMovieSearchUpdate = new JTextField();
		textFieldMovieSearchUpdate.setBounds(74, 90, 220, 20);
		updateProjectionPanel.add(textFieldMovieSearchUpdate);
		textFieldMovieSearchUpdate.setColumns(10);
		
		JScrollPane scrollPaneMovieUpdate = new JScrollPane();
		scrollPaneMovieUpdate.setBounds(10, 121, 284, 126);
		updateProjectionPanel.add(scrollPaneMovieUpdate);
		
		tableMovieUpdate = new JTable();
		scrollPaneMovieUpdate.setViewportView(tableMovieUpdate);
		tableMovieUpdate.setBounds(10, 121, 361, 126);
		
		JButton btnSaveUpdateProj = new JButton("Save");
		btnSaveUpdateProj.setBackground(Color.ORANGE);
		btnSaveUpdateProj.setBounds(74, 261, 89, 23);
		updateProjectionPanel.add(btnSaveUpdateProj);
		
		JButton btnBackUpdateProj = new JButton("Back");
		btnBackUpdateProj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		btnBackUpdateProj.setBounds(376, 261, 89, 23);
		updateProjectionPanel.add(btnBackUpdateProj);
		
		JLabel lblActiveUpdate = new JLabel("active:");
		lblActiveUpdate.setBounds(10, 68, 46, 14);
		updateProjectionPanel.add(lblActiveUpdate);
		
		JCheckBox chckbxYesUpdate = new JCheckBox("yes");
		chckbxYesUpdate.setBounds(74, 64, 60, 23);
		updateProjectionPanel.add(chckbxYesUpdate);
		
        JScrollPane scrollPaneHallUpdate = new JScrollPane();
        scrollPaneHallUpdate.setBounds(323, 121, 219, 126);
        updateProjectionPanel.add(scrollPaneHallUpdate);
        
        tableHallUpdate = new JTable();
        tableHallUpdate.setBounds(250, 121, 171, 126);
        scrollPaneHallUpdate.setViewportView(tableHallUpdate);
        
        JLabel lblHallUpdate = new JLabel("hall:");
        lblHallUpdate.setBounds(323, 93, 46, 14);
        updateProjectionPanel.add(lblHallUpdate);
        
        textFieldHallUpdate = new JTextField();
        textFieldHallUpdate.setBounds(371, 90, 171, 20);
        updateProjectionPanel.add(textFieldHallUpdate);
        textFieldHallUpdate.setColumns(10);
        
        textFieldIdUpdate = new JTextField();
        textFieldIdUpdate.setBounds(430, 40, 46, 20);
        updateProjectionPanel.add(textFieldIdUpdate);
        textFieldIdUpdate.setColumns(10);

        frame.setBounds(100, 100, 585, 334);
		frame.setTitle("Update projection");
	    frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
	    frame.setLocation(x, y);
		
	}
}