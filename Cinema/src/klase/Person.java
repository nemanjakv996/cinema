package klase;


import java.util.*;

/**
 * 
 */
public class Person {

    /**
     * Default constructor
     */
    public Person() {
    
    }
  
    private int id;
    private String name;
    private String surname;
    private String email;
    private String password;

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Person(String name, String surname, String email, String password) {
		super();
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.password = password;
	}

	public Person(int id, String name, String surname, String email, String password) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.password = password;
	}

	public Person(String name, String surname, String email) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.surname = surname;
		this.email = email;
	}

	/**
     * 
     */
   
    public void Login() {
        // TODO implement here
    }

    /**
     * 
     */
    public void Logout() {
        // TODO implement here
    }

}