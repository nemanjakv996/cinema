package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTable;



import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.ScrollPane;
import java.awt.Toolkit;
import java.awt.Point;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import klase.Cinema;
import klase.Controller;
import klase.Form;
import klase.Hall;
import klase.Manager;
import klase.Movie;
import klase.Projection;
import klase.User;
import klase.Worker;

import javax.swing.JRadioButton;
import javax.swing.JProgressBar;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.ImagingOpException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.Font;


public class ManagerCinema {

	private JFrame frame;
	private JTextField textFieldSearchFront;
	private JTable table;
	byte[] image_manager= null;
	String fileName;
	byte[] image_user=null;
	String fileNameUser;
	byte[] image_worker=null;
	String fileNameWorker;
	private JTextField textFieldNameUpdate;
	private JTextField textFieldSurnameUpdate;
	private JTextField textFieldAddressUpdate;
	private JTextField textFieldEmailUpdate;
	private JTextField textFieldManagerIdUpdate;
	private JTextField textFieldSalaryUpdate;
	private JTable tableSearch;
	private JTextField textFieldSearchId;
	private JTextField textFieldMovieName;
	private JTextField textFieldGenreName;
	private JTextField textFieldDuration;
	private JTable tableMovie;
	private JTextField textFieldMovieSearch;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManagerCinema window = new ManagerCinema();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ManagerCinema() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//Form class
		Form form=new Form();
		//Controller class
		Controller controller = new Controller();
		//frame setup
		
		frame = new JFrame("Manager");
		frame.setBounds(100, 100, 464, 334);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
	    frame.setLocation(x, y);
	    frame.setResizable(false);
	    
		//panel setup
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBackground(new Color(220, 20, 60));
		tabbedPane.setForeground(Color.BLACK);
		tabbedPane.setBounds(10, 11, 428, 273);
		frame.getContentPane().add(tabbedPane);
		
		ChangeListener changeState=new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent arg0) {
				frame.setBounds(100, 100, 464, 334);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getContentPane().setLayout(null);
				Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
			    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
			    frame.setLocation(x, y);
			    frame.setResizable(false);
				
			}
		};
		
		tabbedPane.addChangeListener(changeState);
		
		JPanel myProfilePanel = new JPanel();
		myProfilePanel.setBounds(0, 0, 448, 295);
		frame.getContentPane().add(myProfilePanel);
		myProfilePanel.setVisible(false);
		
		JPanel addUserPanel = new JPanel();
		addUserPanel.setBounds(0, 0, 448, 295);
		frame.getContentPane().add(addUserPanel);
		addUserPanel.setVisible(false);
		
		JPanel addCashierPanel = new JPanel();
		addCashierPanel.setBounds(0, 0, 419, 258);
		frame.getContentPane().add(addCashierPanel);
		addCashierPanel.setVisible(false);
		
		JPanel addProjectionPanel = new JPanel();
		addProjectionPanel.setBounds(0, 0, 569, 295);
		frame.getContentPane().add(addProjectionPanel);
		addProjectionPanel.setVisible(false);
		
		JPanel panelFront = new JPanel();
		tabbedPane.addTab("Front Page", null, panelFront, null);
		panelFront.setLayout(null);
		
		JPanel panelCRUD = new JPanel();
		tabbedPane.addTab("CRUD page", null, panelCRUD, null);
		panelCRUD.setLayout(null);
		
		ScrollPane scrollPaneCRUD = new ScrollPane();
		scrollPaneCRUD.setBounds(10, 35, 259, 200);
		panelCRUD.add(scrollPaneCRUD);
		
		tableSearch = new JTable();
		
		JScrollPane scrollPaneSearch = new JScrollPane(tableSearch, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPaneSearch.setBounds(112, 42, 301, 192);
		panelFront.add(scrollPaneSearch);
		
		JPanel updateProjectionPanel = new JPanel();
		updateProjectionPanel.setLayout(null);
		updateProjectionPanel.setBounds(0, 0, 569, 295);
		frame.getContentPane().add(updateProjectionPanel);
		updateProjectionPanel.setVisible(false);
		
		scrollPaneSearch.setViewportView(tableSearch);
		tableSearch.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		JLabel lblSearch = new JLabel("Search:");
		lblSearch.setBounds(137, 17, 46, 14);
		panelFront.add(lblSearch);
		
		//JButton setup
		JButton btnAdd = new JButton("Add");
		btnAdd.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Add-User-icon.png"));
		btnAdd.setBounds(295, 11, 105, 23);
		panelCRUD.add(btnAdd);
		
		JButton btnRemove = new JButton("Remove");
		btnRemove.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\userRemove.png"));
		btnRemove.setBounds(293, 45, 107, 23);
		panelCRUD.add(btnRemove);
		
		JButton btnShowAll = new JButton("Show all");
		btnShowAll.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\showUser.png"));
		btnShowAll.setBounds(295, 113, 105, 23);
		panelCRUD.add(btnShowAll);
		
		//update panel show
		JPanel updateUserPanel = new JPanel();
		updateUserPanel.setBounds(455, 11, 355, 273);
		frame.getContentPane().add(updateUserPanel);
		updateUserPanel.setLayout(null);
		updateUserPanel.setVisible(false);
		
		JLabel lblNameUpdate = new JLabel("name:");
		lblNameUpdate.setBounds(10, 32, 64, 14);
		updateUserPanel.add(lblNameUpdate);
		
		JLabel lblSurnameUpdate = new JLabel("surname:");
		lblSurnameUpdate.setBounds(10, 60, 64, 14);
		updateUserPanel.add(lblSurnameUpdate);
		
		JLabel lblAddressUpdate = new JLabel("address:");
		lblAddressUpdate.setBounds(10, 91, 64, 14);
		updateUserPanel.add(lblAddressUpdate);
		
		JLabel lblEmailUpdate = new JLabel("email:");
		lblEmailUpdate.setBounds(10, 121, 64, 14);
		updateUserPanel.add(lblEmailUpdate);
		
		JLabel lblManagerIdUpdate = new JLabel("manager id:");
		lblManagerIdUpdate.setBounds(10, 152, 74, 14);
		updateUserPanel.add(lblManagerIdUpdate);
		
		JLabel lblResetPassword = new JLabel("reset password?");
		lblResetPassword.setBounds(245, 186, 100, 14);
		updateUserPanel.add(lblResetPassword);
		
		textFieldNameUpdate = new JTextField();
		textFieldNameUpdate.setBounds(84, 29, 134, 20);
		updateUserPanel.add(textFieldNameUpdate);
		textFieldNameUpdate.setColumns(10);
		
		textFieldSurnameUpdate = new JTextField();
		textFieldSurnameUpdate.setBounds(84, 57, 134, 20);
		updateUserPanel.add(textFieldSurnameUpdate);
		textFieldSurnameUpdate.setColumns(10);
		
		textFieldAddressUpdate = new JTextField();
		textFieldAddressUpdate.setBounds(84, 88, 134, 20);
		updateUserPanel.add(textFieldAddressUpdate);
		textFieldAddressUpdate.setColumns(10);
		
		textFieldEmailUpdate = new JTextField();
		textFieldEmailUpdate.setBounds(84, 118, 134, 20);
		updateUserPanel.add(textFieldEmailUpdate);
		textFieldEmailUpdate.setColumns(10);
		
		textFieldManagerIdUpdate = new JTextField();
		textFieldManagerIdUpdate.setBounds(84, 149, 36, 20);
		updateUserPanel.add(textFieldManagerIdUpdate);
		textFieldManagerIdUpdate.setColumns(10);
		
		JButton btnSaveUpdate = new JButton("Save");
		btnSaveUpdate.setBounds(149, 228, 89, 23);
		updateUserPanel.add(btnSaveUpdate);
		
		JButton button = new JButton("<<");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setBounds(100, 100, 464, 334);
				updateUserPanel.setVisible(false);
				Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
			    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
			    frame.setLocation(x, y);
			}
		});
		button.setBounds(10, 228, 49, 23);
		updateUserPanel.add(button);
		
		JLabel lblShowLess = new JLabel("Show less");
		lblShowLess.setBounds(69, 232, 70, 14);
		updateUserPanel.add(lblShowLess);
		
		JLabel lblUpdatePanel = new JLabel("Update panel");
		lblUpdatePanel.setBounds(10, 7, 89, 14);
		updateUserPanel.add(lblUpdatePanel);
		
		JLabel labelPictureUpdate = new JLabel("");
		labelPictureUpdate.setBounds(238, 32, 107, 103);
		updateUserPanel.add(labelPictureUpdate);
		
		JButton btnPictureUpdate = new JButton("Picture");
		btnPictureUpdate.setBounds(248, 148, 89, 23);
		updateUserPanel.add(btnPictureUpdate);
		Action saveImageUserUpdate=new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				JFileChooser choosePicture = new JFileChooser();
				choosePicture.showOpenDialog(null);
				File file=choosePicture.getSelectedFile();
				fileNameUser=file.getAbsolutePath();
				ImageIcon imageIcon=new ImageIcon(new ImageIcon(fileNameUser).getImage().getScaledInstance(labelPictureUpdate.getWidth(), labelPictureUpdate.getHeight(), Image.SCALE_SMOOTH));
				labelPictureUpdate.setIcon(imageIcon);
				try {
					File image=new File(fileNameUser);
					FileInputStream fis= new FileInputStream(image);
					ByteArrayOutputStream bos=new ByteArrayOutputStream();
					byte[] buf=new byte[1024];
					for(int readNum; (readNum=fis.read(buf)) != -1;) {
						bos.write(buf,0,readNum);
					}
					image_user=bos.toByteArray();
				}
				catch(Exception e) {
					JOptionPane.showMessageDialog(null, e);
				}
		}
		};
		btnPictureUpdate.addActionListener(saveImageUserUpdate);
		
		JLabel lblSalaryUpdate = new JLabel("salary");
		lblSalaryUpdate.setBounds(10, 186, 64, 14);
		updateUserPanel.add(lblSalaryUpdate);
		lblSalaryUpdate.setVisible(false);
		
		textFieldSalaryUpdate = new JTextField();
		textFieldSalaryUpdate.setBounds(83, 183, 64, 20);
		updateUserPanel.add(textFieldSalaryUpdate);
		textFieldSalaryUpdate.setColumns(10);
		textFieldSalaryUpdate.setVisible(false);
		
		JButton btnMyProfile = new JButton("My Profile");
		btnMyProfile.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\profile-icon.png"));
		btnMyProfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
		    	//panels and frame setup
		    	tabbedPane.setVisible(false);
				tabbedPane.setEnabled(false);
				frame.getContentPane().add(myProfilePanel);
				frame.setTitle("My profile");
				frame.setBounds(100, 100, 440, 328);
			    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
			    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
			    frame.setLocation(x, y);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				myProfilePanel.setLayout(null);
				
				//picture setup
				
				
				//JLabel setup
				JLabel lblName = new JLabel("Name:");
				lblName.setBounds(10, 39, 72, 14);
				myProfilePanel.add(lblName);
				
				JLabel lblSurname = new JLabel("Surname:");
				lblSurname.setBounds(10, 64, 72, 14);
				myProfilePanel.add(lblSurname);
				
				JLabel lblId = new JLabel("Id:");
				lblId.setBounds(10, 14, 46, 14);
				myProfilePanel.add(lblId);
				
				JLabel lblEmail = new JLabel("Email:");
				lblEmail.setBounds(10, 89, 72, 14);
				myProfilePanel.add(lblEmail);
				
				JLabel lblPassword = new JLabel("Password:");
				lblPassword.setBounds(10, 114, 96, 14);
				myProfilePanel.add(lblPassword);
				
				JLabel lblWorkingHours = new JLabel("Working hours:");
				lblWorkingHours.setBounds(10, 189, 98, 14);
				myProfilePanel.add(lblWorkingHours);
				
				JLabel lblTime = new JLabel("Time");
				lblTime.setBounds(118, 189, 86, 14);
				myProfilePanel.add(lblTime);
				
				JLabel lblPassAgain = new JLabel("Pass again:");
				lblPassAgain.setBounds(10, 139, 96, 14);
				myProfilePanel.add(lblPassAgain);
				
				JLabel lblAddress = new JLabel("Address:");
				lblAddress.setBounds(10, 164, 72, 14);
				myProfilePanel.add(lblAddress);
				
				JLabel lblPosition = new JLabel("Position:");
				lblPosition.setBounds(10, 214, 72, 14);
				myProfilePanel.add(lblPosition);
				
				JLabel lblPostype = new JLabel("posType");
				lblPostype.setBounds(118, 214, 98, 14);
				myProfilePanel.add(lblPostype);
				
				//JText setup
				JTextField textFieldName = new JTextField();
				textFieldName.setBounds(116, 36, 144, 20);
				myProfilePanel.add(textFieldName);
				textFieldName.setColumns(10);
				
				JTextField textFieldSurname = new JTextField();
				textFieldSurname.setBounds(116, 61, 144, 20);
				myProfilePanel.add(textFieldSurname);
				textFieldSurname.setColumns(10);
				
				JTextField textFieldId = new JTextField();
				textFieldId.setBounds(116, 11, 72, 20);
				myProfilePanel.add(textFieldId);
				textFieldId.setColumns(10);
				
				JTextField textFieldEmail = new JTextField();
				textFieldEmail.setBounds(116, 86, 144, 20);
				myProfilePanel.add(textFieldEmail);
				textFieldEmail.setColumns(10);
				
				JPasswordField textFieldPassword = new JPasswordField();
				textFieldPassword.setBounds(116, 111, 144, 20);
				myProfilePanel.add(textFieldPassword);
				textFieldPassword.setColumns(10);
				
				JPasswordField textFieldPassAgain = new JPasswordField();
				textFieldPassAgain.setBounds(116, 136, 144, 20);
				myProfilePanel.add(textFieldPassAgain);
				textFieldPassAgain.setColumns(10);
				
				JTextField textFieldAddress = new JTextField();
				textFieldAddress.setBounds(118, 161, 142, 20);
				myProfilePanel.add(textFieldAddress);
				textFieldAddress.setColumns(10);
				
				//JcheckBox setup
				JCheckBox chckbxShow = new JCheckBox("Show");
				chckbxShow.setBounds(265, 135, 65, 23);
				myProfilePanel.add(chckbxShow);
				
				//JButton setup
				JLabel lblPicture = new JLabel("");
				lblPicture.setBounds(300, 14, 114, 114);
				myProfilePanel.add(lblPicture);
				lblPicture.setVisible(true);
				
				JButton btnSave = new JButton("Save");
				btnSave.setBackground(Color.ORANGE);
				btnSave.setBounds(71, 247, 89, 23);
				myProfilePanel.add(btnSave);
				
				
				Action updateDetailsManager=new AbstractAction() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						String idStr=textFieldId.getText();
						textFieldId.setEditable(false);
						String name=textFieldName.getText();
						String surname=textFieldSurname.getText();
						String email=textFieldEmail.getText();
						String password=textFieldPassword.getText();
						String passagain=textFieldPassAgain.getText();
						String address=textFieldAddress.getText();
						String idS=idStr.substring(0, idStr.length()-1);
						int id=Integer.parseInt(idS);
						if(textFieldPassAgain.isEnabled() && password.equals(passagain) && valEmail(email)) {
							controller.updateManagerProfile(id, name, surname, email, password, address, image_manager);
						}else if(!textFieldPassAgain.isEnabled() && valEmail(email)){
							controller.updateManagerProfile(id, name, surname, email, password, address, image_manager);
						}else if(textFieldPassAgain.isEnabled() && !password.equals(passagain) && image_manager!=null){
							form.showErrorMessage("Passwords must match.", "Update failed");
						}else if(!valEmail(email)) {
							form.showErrorMessage("Email form is incorrect.", "Update failed");
						}else {
							form.showMessage("Try again.");
						}
						
					}
				};
				
				Action saveImageManager=new AbstractAction() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						JFileChooser choosePicture = new JFileChooser();
						choosePicture.showOpenDialog(null);
						File file=choosePicture.getSelectedFile();
						fileName=file.getAbsolutePath();
						ImageIcon imageIcon=new ImageIcon(new ImageIcon(fileName).getImage().getScaledInstance(lblPicture.getWidth(), lblPicture.getHeight(), Image.SCALE_SMOOTH));
						lblPicture.setIcon(imageIcon);
						try {
							File image=new File(fileName);
							FileInputStream fis= new FileInputStream(image);
							ByteArrayOutputStream bos=new ByteArrayOutputStream();
							byte[] buf=new byte[1024];
							for(int readNum; (readNum=fis.read(buf)) != -1;) {
								bos.write(buf,0,readNum);
							}
							image_manager=bos.toByteArray();
						}
						catch(Exception e) {
							JOptionPane.showMessageDialog(null, e);
						}
				}
				};
				
				btnSave.addActionListener(updateDetailsManager);
				
				JButton btnChoosePicture = new JButton("Picture");
				btnChoosePicture.addActionListener(saveImageManager);
				btnChoosePicture.setBounds(300, 160, 114, 23);
				myProfilePanel.add(btnChoosePicture);
				
				JButton btnBack = new JButton("Back");
				btnBack.setBounds(250, 247, 89, 23);
				myProfilePanel.add(btnBack);
				btnBack.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						tabbedPane.setVisible(true);
						tabbedPane.setEnabled(true);
						frame.setTitle("Manager");
						myProfilePanel.setVisible(false);
						myProfilePanel.setEnabled(false);
						frame.setBounds(100, 100, 464, 334);
					    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
					    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
					    frame.setLocation(x, y);
					}
				});
				
				myProfilePanel.setVisible(true);
				myProfilePanel.setEnabled(true);
				
				Manager manager = controller.getManager();
				if(manager!=null) {
				textFieldId.setText(manager.getId()+" ");
				textFieldId.setEditable(false);
				textFieldName.setText(manager.getName());
				textFieldSurname.setText(manager.getSurname());
				textFieldEmail.setText(manager.getEmail());
				textFieldPassword.setText(manager.getPassword());
				textFieldPassAgain.setEnabled(false);
				textFieldAddress.setText(manager.getAddress());
				lblPostype.setText(manager.getPosition());
				lblTime.setText(manager.getWorkingHours()+"h");
				if(manager.getMyPicture()!=null) {
				ImageIcon imageIcon=new ImageIcon(new ImageIcon(manager.getMyPicture()).getImage().getScaledInstance(lblPicture.getWidth(), lblPicture.getHeight(), Image.SCALE_SMOOTH));
				lblPicture.setIcon(imageIcon);
				}else {
					lblPicture.setText("Insert image here");
				}
				}else if(manager==null) {
					textFieldId.setText("");
					textFieldId.setEditable(false);
					textFieldName.setText("");
					textFieldSurname.setText("");
					textFieldEmail.setText("");
					textFieldPassword.setText("");
					textFieldPassAgain.setEnabled(false);
					textFieldAddress.setText("");
					lblPostype.setText("");
					lblTime.setText(0+"h");
				}
				chckbxShow.addItemListener(new ItemListener() {
					
					@Override
					public void itemStateChanged(ItemEvent arg0) {
						// TODO Auto-generated method stub
						if(chckbxShow.isSelected()) {
							textFieldPassword.setEchoChar((char)0);;
							textFieldPassAgain.setEchoChar((char)0);;
						}else {
							textFieldPassword.setEchoChar('*');
							textFieldPassAgain.setEchoChar('*');
						}
					}
				});
				textFieldPassword.addMouseListener(new MouseAdapter() {
					@Override
		            public void mouseClicked(MouseEvent e){
						textFieldPassAgain.setEnabled(true);
		            }
					
				});
				
			}
		});
		btnMyProfile.setBounds(4, 13, 121, 23);
		panelFront.add(btnMyProfile);
		
		//JTextField setup
		textFieldSearchFront = new JTextField();
		textFieldSearchFront.setBounds(194, 11, 206, 20);
		panelFront.add(textFieldSearchFront);
		textFieldSearchFront.setColumns(10);
		
		JRadioButton rdbtnUser = new JRadioButton("User");
		rdbtnUser.setBounds(4, 90, 73, 23);
		panelFront.add(rdbtnUser);
		
		rdbtnUser.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				ArrayList<User> users;
				try {
					users = controller.getAllUsers("all");
					showAllUsers(tableSearch, users);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		JRadioButton rdbtnWorker = new JRadioButton("Worker");
		rdbtnWorker.setBounds(4, 116, 73, 23);
		panelFront.add(rdbtnWorker);
		
		rdbtnWorker.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				// TODO Auto-generated method stub
				try {
					ArrayList<Worker> workers=controller.getAllWorkers("all");
					showAllWorkers(tableSearch, workers);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		JRadioButton rdbtnProjection = new JRadioButton("Projection");
		rdbtnProjection.setBounds(4, 142, 100, 23);
		panelFront.add(rdbtnProjection);
		
		rdbtnProjection.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				ArrayList<Projection> projections;
				try {
					projections = controller.getAllProjection("all");
					showAllProjectionsFront(tableSearch, projections);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		Action actionSearchFront=new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String search;
				if(rdbtnUser.isSelected()) {
					search=textFieldSearchFront.getText();
					try {
						ArrayList<User> users= controller.getAllUsers(search);
						showAllUsers(tableSearch, users);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}else if(rdbtnWorker.isSelected()) {
					search=textFieldSearchFront.getText();
					try {
						ArrayList<Worker> workers=controller.getAllWorkers(search);
						showAllWorkers(tableSearch, workers);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else if(rdbtnProjection.isSelected()) {
					search=textFieldSearchFront.getText();
					try {
						ArrayList<Projection> projections=controller.getAllProjection(search);
						showAllProjectionsFront(tableSearch, projections);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};
		
		textFieldSearchFront.addActionListener(actionSearchFront);
		
		//JRadioButton setup
		
		
		JRadioButton radioButtonWorkerCRUD = new JRadioButton("Worker");
		radioButtonWorkerCRUD.setBounds(311, 179, 73, 23);
		panelCRUD.add(radioButtonWorkerCRUD);
		
		//adding to buttongroup
		ButtonGroup radios = new ButtonGroup();
		radios.add(rdbtnUser);
		radios.add(rdbtnWorker);
		radios.add(rdbtnProjection);
		
		JRadioButton radioButtonUserCRUD = new JRadioButton("User");
		radioButtonUserCRUD.setBounds(311, 153, 73, 23);
		panelCRUD.add(radioButtonUserCRUD);
		
		JRadioButton radioButtonProjectionCRUD = new JRadioButton("Projection");
		radioButtonProjectionCRUD.setBounds(311, 205, 89, 23);
		panelCRUD.add(radioButtonProjectionCRUD);
		
		JTable table = new JTable();
		table.setBounds(10, 11, 259, 224);
		scrollPaneCRUD.add(table);
		
		//Show all button
		
			btnShowAll.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					//show all users
					if(radioButtonUserCRUD.isSelected()) {
					try {
					ArrayList <User> users=controller.getAllUsersManager();
					showAllUsers(table, users);
					scrollPaneCRUD.add(table);
					}catch(Exception e) {
						JOptionPane.showMessageDialog(null, e);
					}
				}//show all workers
					else if(radioButtonWorkerCRUD.isSelected()) {
					try {
						ArrayList <Worker> workers=controller.getAllWorkers();
						showAllWorkers(table, workers);
						scrollPaneCRUD.add(table);
						}catch(Exception e) {
							JOptionPane.showMessageDialog(null, e);
						}
				}else if(radioButtonProjectionCRUD.isSelected()) {
					ArrayList<Projection> projections;
					try {
						projections = controller.getAllProjection();
						showAllProjectionsCRUD(table, projections);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
					
			}
});
			
		//Add button
			
	btnAdd.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			//user add
			if(radioButtonUserCRUD.isSelected()) {
				tabbedPane.setVisible(false);
				tabbedPane.setEnabled(false);
		    	frame.setTitle("Add user");
				addUserPanel.setLayout(null);
				
				JLabel lblName = new JLabel("name:");
				lblName.setBounds(10, 14, 66, 14);
				addUserPanel.add(lblName);
				
				JLabel lblSurname = new JLabel("surname:");
				lblSurname.setBounds(10, 45, 86, 14);
				addUserPanel.add(lblSurname);
				
				JLabel lblPassword = new JLabel("password:");
				lblPassword.setBounds(10, 136, 66, 14);
				addUserPanel.add(lblPassword);
				
				JLabel lblAddress = new JLabel("address:");
				lblAddress.setBounds(10, 106, 108, 14);
				addUserPanel.add(lblAddress);
				
				JLabel lblEmail = new JLabel("email:");
				lblEmail.setBounds(10, 76, 66, 14);
				addUserPanel.add(lblEmail);
				
				JLabel lblBirthDate = new JLabel("birth date:");
				lblBirthDate.setBounds(10, 168, 86, 14);
				addUserPanel.add(lblBirthDate);
				
				//JTextField setup
				/**JTextField textFieldId = new JTextField();
				textFieldId.setBounds(128, 33, 55, 20);
				addPanel.add(textFieldId);
				textFieldId.setColumns(10);*/

				JTextField textFieldName = new JTextField();
				textFieldName.setBounds(128, 11, 136, 20);
				addUserPanel.add(textFieldName);
				textFieldName.setColumns(10);
				
				JTextField textFieldSurname = new JTextField();
				textFieldSurname.setBounds(128, 42, 136, 20);
				addUserPanel.add(textFieldSurname);
				textFieldSurname.setColumns(10);
				
				JTextField textFieldEmail = new JTextField();
				textFieldEmail.setBounds(128, 73, 136, 20);
				addUserPanel.add(textFieldEmail);
				textFieldEmail.setColumns(10);
				
				JTextField textFieldAddress = new JTextField();
				textFieldAddress.setBounds(128, 103, 136, 20);
				addUserPanel.add(textFieldAddress);
				textFieldAddress.setColumns(10);
				
				
				//JPassword setup
				JPasswordField passwordField = new JPasswordField();
				passwordField.setBounds(128, 134, 113, 18);
				addUserPanel.add(passwordField);

				JLabel labelPicture = new JLabel("");
				labelPicture.setBounds(312, 11, 113, 109);
				addUserPanel.add(labelPicture);
				
				JButton btnPicture = new JButton("Picture");
				btnPicture.setBounds(319, 132, 98, 23);
				addUserPanel.add(btnPicture);
				
				Action saveImageUser=new AbstractAction() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						JFileChooser choosePicture = new JFileChooser();
						choosePicture.showOpenDialog(null);
						File file=choosePicture.getSelectedFile();
						fileNameUser=file.getAbsolutePath();
						ImageIcon imageIcon=new ImageIcon(new ImageIcon(fileNameUser).getImage().getScaledInstance(labelPicture.getWidth(), labelPicture.getHeight(), Image.SCALE_SMOOTH));
						labelPicture.setIcon(imageIcon);
						try {
							File image=new File(fileNameUser);
							FileInputStream fis= new FileInputStream(image);
							ByteArrayOutputStream bos=new ByteArrayOutputStream();
							byte[] buf=new byte[1024];
							for(int readNum; (readNum=fis.read(buf)) != -1;) {
								bos.write(buf,0,readNum);
							}
							image_user=bos.toByteArray();
						}
						catch(Exception e) {
							JOptionPane.showMessageDialog(null, e);
						}
				}
				};
				btnPicture.addActionListener(saveImageUser);
				
				JComboBox comboBoxDay = new JComboBox();
				JComboBox comboBoxMonth = new JComboBox();
				JComboBox comboBoxYear = new JComboBox();
				
				JButton btnBack = new JButton("Back");
				btnBack.setBackground(Color.LIGHT_GRAY);
				btnBack.setBounds(264, 213, 89, 23);
				addUserPanel.add(btnBack);
				btnBack.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						frame.setTitle("Manager");
						textFieldName.setText("");
						textFieldSurname.setText("");
						textFieldEmail.setText("");
						textFieldAddress.setText("");	
						passwordField.setText("");
						labelPicture.setIcon(null);
						comboBoxDay.setSelectedIndex(0);
						comboBoxMonth.setSelectedIndex(0);
						comboBoxYear.setSelectedIndex(0);
						tabbedPane.setEnabled(true);
						tabbedPane.setVisible(true);
						frame.setBounds(100, 100, 464, 334);
						Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
					    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
					    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
					    frame.setLocation(x, y);
						addUserPanel.setEnabled(false);
						addUserPanel.setVisible(false);
						ArrayList<User> users;
						try {
							users = controller.getAllUsersManager();
							showAllUsers(table, users);
						} catch (ClassNotFoundException | SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				});
				
				//JCheckBox setup
				JCheckBox chckbxShowPass = new JCheckBox("show");
				chckbxShowPass.setBounds(247, 130, 66, 27);
				addUserPanel.add(chckbxShowPass);
				chckbxShowPass.addItemListener(new ItemListener() {
					
					@Override
					public void itemStateChanged(ItemEvent arg0) {
						if(chckbxShowPass.isSelected()) {
							passwordField.setEchoChar((char)0);;
						}else {
							passwordField.setEchoChar('*');
						}
						
					}
				});
				
				//day
				for(int i=0; i <= 3; i++) {
					for(int j=0;j<=9;j++) {
						if((i+""+j).equals("32")) {
							break;
						}
						else {
							comboBoxDay.addItem(i+""+j);
							comboBoxDay.removeItem("00");
						}
					}
				}
				
				comboBoxDay.setBackground(Color.LIGHT_GRAY);
				comboBoxDay.setBounds(128, 164, 48, 22);
				addUserPanel.add(comboBoxDay);
				
				
				//month
				for(int i=0; i <= 1; i++) {
					for(int j=0;j<=9;j++) {
						if((i+""+j).equals("13")) {
							break;
						}else {
							comboBoxMonth.addItem(i+""+j);
							comboBoxMonth.removeItem("00");
						}
					}
					}
				
			
				comboBoxMonth.setBackground(Color.LIGHT_GRAY);
				comboBoxMonth.setBounds(186, 164, 48, 22);
				addUserPanel.add(comboBoxMonth);
			
				
				//year
				for(int i=1970;i<=2001;i++) {
					comboBoxYear.addItem(i);
				}
				
				comboBoxMonth.setBackground(Color.LIGHT_GRAY);
				comboBoxYear.setBounds(246, 164, 80, 22);
				addUserPanel.add(comboBoxYear);
				
				
				JLabel lblDay = new JLabel("dd/mm/yyyy");			
				lblDay.setBounds(336, 168, 75, 14);
				addUserPanel.add(lblDay);
				
				
				
				
				//frame positioning after all components are loaded
				frame.setBounds(100, 100, 463, 291);
				Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
			    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
			    frame.setLocation(x, y);
				addUserPanel.setVisible(true);
				
				
				//-----------------code---------TEST-------------
							
				//JButton setup
				JButton btnRegister = new JButton("Register");
				btnRegister.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						if(textFieldName.getText().length()!=0 && textFieldSurname.getText().length()!=0 && textFieldEmail.getText().length()!=0 || 
								textFieldAddress.getText().length()!=0 ) {
							
							String name = textFieldName.getText();
							String surname = textFieldSurname.getText();
							String email = textFieldEmail.getText();
							String password = passwordField.getText();
							String address = textFieldAddress.getText();
							String birthDay =  comboBoxYear.getItemAt(comboBoxYear.getSelectedIndex())+"-" +
							comboBoxMonth.getItemAt(comboBoxMonth.getSelectedIndex())+"-"+ comboBoxDay.getItemAt(comboBoxDay.getSelectedIndex());
							int id_patmentCard;
							if(valEmail(email)) {
							int id_manager=controller.getCurrentIdManager();
								try {					
									id_patmentCard = controller.getIdPaymentCard(password);				
									User u = new User(name, surname, email, password, address, birthDay, id_manager, id_patmentCard,image_user);
									controller.addUser(u);
									textFieldName.setText("");
									textFieldSurname.setText("");
									textFieldEmail.setText("");
									textFieldAddress.setText("");	
									passwordField.setText("");
									comboBoxDay.setSelectedIndex(0);
									comboBoxMonth.setSelectedIndex(0);
									comboBoxYear.setSelectedIndex(0);
									frame.setTitle("Manager");
									tabbedPane.setEnabled(true);
									tabbedPane.setVisible(true);
									frame.setBounds(100, 100, 464, 334);
									Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
								    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
								    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
								    frame.setLocation(x, y);
									addUserPanel.setEnabled(false);
									addUserPanel.setVisible(false);
									ArrayList<User> users;
									try {
										users = controller.getAllUsersManager();
										showAllUsers(table, users);
									} catch (ClassNotFoundException | SQLException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}catch (ClassNotFoundException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}else {
								form.showErrorMessage("Wrong email format", "Failed to add user");
							}
						   
						}else {
							JOptionPane.showMessageDialog(null, "All fields must be filled out as required!");
						}
						
					}
				});
				btnRegister.setBackground(Color.ORANGE);
				btnRegister.setBounds(74, 213, 89, 23);
				addUserPanel.add(btnRegister);
			} 
			
			//worker add
			
			else if(radioButtonWorkerCRUD.isSelected()) {
				addCashierPanel.setLayout(null);
				frame.getContentPane().add(addCashierPanel);
				
				frame.setBounds(100, 100, 431, 297);
				frame.setTitle("Add cashier");
			    frame.setResizable(false);
				
				addCashierPanel.setVisible(true);
				tabbedPane.setVisible(false);
				tabbedPane.setEnabled(false);
				
				JLabel lblName = new JLabel("name:");
				lblName.setBounds(10, 25, 58, 14);
				addCashierPanel.add(lblName);
				
				JLabel lblSurname = new JLabel("surname:");
				lblSurname.setBounds(10, 50, 72, 14);
				addCashierPanel.add(lblSurname);
				
				JLabel lblEmail = new JLabel("email:");
				lblEmail.setBounds(10, 75, 58, 14);
				addCashierPanel.add(lblEmail);
				
				JLabel lblPassword = new JLabel("password:");
				lblPassword.setBounds(10, 175, 84, 14);
				addCashierPanel.add(lblPassword);
				
				JLabel lblSalary = new JLabel("salary:");
				lblSalary.setBounds(10, 125, 58, 14);
				addCashierPanel.add(lblSalary);
				
				JLabel lblWorkingHours = new JLabel("working hours:");
				lblWorkingHours.setBounds(10, 150, 97, 14);
				addCashierPanel.add(lblWorkingHours);
				
				JLabel lblAddress = new JLabel("address:");
				lblAddress.setBounds(10, 100, 72, 14);
				addCashierPanel.add(lblAddress);
				
				JTextField textFieldSurname = new JTextField();
				textFieldSurname.setBounds(103, 47, 141, 20);
				addCashierPanel.add(textFieldSurname);
				textFieldSurname.setColumns(10);
				
				JTextField textFieldName = new JTextField();
				textFieldName.setColumns(10);
				textFieldName.setBounds(103, 22, 141, 20);
				addCashierPanel.add(textFieldName);
				
				JTextField textFieldEmail = new JTextField();
				textFieldEmail.setColumns(10);
				textFieldEmail.setBounds(103, 72, 141, 20);
				addCashierPanel.add(textFieldEmail);
				
				JPasswordField passwordField = new JPasswordField();
				passwordField.setBounds(103, 172, 141, 20);
				addCashierPanel.add(passwordField);
				
				JTextField textFieldSalary = new JTextField();
				textFieldSalary.setBounds(102, 122, 86, 20);
				addCashierPanel.add(textFieldSalary);
				textFieldSalary.setColumns(10);
				
				JTextField textFieldWHours = new JTextField();
				textFieldWHours.setBounds(102, 147, 86, 20);
				addCashierPanel.add(textFieldWHours);
				textFieldWHours.setColumns(10);
				
				JLabel lblEur = new JLabel("eur (\u20AC)");
				lblEur.setBounds(198, 125, 46, 14);
				addCashierPanel.add(lblEur);
				
				JTextField textFieldAddress = new JTextField();
				textFieldAddress.setBounds(103, 97, 141, 20);
				addCashierPanel.add(textFieldAddress);
				textFieldAddress.setColumns(10);
				
				JLabel lblH = new JLabel("h");
				lblH.setBounds(198, 150, 46, 14);
				addCashierPanel.add(lblH);
				
				JLabel lblPictureWorker = new JLabel("");
				lblPictureWorker.setBounds(275, 25, 127, 114);
				addCashierPanel.add(lblPictureWorker);
				
				JButton btnPictureWorker = new JButton("Picture");
				btnPictureWorker.setBounds(295, 146, 89, 23);
				addCashierPanel.add(btnPictureWorker);
				
				Action saveImageWorker=new AbstractAction() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						JFileChooser choosePicture = new JFileChooser();
						choosePicture.showOpenDialog(null);
						File file=choosePicture.getSelectedFile();
						fileNameWorker=file.getAbsolutePath();
						ImageIcon imageIcon=new ImageIcon(new ImageIcon(fileNameWorker).getImage().getScaledInstance(lblPictureWorker.getWidth(), lblPictureWorker.getHeight(), Image.SCALE_SMOOTH));
						lblPictureWorker.setIcon(imageIcon);
						try {
							File image=new File(fileNameWorker);
							FileInputStream fis= new FileInputStream(image);
							ByteArrayOutputStream bos=new ByteArrayOutputStream();
							byte[] buf=new byte[1024];
							for(int readNum; (readNum=fis.read(buf)) != -1;) {
								bos.write(buf,0,readNum);
							}
							image_worker=bos.toByteArray();
						}
						catch(Exception e) {
							JOptionPane.showMessageDialog(null, e);
						}
				}
				};
				btnPictureWorker.addActionListener(saveImageWorker);
				
				JButton btnInsert = new JButton("Insert");
				btnInsert.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if(textFieldName.getText().length()!=0 && textFieldSurname.getText().length()!=0 && textFieldEmail.getText().length()!=0 || 
								textFieldAddress.getText().length()!=0 ) {
							
							String name = textFieldName.getText();
							String surname = textFieldSurname.getText();
							String email = textFieldEmail.getText();
							String password = passwordField.getText();
							String address = textFieldAddress.getText();
							String salaryStr = textFieldSalary.getText();
							String wHoursStr=textFieldWHours.getText();
							
							float salary=0;
							int wHours=0;
							try {
							salary=Float.parseFloat(salaryStr);
							}catch(Exception e) {
								form.showErrorMessage("Wrong format of salary", "Failed to insert");
							}
							try {
							wHours=Integer.parseInt(wHoursStr);
							}catch(Exception e ) {
								form.showErrorMessage("Wrong format of working hours", "Failed to insert");
							}
							if(valEmail(email)) {
							int id_manager=controller.getCurrentIdManager();
								try {
									Worker worker = new Worker(name, surname, email, salary, image_worker, password, wHours, address);
									controller.addWorker(worker);
									textFieldName.setText("");
									textFieldSurname.setText("");
									textFieldEmail.setText("");
									textFieldAddress.setText("");	
									passwordField.setText("");
									textFieldWHours.setText("");
									textFieldSalary.setText("");
									frame.setTitle("Manager");
									tabbedPane.setEnabled(true);
									tabbedPane.setVisible(true);
									frame.setBounds(100, 100, 464, 334);
									Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
								    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
								    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
								    frame.setLocation(x, y);
									addCashierPanel.setEnabled(false);
									addCashierPanel.setVisible(false);
									ArrayList<Worker> workers;
									workers = controller.getAllWorkers();
									showAllWorkers(table, workers);
								}catch (ClassNotFoundException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}else {
								form.showErrorMessage("Wrong email format", "Failed to insert");
							}
						   
						}else {
							JOptionPane.showMessageDialog(null, "All fields must be filled out as required!");
						}
					}
				});
				btnInsert.setBackground(Color.ORANGE);
				btnInsert.setBounds(69, 216, 89, 23);
				addCashierPanel.add(btnInsert);
				
				JButton btnBack = new JButton("Back");
				btnBack.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						frame.setTitle("Manager");
						textFieldName.setText("");
						textFieldSurname.setText("");
						textFieldEmail.setText("");
						textFieldAddress.setText("");	
						passwordField.setText("");
						textFieldWHours.setText("");
						textFieldSalary.setText("");
						lblPictureWorker.setIcon(null);
						tabbedPane.setEnabled(true);
						tabbedPane.setVisible(true);
						frame.setBounds(100, 100, 464, 334);
						ArrayList<Worker> workers;
						workers = controller.getAllWorkers();
						showAllWorkers(table, workers);
						Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
					    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
					    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
					    frame.setLocation(x, y);
						addCashierPanel.setEnabled(false);
						addCashierPanel.setVisible(false);
					}
				});
				btnBack.setBounds(236, 216, 89, 23);
				addCashierPanel.add(btnBack);
				
				JCheckBox chckbxShow = new JCheckBox("Show");
				chckbxShow.setBounds(248, 171, 97, 23);
				addCashierPanel.add(chckbxShow);
				chckbxShow.addItemListener(new ItemListener() {
					
					@Override
					public void itemStateChanged(ItemEvent arg0) {
						if(chckbxShow.isSelected()) {
							passwordField.setEchoChar((char)0);;
						}else {
							passwordField.setEchoChar('*');
						}
						
					}
				});
				
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getContentPane().setLayout(null);
				Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
			    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
			    frame.setLocation(x, y);
			}
			else if(radioButtonProjectionCRUD.isSelected()) {
				frame.setTitle("Add projection");
				addProjectionPanel.setVisible(true);
				addProjectionPanel.setLayout(null);
				tabbedPane.setVisible(false);
				tabbedPane.setEnabled(false);
				JLabel lblDateAndTime = new JLabel("date:");
				lblDateAndTime.setBounds(10, 11, 54, 14);
				addProjectionPanel.add(lblDateAndTime);
				
				JComboBox comboBoxDay = new JComboBox();
				comboBoxDay.setBounds(74, 7, 54, 22);
				addProjectionPanel.add(comboBoxDay);
				//day
				for(int i=0; i <= 3; i++) {
					for(int j=0;j<=9;j++) {
						if((i+""+j).equals("32")) {
							break;
						}
						else {
							comboBoxDay.addItem(i+""+j);
							comboBoxDay.removeItem("00");
						}
					}
				}
				
				JComboBox comboBoxMonth = new JComboBox();
				comboBoxMonth.setBounds(138, 7, 54, 22);
				addProjectionPanel.add(comboBoxMonth);
				
				//month
				for(int i=0; i <= 1; i++) {
					for(int j=0;j<=9;j++) {
						if((i+""+j).equals("13")) {
							break;
						}else {
							comboBoxMonth.addItem(i+""+j);
							comboBoxMonth.removeItem("00");
						}
					}
					}
				
				JComboBox comboBoxYear = new JComboBox();
				comboBoxYear.setBounds(202, 7, 92, 22);
				addProjectionPanel.add(comboBoxYear);
				
				//year
				for(int i=2019;i<=2050;i++) {
					comboBoxYear.addItem(i);
				}
				
				JLabel lblDdmmyyyu = new JLabel("dd/mm/yyyy");
				lblDdmmyyyu.setBounds(304, 11, 83, 14);
				addProjectionPanel.add(lblDdmmyyyu);
				
				JLabel lblTime = new JLabel("Time:");
				lblTime.setBounds(10, 43, 46, 14);
				addProjectionPanel.add(lblTime);
				
				JTextField textFieldHours = new JTextField();
				textFieldHours.setBounds(74, 40, 54, 20);
				addProjectionPanel.add(textFieldHours);
				textFieldHours.setColumns(10);
				
				JTextField textFieldMinutes = new JTextField();
				textFieldMinutes.setBounds(138, 40, 54, 20);
				addProjectionPanel.add(textFieldMinutes);
				textFieldMinutes.setColumns(10);
				
				JLabel lblHhmm = new JLabel("HH:mm");
				lblHhmm.setBounds(202, 43, 60, 14);
				addProjectionPanel.add(lblHhmm);
				
				JLabel lblMovie = new JLabel("movie:");
				lblMovie.setBounds(10, 93, 46, 14);
				addProjectionPanel.add(lblMovie);
				
				JLabel lblActive = new JLabel("active:");
				lblActive.setBounds(10, 68, 46, 14);
				addProjectionPanel.add(lblActive);
				
				JCheckBox chckbxYes = new JCheckBox("yes");
				chckbxYes.setBounds(74, 64, 60, 23);
				addProjectionPanel.add(chckbxYes);
				
				JTextField textFieldMovieSearch = new JTextField();
				textFieldMovieSearch.setBounds(74, 90, 155, 20);
				addProjectionPanel.add(textFieldMovieSearch);
				textFieldMovieSearch.setColumns(10);
				
				JScrollPane scrollPaneMovies = new JScrollPane();
				scrollPaneMovies.setBounds(10, 121, 284, 126);
				addProjectionPanel.add(scrollPaneMovies);
				
				JTable tableMovie = new JTable();
				scrollPaneMovies.setViewportView(tableMovie);
				tableMovie.setBounds(10, 121, 361, 126);
				
				JScrollPane scrollPaneHall = new JScrollPane();
		        scrollPaneHall.setBounds(323, 121, 219, 126);
		        addProjectionPanel.add(scrollPaneHall);
		        
		        JTable tableHall = new JTable();
		        tableHall.setBounds(323, 121, 219, 126);
		        scrollPaneHall.setViewportView(tableHall);

		        JLabel lblHall = new JLabel("cinema:");
		        lblHall.setBounds(323, 93, 46, 14);

		        addProjectionPanel.add(lblHall);
		        
		        JTextField textFieldHall = new JTextField();
		        textFieldHall.setBounds(371, 90, 171, 20);
		        addProjectionPanel.add(textFieldHall);
		        textFieldHall.setColumns(10);
				
				Action actionSearch = new AbstractAction() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						String search=textFieldMovieSearch.getText();
						ArrayList<Movie> movies;
						try {
							movies = controller.getAllMovies(search);
							showAllMovies(tableMovie, movies);
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				};
				textFieldMovieSearch.addActionListener(actionSearch);
				
				Action actionHallSearch=new AbstractAction() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						ArrayList<Cinema> cinemas;
						String search=textFieldHall.getText();
						try {
							cinemas = controller.getAllCinema(search);
							showAllCinemas(tableHall, cinemas);;
						} catch (ClassNotFoundException | SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
					}
				};
				textFieldHall.addActionListener(actionHallSearch);
				
				JButton btnSave = new JButton("Save");
				btnSave.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						int rowMovie=tableMovie.getSelectedRow();
						int  rowHall=tableHall.getSelectedRow();
						boolean active;
						String hoursStr = textFieldHours.getText();
						String minutesStr = textFieldMinutes.getText();
						String idMovStr=tableMovie.getValueAt(rowMovie, 0).toString();
						String idHallStr=tableHall.getValueAt(rowHall, 0).toString();
						int idMov=Integer.parseInt(idMovStr);
						int idHall=Integer.parseInt(idHallStr);
						int hours=Integer.parseInt(hoursStr);
						int minutes=Integer.parseInt(minutesStr);
						if(hours>0 && hours<24 && minutes>=0 && minutes<60) {
						String projectionDay =  comboBoxYear.getItemAt(comboBoxYear.getSelectedIndex())+"-" +
								comboBoxMonth.getItemAt(comboBoxMonth.getSelectedIndex())+"-"+ comboBoxDay.getItemAt(comboBoxDay.getSelectedIndex())+ " "
								+ hours +":"+minutes+":"+00;
						if(chckbxYes.isSelected()) {
							active=true;
						}else {
							active=false;
						}
							Projection projection=new Projection(idHall, active, idMov, projectionDay);
							try {
								controller.addProjection(projection);
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}else {
							form.showErrorMessage("Date is not in the right range", "Date error");
						}
					}
				});
				btnSave.setBackground(Color.ORANGE);
				btnSave.setBounds(74, 261, 89, 23);
				addProjectionPanel.add(btnSave);
				
				JButton btnBack = new JButton("Back");
				btnBack.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						try {
							ArrayList<Projection> projections= controller.getAllProjection();
							showAllProjectionsCRUD(table, projections);
						} catch (ClassNotFoundException | SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						addProjectionPanel.setVisible(false);
						frame.setTitle("Manager");
						tabbedPane.setVisible(true);
						tabbedPane.setEnabled(true);
						frame.setBounds(100, 100, 464, 334);
						Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
					    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
					    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
					    frame.setLocation(x, y);
					}
				});
				btnBack.setBounds(376, 261, 89, 23);
				addProjectionPanel.add(btnBack);
				
				frame.setBounds(100, 100, 585, 334);
			    frame.setResizable(false);
				
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getContentPane().setLayout(null);
				Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
			    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
			    frame.setLocation(x, y);
			}
			
		}
	});	
	
	//update button
	JButton btnUpdate = new JButton("Update");
	btnUpdate.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\userUpdate.png"));
	btnUpdate.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			if(radioButtonUserCRUD.isSelected()) {
			int rowNumber=table.getSelectedRow();
			if(rowNumber>=0) {
			frame.setBounds(100, 100, 836, 334);
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
		    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
		    frame.setLocation(x, y);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().add(updateUserPanel);
			updateUserPanel.setVisible(true);
			TableModel model=table.getModel();
			
			}
			else {
				form.showMessage("Show all and select user");
			}
		}else if(radioButtonWorkerCRUD.isSelected()) {
			int rowNumber=table.getSelectedRow();
			if(rowNumber>=0) {
			frame.setBounds(100, 100, 836, 334);
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
		    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
		    frame.setLocation(x, y);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().add(updateUserPanel);
			updateUserPanel.setVisible(true);
			textFieldSalaryUpdate.setVisible(true);
			lblSalaryUpdate.setVisible(true);
			TableModel model=table.getModel();
		}else {
				form.showMessage("Show all and select worker");
			}
		}else if(radioButtonProjectionCRUD.isSelected()) {
			int rowNumber=table.getSelectedRow();
			if(rowNumber>=0) {
			updateProjectionPanel.setVisible(true);
			tabbedPane.setVisible(false);
			tabbedPane.setEnabled(false);
			frame.setBounds(100, 100, 585, 334);
			frame.setTitle("Update projection");
		    frame.setResizable(false);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
		    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
		    frame.setLocation(x, y);
			}else {
				form.showMessage("Show all and select projection");
			}
		}
		}
	});
	
	btnSaveUpdate.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(radioButtonUserCRUD.isSelected()) {
			int row=table.getSelectedRow();
			if(row>=0) {
			String idStr=table.getValueAt(row, 0).toString();
			int id=Integer.parseInt(idStr);
			String name = textFieldNameUpdate.getText();
			String surname = textFieldSurnameUpdate.getText();
			String email = textFieldEmailUpdate.getText();
			String address = textFieldAddressUpdate.getText();
			String idManager = textFieldManagerIdUpdate.getText();
			int manager_id=Integer.parseInt(idManager);
			
			if(valEmail(email)) {
			controller.updateUser(id, name, surname, email, address, manager_id, image_user);
			frame.setBounds(100, 100, 464, 334);
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
		    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
		    frame.setLocation(x, y);
		    clearFieldsUpdate();
			ArrayList<User> users;
			try {
				users = controller.getAllUsersManager();
				showAllUsers(table, users);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}else {
				form.showErrorMessage("Wrong email format", "Update failed");
			}
			}
			else {
				form.showErrorMessage("Select user again.", " ");
			}
		}else if(radioButtonWorkerCRUD.isSelected()) {
			int row=table.getSelectedRow();
			if(row>=0) {
			String idStr=table.getValueAt(row, 0).toString();
			int id=Integer.parseInt(idStr);
			String name = textFieldNameUpdate.getText();
			String surname = textFieldSurnameUpdate.getText();
			String email = textFieldEmailUpdate.getText();
			String address = textFieldAddressUpdate.getText();
			String idManager = textFieldManagerIdUpdate.getText();
			String salaryStr=textFieldSalaryUpdate.getText();
			int manager_id=Integer.parseInt(idManager);
			float salary=Float.parseFloat(salaryStr);
			if(valEmail(email)) {
			Worker worker= new Worker(name, surname, email, salary, image_user, address, manager_id);
			controller.updateWorker(worker,id);
			frame.setBounds(100, 100, 464, 334);
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
			int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
		    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
		    frame.setLocation(x, y);
			ArrayList<Worker> workers;
			workers = controller.getAllWorkers();
			showAllWorkers(table, workers);
			}else {
				form.showErrorMessage("Wrong email format", "Update failed");
			}
			}
			else {
				form.showErrorMessage("Select user again.", " ");
			}
		}
			
		}
	});
	
	//update projections
	
	JLabel lblDateAndTimeUpdate = new JLabel("date:");
	lblDateAndTimeUpdate.setBounds(10, 11, 54, 14);
	updateProjectionPanel.add(lblDateAndTimeUpdate);
	
	JComboBox comboBoxDayUpdate = new JComboBox();
	comboBoxDayUpdate.setBounds(74, 7, 54, 22);
	updateProjectionPanel.add(comboBoxDayUpdate);
	
	//day
	for(int i=0; i <= 3; i++) {
		for(int j=0;j<=9;j++) {
			if((i+""+j).equals("32")) {
				break;
			}
			else {
				comboBoxDayUpdate.addItem(i+""+j);
				comboBoxDayUpdate.removeItem("00");
			}
		}
	}
	
	JComboBox comboBoxMonthUpdate = new JComboBox();
	comboBoxMonthUpdate.setBounds(138, 7, 54, 22);
	updateProjectionPanel.add(comboBoxMonthUpdate);
	
	for(int i=0; i <= 1; i++) {
		for(int j=0;j<=9;j++) {
			if((i+""+j).equals("13")) {
				break;
			}else {
				comboBoxMonthUpdate.addItem(i+""+j);
				comboBoxMonthUpdate.removeItem("00");
			}
		}
		}
	
	JComboBox comboBoxYearUpdate = new JComboBox();
	comboBoxYearUpdate.setBounds(202, 7, 92, 22);
	updateProjectionPanel.add(comboBoxYearUpdate);
	
	for(int i=2019;i<=2050;i++) {
		comboBoxYearUpdate.addItem(i);
	}
	
	JLabel lblDdmmyyyuUpdate = new JLabel("dd/mm/yyyy");
	lblDdmmyyyuUpdate.setBounds(304, 11, 83, 14);
	updateProjectionPanel.add(lblDdmmyyyuUpdate);
	
	JLabel lblTimeUpdate = new JLabel("Time:");
	lblTimeUpdate.setBounds(10, 43, 46, 14);
	updateProjectionPanel.add(lblTimeUpdate);
	
	JTextField textFieldHoursUpdate = new JTextField();
	textFieldHoursUpdate.setBounds(74, 40, 54, 20);
	updateProjectionPanel.add(textFieldHoursUpdate);
	textFieldHoursUpdate.setColumns(10);
	
	JTextField textFieldMinutesUpdate = new JTextField();
	textFieldMinutesUpdate.setBounds(138, 40, 54, 20);
	updateProjectionPanel.add(textFieldMinutesUpdate);
	textFieldMinutesUpdate.setColumns(10);
	
	JLabel lblHhmmUpdate = new JLabel("HH:mm");
	lblHhmmUpdate.setBounds(202, 43, 60, 14);
	updateProjectionPanel.add(lblHhmmUpdate);
	
	JLabel lblMovieUpdate = new JLabel("movie:");
	lblMovieUpdate.setBounds(10, 93, 46, 14);
	updateProjectionPanel.add(lblMovieUpdate);
	
	
	JScrollPane scrollPaneMovieUpdate = new JScrollPane();
	scrollPaneMovieUpdate.setBounds(10, 121, 284, 126);
	updateProjectionPanel.add(scrollPaneMovieUpdate);
	
	JTable tableMovieUpdate = new JTable();
	scrollPaneMovieUpdate.setViewportView(tableMovieUpdate);
	tableMovieUpdate.setBounds(10, 121, 361, 126);
	
	JTextField textFieldMovieSearchUpdate = new JTextField();
	textFieldMovieSearchUpdate.setBounds(74, 90, 220, 20);
	updateProjectionPanel.add(textFieldMovieSearchUpdate);
	textFieldMovieSearchUpdate.setColumns(10);
	textFieldMovieSearchUpdate.setEnabled(false);
	
	JTextField textFieldIdUpdate = new JTextField();
    textFieldIdUpdate.setBounds(430, 40, 46, 20);
    updateProjectionPanel.add(textFieldIdUpdate);
    textFieldIdUpdate.setColumns(10);
	textFieldIdUpdate.setEnabled(false);
	
	JScrollPane scrollPaneHallUpdate = new JScrollPane();
    scrollPaneHallUpdate.setBounds(323, 121, 219, 126);
    updateProjectionPanel.add(scrollPaneHallUpdate);
    
    JTable tableHallUpdate = new JTable();
    tableHallUpdate.setBounds(250, 121, 171, 126);
    scrollPaneHallUpdate.setViewportView(tableHallUpdate);
	
	JCheckBox chckbxYesUpdate = new JCheckBox("yes");
	chckbxYesUpdate.setBounds(74, 64, 60, 23);
	updateProjectionPanel.add(chckbxYesUpdate);
	
	 JTextField textFieldHallUpdate = new JTextField();
	    textFieldHallUpdate.setBounds(371, 90, 171, 20);
	    updateProjectionPanel.add(textFieldHallUpdate);
	    textFieldHallUpdate.setColumns(10);
	    textFieldHallUpdate.setEnabled(false);
	
	textFieldMovieSearchUpdate.addMouseListener(new MouseListener() {
		
		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseClicked(MouseEvent arg0) {
			textFieldMovieSearchUpdate.setEnabled(true);
			// TODO Auto-generated method stub
			
		}
	});
	
	Action actionSearchUpdate = new AbstractAction() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String search=textFieldMovieSearchUpdate.getText();
			ArrayList<Movie> movies;
			try {
				movies = controller.getAllMovies(search);
				showAllMovies(tableMovieUpdate, movies);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};
	textFieldMovieSearchUpdate.addActionListener(actionSearchUpdate);
	

	JButton btnSaveUpdateProj = new JButton("Save");
	btnSaveUpdateProj.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String idStr=textFieldIdUpdate.getText();
			int rowMovieUpdate=tableMovieUpdate.getSelectedRow();
			int  rowHallUpdate=tableHallUpdate.getSelectedRow();
			boolean activeUpdate;
			String hoursStrUpdate = textFieldHoursUpdate.getText();
			String minutesStrUpdate = textFieldMinutesUpdate.getText();
			int idMovUpdate = 0;
			int idHallUpdate = 0;
			if(textFieldMovieSearchUpdate.isEnabled()) {
			String idMovStr=tableMovieUpdate.getValueAt(rowMovieUpdate, 0).toString();
			idMovUpdate=Integer.parseInt(idMovStr);
			}else {
					try {
						idMovUpdate=controller.getMovieID(textFieldMovieSearchUpdate.getText());
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					}
			}
			if(textFieldHallUpdate.isEnabled()) {
			String idHallStr=tableHallUpdate.getValueAt(rowHallUpdate, 0).toString();
			idHallUpdate=Integer.parseInt(idHallStr);
			}else {
				try {
					idHallUpdate=controller.getHallId(textFieldHallUpdate.getText());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			int hours=Integer.parseInt(hoursStrUpdate);
			int minutes=Integer.parseInt(minutesStrUpdate);
			int idUpdateProj=Integer.parseInt(idStr);
			if(hours>0 && hours<24 && minutes>=0 && minutes<60) {
				String projectionDayUpdate =  comboBoxYearUpdate.getItemAt(comboBoxYearUpdate.getSelectedIndex())+"-" +
						comboBoxMonthUpdate.getItemAt(comboBoxMonthUpdate.getSelectedIndex())+"-"+ comboBoxDayUpdate.getItemAt(comboBoxDayUpdate.getSelectedIndex())+ " "
						+ hours +":"+minutes+":"+00;
				if(chckbxYesUpdate.isSelected()) {
					activeUpdate=true;
				}else {
					activeUpdate=false;
				}
					Projection projection=new Projection(idUpdateProj, idHallUpdate, activeUpdate, idMovUpdate, projectionDayUpdate);
					ArrayList<Projection> projections = controller.updateProjection(projection);
					showAllProjectionsCRUD(table, projections);
				}else {
					form.showErrorMessage("Date is not in the right range", "Date error");
				}
			
		}
	});
	btnSaveUpdateProj.setBackground(Color.ORANGE);
	btnSaveUpdateProj.setBounds(74, 261, 89, 23);
	updateProjectionPanel.add(btnSaveUpdateProj);
	
	JButton btnBackUpdateProj = new JButton("Back");
	btnBackUpdateProj.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			tabbedPane.setVisible(true);
			tabbedPane.setEnabled(true);
			updateProjectionPanel.setVisible(false);
			frame.setBounds(100, 100, 464, 334);
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
				    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
				    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
				    frame.setLocation(x, y);
		}
	});
	btnBackUpdateProj.setBounds(376, 261, 89, 23);
	updateProjectionPanel.add(btnBackUpdateProj);
	
	JLabel lblActiveUpdate = new JLabel("active:");
	lblActiveUpdate.setBounds(10, 68, 46, 14);
	updateProjectionPanel.add(lblActiveUpdate);
    
    JLabel lblHallUpdate = new JLabel("hall:");
    lblHallUpdate.setBounds(323, 93, 46, 14);
    updateProjectionPanel.add(lblHallUpdate);
    
    Action actionHallSearchUpdate=new AbstractAction() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			ArrayList<Cinema> cinemas;
			String search=textFieldHallUpdate.getText();
			try {
				cinemas = controller.getAllCinema(search);
				showAllCinemas(tableHallUpdate, cinemas);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	};
    textFieldHallUpdate.addActionListener(actionHallSearchUpdate);
    textFieldHallUpdate.addMouseListener(new MouseListener() {
		
		@Override
		public void mouseReleased(MouseEvent arg0) {
			
			
		}
		
		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseClicked(MouseEvent arg0) {
			textFieldHallUpdate.setEnabled(true);
			
		}
	});
    
    //table listener
	
	table.addMouseListener(new MouseListener() {
		
		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			if(radioButtonUserCRUD.isSelected()) {
			int row=table.getSelectedRow();
			TableModel model=table.getModel();
			String idStr=model.getValueAt(row, 0).toString();
			int id=Integer.parseInt(idStr);
			textFieldNameUpdate.setText(model.getValueAt(row, 1).toString());
			textFieldSurnameUpdate.setText(model.getValueAt(row, 2).toString());
			textFieldEmailUpdate.setText(model.getValueAt(row, 3).toString());
			textFieldAddressUpdate.setText(model.getValueAt(row, 4).toString());
			textFieldManagerIdUpdate.setText(model.getValueAt(row, 5).toString());
			byte[] picture=controller.getUserPicture(id);
			if(picture!=null) {
				ImageIcon imageIcon=new ImageIcon(new ImageIcon(picture).getImage().getScaledInstance(labelPictureUpdate.getWidth(), labelPictureUpdate.getHeight(), Image.SCALE_SMOOTH));
				labelPictureUpdate.setIcon(imageIcon);
				}else if(picture == null){
					labelPictureUpdate.setIcon(null);
					labelPictureUpdate.setText("Insert image here");
				}
			}else if(radioButtonWorkerCRUD.isSelected()) {
				int row=table.getSelectedRow();
				TableModel model=table.getModel();
				textFieldNameUpdate.setText(model.getValueAt(row, 1).toString());
				textFieldSurnameUpdate.setText(model.getValueAt(row, 2).toString());
				textFieldEmailUpdate.setText(model.getValueAt(row, 3).toString());
				textFieldAddressUpdate.setText(model.getValueAt(row, 4).toString());
				textFieldSalaryUpdate.setText(model.getValueAt(row, 5).toString());
				textFieldManagerIdUpdate.setText(model.getValueAt(row, 6).toString());
				String idStr=model.getValueAt(row, 0).toString();
				int id=Integer.parseInt(idStr);
				byte[] picture=controller.getWorkerPicture(id);
				if(picture!=null) {
					ImageIcon imageIcon=new ImageIcon(new ImageIcon(picture).getImage().getScaledInstance(labelPictureUpdate.getWidth(), labelPictureUpdate.getHeight(), Image.SCALE_SMOOTH));
					labelPictureUpdate.setIcon(imageIcon);
					}else if(picture == null){
						labelPictureUpdate.setIcon(null);
						labelPictureUpdate.setText("Insert image here");
					}
			}else if(radioButtonProjectionCRUD.isSelected()) {
				int row=table.getSelectedRow();
				TableModel model=table.getModel();
				textFieldMovieSearchUpdate.setText(model.getValueAt(row, 2).toString());
				textFieldHallUpdate.setText(model.getValueAt(row, 3).toString());
				textFieldIdUpdate.setText(model.getValueAt(row, 0).toString());
				String date_time=model.getValueAt(row, 1).toString();
				String year=date_time.substring(0,4);
				String month=date_time.substring(5, 7);
				String day=date_time.substring(8,10);
				comboBoxYearUpdate.setSelectedItem(year);
				comboBoxMonthUpdate.setSelectedItem(month);
				comboBoxDayUpdate.setSelectedItem(day);
				String hour=date_time.substring(11, 13);
				textFieldHoursUpdate.setText(hour);
				String minute=date_time.substring(14, 16);
				textFieldMinutesUpdate.setText(minute);
				String active=model.getValueAt(row, 4).toString();
				if(active.equals("true")) {
					chckbxYesUpdate.setSelected(true);
				}else {
					chckbxYesUpdate.setSelected(false);
				}
			}
		}
	});
	
	
	btnUpdate.setBounds(295, 79, 105, 23);
	panelCRUD.add(btnUpdate);
		//remove button
	
		btnRemove.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//remove users
				if(radioButtonUserCRUD.isSelected()) {
					int rowNumber=table.getSelectedRow();
					if(rowNumber>=0) {
					String idStr=String.valueOf(table.getValueAt(rowNumber, 0));
					String name=String.valueOf(table.getValueAt(rowNumber, 1));
					String surname=String.valueOf(table.getValueAt(rowNumber, 2));
					String email=String.valueOf(table.getValueAt(rowNumber, 3));
					String address=String.valueOf( table.getValueAt(rowNumber, 4));
					String managerIdStr=String.valueOf(table.getValueAt(rowNumber, 5));
					int id=Integer.parseInt(idStr);
					int manager_id=Integer.parseInt(managerIdStr);
					User user=new User(id, name, surname, email, address, manager_id);
					try {
						int dialogButton = JOptionPane.YES_NO_OPTION;
						int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to delete "+user.getName()+" "+user.getSurname()+"?","Warning",dialogButton);
						if(dialogResult == JOptionPane.YES_OPTION){
						ArrayList<User> users=controller.deleteUser(user);
						showAllUsers(table, users);
						}
						scrollPaneCRUD.add(table);
					} catch (ClassNotFoundException | SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					}else {
						form.showMessage("You have to show all and select user first.");
					}
				}
				//worker remove
				else if(radioButtonWorkerCRUD.isSelected()) {
						int rowNumber=table.getSelectedRow();
						if(rowNumber>=0) {
						String idStr=String.valueOf(table.getValueAt(rowNumber, 0));
						String name=String.valueOf(table.getValueAt(rowNumber, 1));
						String surname=String.valueOf(table.getValueAt(rowNumber, 2));
						int id=Integer.parseInt(idStr);
						try {
							int dialogButton = JOptionPane.YES_NO_OPTION;
							int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to delete "+name+" "+surname+"?","Warning",dialogButton);
							if(dialogResult == JOptionPane.YES_OPTION){
							ArrayList<Worker> workers=controller.deleteWorker(id);
							showAllWorkers(table, workers);
							}
							scrollPaneCRUD.add(table);
						} catch (ClassNotFoundException | SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						}else {
							form.showMessage("You have to show all and select worker first.");
						}
				}else if(radioButtonProjectionCRUD.isSelected()) {
					int rowNumber=table.getSelectedRow();
					if(rowNumber>=0) {
					String idStr=String.valueOf(table.getValueAt(rowNumber, 0));
					String date_time=String.valueOf(table.getValueAt(rowNumber, 1));
					String movieName=String.valueOf(table.getValueAt(rowNumber, 2));
					String cinemaName=String.valueOf(table.getValueAt(rowNumber, 3));
					int id=Integer.parseInt(idStr);
					try {
						int dialogButton = JOptionPane.YES_NO_OPTION;
						int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to delete "+movieName+" at "+date_time+" in cinema "+cinemaName+"?","Warning",dialogButton);
						if(dialogResult == JOptionPane.YES_OPTION){
						ArrayList<Projection> projections=controller.deleteProjection(id);
						showAllProjectionsCRUD(table, projections);
						}
						scrollPaneCRUD.add(table);
					} catch (ClassNotFoundException | SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					}else {
						form.showMessage("You have to show all and select worker first.");
					}
				}
			}
		});
		
		//update button
		
		//adding to the buttongroup
		ButtonGroup radiosCRUD = new ButtonGroup();
		radiosCRUD.add(radioButtonUserCRUD);
		radiosCRUD.add(radioButtonWorkerCRUD);
		radiosCRUD.add(radioButtonProjectionCRUD);
		radioButtonUserCRUD.setSelected(true);
		
		JLabel lblId_1 = new JLabel("id:");
		lblId_1.setBounds(10, 10, 31, 14);
		panelCRUD.add(lblId_1);
		
		textFieldSearchId = new JTextField();
		textFieldSearchId.setBounds(36, 7, 66, 20);
		panelCRUD.add(textFieldSearchId);
		textFieldSearchId.setColumns(10);
		Action actionSearchId=new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String idStr=textFieldSearchId.getText();
				int id=0;
				try {
				id=Integer.parseInt(idStr);}
				catch(Exception e) {
					form.showErrorMessage("Please enter id", "");
				}
				if(radioButtonUserCRUD.isSelected()) {
					ArrayList<User> users;
					try {
						users = controller.searchAllUsersId(id);
						showAllUsers(table, users);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}else if(radioButtonWorkerCRUD.isSelected()) {
					ArrayList<Worker> workers;
					try {
						workers=controller.searchAllWorkersId(id);
						showAllWorkers(table, workers);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else if(radioButtonProjectionCRUD.isSelected()) {
					ArrayList<Projection> projections;
					try {
						projections=controller.searchAllProjectionsId(id);
						showAllProjectionsCRUD(table, projections);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
		};
		textFieldSearchId.addActionListener(actionSearchId);
		
		
		radioButtonUserCRUD.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				 if (e.getStateChange() == ItemEvent.SELECTED) {
					try {
						labelPictureUpdate.setIcon(null);
						labelPictureUpdate.setText("Insert image here");
						ArrayList<User> users=controller.getAllUsersManager();
						showAllUsers(table, users);
						clearFieldsUpdate();
						frame.setBounds(100, 100, 464, 334);
						Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
					    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
					    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
					    frame.setLocation(x, y);
					    lblSalaryUpdate.setVisible(false);
					    textFieldSalaryUpdate.setVisible(false);
					} catch (ClassNotFoundException | SQLException e1) {
						e1.printStackTrace();
					}
				    }
			}
		});
		radioButtonWorkerCRUD.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				 if (e.getStateChange() == ItemEvent.SELECTED) {
					 labelPictureUpdate.setIcon(null);
					 labelPictureUpdate.setText("Insert image here");
					 ArrayList<Worker> workers=controller.getAllWorkers();
					 showAllWorkers(table, workers);
					 clearFieldsUpdate();
					 frame.setBounds(100, 100, 464, 334);
					 Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
					    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
					    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
					    frame.setLocation(x, y);
				 }
			}
		});
		
		radioButtonProjectionCRUD.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				 if (e.getStateChange() == ItemEvent.SELECTED) {
					 ArrayList<Projection> projections;
					try {
						labelPictureUpdate.setIcon(null);
						labelPictureUpdate.setText("Insert image here");
						projections = controller.getAllProjection();
						showAllProjectionsCRUD(table, projections);
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 clearFieldsUpdate();
					 frame.setBounds(100, 100, 464, 334);
					 Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
					    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
					    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
					    frame.setLocation(x, y);
				 }
			}
		});
		
		rdbtnUser.setSelected(true);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnLogout.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Button-Log-Off-icon.png"));
		btnLogout.setBackground(new Color(220, 20, 60));
		btnLogout.setForeground(Color.WHITE);
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to logout? ","Warning",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
					CinemaFront cinema = new CinemaFront();
					cinema.main(null);
					frame.dispose();
			}
			}
		});
		btnLogout.setBounds(4, 211, 100, 23);
		panelFront.add(btnLogout);
		
		JPanel panelMovie = new JPanel();
		tabbedPane.addTab("Movie Page", null, panelMovie, null);
		panelMovie.setLayout(null);
		
		JLabel lblNameMovie = new JLabel("name:");
		lblNameMovie.setBounds(10, 14, 67, 14);
		panelMovie.add(lblNameMovie);
		
		textFieldMovieName = new JTextField();
		textFieldMovieName.setBounds(66, 11, 108, 20);
		panelMovie.add(textFieldMovieName);
		textFieldMovieName.setColumns(10);
		
		JLabel lblGenre = new JLabel("genre");
		lblGenre.setBounds(10, 42, 46, 14);
		panelMovie.add(lblGenre);
		
		textFieldGenreName = new JTextField();
		textFieldGenreName.setBounds(66, 39, 108, 20);
		panelMovie.add(textFieldGenreName);
		textFieldGenreName.setColumns(10);
		
		JLabel lblDuration = new JLabel("duration");
		lblDuration.setBounds(10, 73, 55, 14);
		panelMovie.add(lblDuration);
		
		textFieldDuration = new JTextField();
		textFieldDuration.setBounds(66, 70, 67, 20);
		panelMovie.add(textFieldDuration);
		textFieldDuration.setColumns(10);
		
		JScrollPane scrollPaneMovie = new JScrollPane();
		scrollPaneMovie.setBounds(10, 135, 403, 99);
		panelMovie.add(scrollPaneMovie);
		
		tableMovie = new JTable();
		scrollPaneMovie.setViewportView(tableMovie);
		
		JButton btnSaveMovie = new JButton("Save");
		btnSaveMovie.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\Save-icon.png"));
		btnSaveMovie.setForeground(Color.WHITE);
		btnSaveMovie.setBackground(new Color(220, 20, 60));
		btnSaveMovie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name=textFieldMovieName.getText();
				String genre=textFieldGenreName.getText();
				String durationStr=textFieldDuration.getText();
				if(name.length()!=0 && genre.length()!=0 && durationStr.length()!=0) {
				try {
				int duration=Integer.parseInt(durationStr);
				Movie movie=new Movie(name,genre,durationStr);
				try {
					textFieldMovieName.setText("");
					textFieldGenreName.setText("");
					textFieldDuration.setText("");
					ArrayList<Movie> movies = controller.addMovie(movie);
		    		form.showMessage("Inserted successfully");
					showAllMovies(tableMovie, movies);
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					
				}
				}catch(Exception e) {
					form.showErrorMessage("Duration must be number","Format error");
				}
			}else {
				form.showErrorMessage("All fields must be filled","Insert error");
			}
			}
		});
		btnSaveMovie.setBounds(299, 39, 101, 22);
		panelMovie.add(btnSaveMovie);
		
		JLabel lblIdMovieClick = new JLabel("id");
		lblIdMovieClick.setBounds(207, 14, 31, 14);
		panelMovie.add(lblIdMovieClick);
		lblIdMovieClick.setVisible(false);
		
		tableMovie.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				btnSaveMovie.setEnabled(false);
				int row=tableMovie.getSelectedRow();
				TableModel model=tableMovie.getModel();
				lblIdMovieClick.setText(model.getValueAt(row, 0).toString());
				textFieldMovieName.setText(model.getValueAt(row, 1).toString());
				textFieldGenreName.setText(model.getValueAt(row, 2).toString());
				String durationStr=model.getValueAt(row, 3).toString();
				String numberDuration=durationStr.substring(0,(durationStr.length()-4));
				textFieldDuration.setText(numberDuration);
			}
		});
		JButton btnUpdateMovie = new JButton("Update");
		btnUpdateMovie.setIcon(new ImageIcon("C:\\Users\\Nemanja\\Desktop\\Seminarski\\cineplex\\images\\userUpdate.png"));
		btnUpdateMovie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String idStr=lblIdMovieClick.getText();
				String movieName=textFieldMovieName.getText();
				String genre=textFieldGenreName.getText();
				String duration = textFieldDuration.getText();
				if(movieName.length()!=0 && genre.length()!=0 && duration.length()!=0) {
				int id=Integer.parseInt(idStr);
				Movie movie=new Movie(id,movieName,genre,duration);
				ArrayList<Movie> movies;
				try {
					textFieldDuration.setText("");
					textFieldGenreName.setText("");
					textFieldMovieName.setText("");
					btnSaveMovie.setEnabled(true);
					form.showMessage("Successfully updated");
					movies = controller.updateMovie(movie);
					showAllMovies(tableMovie, movies);
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					form.showErrorMessage("Update failed", "Database error");
				}
				}else {
					form.showErrorMessage("All fields must be filled","Insert error");
				}
			}
		});
		btnUpdateMovie.setBackground(Color.PINK);
		btnUpdateMovie.setBounds(299, 69, 101, 23);
		panelMovie.add(btnUpdateMovie);
		
		JLabel lblSearchMovie = new JLabel("Search:");
		lblSearchMovie.setBounds(10, 106, 67, 14);
		panelMovie.add(lblSearchMovie);
		
		textFieldMovieSearch = new JTextField();
		textFieldMovieSearch.setBounds(66, 104, 152, 20);
		panelMovie.add(textFieldMovieSearch);
		textFieldMovieSearch.setColumns(10);
		
		Action searchMovie=new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<Movie> movies;
				String search=textFieldMovieSearch.getText();
				try {
					movies=controller.getAllMovies(search);
					showAllMovies(tableMovie, movies);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		};
		textFieldMovieSearch.addActionListener(searchMovie);
		
		JLabel lblMinutes = new JLabel("minutes");
		lblMinutes.setBounds(143, 73, 75, 14);
		panelMovie.add(lblMinutes);
		
		JButton btnAll = new JButton("all");
		btnAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<Movie> movies;
				try {
					movies=controller.getAllMovies("all");
					showAllMovies(tableMovie, movies);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		});
		btnAll.setBounds(228, 102, 55, 23);
		panelMovie.add(btnAll);
	}
	
	private void showAllUsers(JTable table, ArrayList<User> users) {
		DefaultTableModel model=new DefaultTableModel(); 
		Object[] columnsName=new Object[6];
		columnsName[0]="Id";
		columnsName[1]="Name";
		columnsName[2]="Surname";
		columnsName[3]="Email";
		columnsName[4]="Address";
		columnsName[5]="Manager id";
		model.setColumnIdentifiers(columnsName);
		Object[] rowData=new Object[6];
		for(int i=0;i<users.size();i++) {
			rowData[0]=users.get(i).getId();
			rowData[1]=users.get(i).getName();
			rowData[2]=users.get(i).getSurname();
			rowData[3]=users.get(i).getEmail();
			rowData[4]=users.get(i).getAddress();
			rowData[5]=users.get(i).getId_manager();
			model.addRow(rowData);}
		table.setModel(model);
		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(120);
		columnModel.getColumn(2).setPreferredWidth(130);
		columnModel.getColumn(3).setPreferredWidth(220);
		columnModel.getColumn(4).setPreferredWidth(180);
		columnModel.getColumn(5).setPreferredWidth(50);
	}
	
	private void showAllWorkers(JTable table, ArrayList<Worker> workers) {
		DefaultTableModel model=new DefaultTableModel(); 
		String[] columnsName=new String[7];
		columnsName[0]="Id";
		columnsName[1]="Name";
		columnsName[2]="Surname";
		columnsName[3]="Email";
		columnsName[4]="Address";
		columnsName[5]="Salary";
		columnsName[6]="Manager id";
		model.setColumnIdentifiers(columnsName);
		Object[] rowData=new Object[7];
		for(int i=0;i<workers.size();i++) {
			rowData[0]=workers.get(i).getId();
			rowData[1]=workers.get(i).getName();
			rowData[2]=workers.get(i).getSurname();
			rowData[3]=workers.get(i).getEmail();
			rowData[4]=workers.get(i).getAddress();
			rowData[5]=workers.get(i).getSalary();
			rowData[6]=workers.get(i).getManagerId();
			model.addRow(rowData);
			}
		table.setModel(model);
		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(120);
		columnModel.getColumn(2).setPreferredWidth(130);
		columnModel.getColumn(3).setPreferredWidth(220);
		columnModel.getColumn(4).setPreferredWidth(180);
		columnModel.getColumn(5).setPreferredWidth(50);
		columnModel.getColumn(6).setPreferredWidth(100);
	}
	
	private void showAllMovies(JTable table, ArrayList<Movie> movies) {
		DefaultTableModel model=new DefaultTableModel(); 
		String[] columnsName=new String[4];
		columnsName[0]="Id";
		columnsName[1]="Name";
		columnsName[2]="Genre";
		columnsName[3]="Duration";
		model.setColumnIdentifiers(columnsName);
		Object[] rowData=new Object[7];
		for(int i=0;i<movies.size();i++) {
			rowData[0]=movies.get(i).getId();
			rowData[1]=movies.get(i).getMovieName();
			rowData[2]=movies.get(i).getGenre();
			rowData[3]=movies.get(i).getDuration();
			model.addRow(rowData);
			}
		table.setModel(model);
		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(80);
		columnModel.getColumn(1).setPreferredWidth(150);
		columnModel.getColumn(2).setPreferredWidth(120);
		columnModel.getColumn(3).setPreferredWidth(120);
	}
	
	private void showAllProjectionsCRUD(JTable table, ArrayList<Projection> projections) {
		DefaultTableModel model=new DefaultTableModel(); 
		String[] columnsName=new String[5];
		columnsName[0]="Id";
		columnsName[1]="Date&Time";
		columnsName[2]="Movie";
		columnsName[3]="Cinema";
		columnsName[4]="Active";
		model.setColumnIdentifiers(columnsName);
		Object[] rowData=new Object[5];
		for(int i=0;i<projections.size();i++) {
			rowData[0]=projections.get(i).getId();
			rowData[1]=projections.get(i).getDate_time();
			rowData[2]=projections.get(i).getName_movie();
			rowData[3]=projections.get(i).getName_cinema();
			rowData[4]=projections.get(i).isActive();
			model.addRow(rowData);
			}
		table.setModel(model);
		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(140);
		columnModel.getColumn(2).setPreferredWidth(150);
		columnModel.getColumn(3).setPreferredWidth(150);
		columnModel.getColumn(4).setPreferredWidth(150);
	}
	
	private void showAllProjectionsFront(JTable table, ArrayList<Projection> projections) {
		DefaultTableModel model=new DefaultTableModel(); 
		String[] columnsName=new String[4];
		columnsName[0]="Id";
		columnsName[1]="Date&Time";
		columnsName[2]="Movie";
		columnsName[3]="Cinema";
		model.setColumnIdentifiers(columnsName);
		Object[] rowData=new Object[4];
		for(int i=0;i<projections.size();i++) {
			rowData[0]=projections.get(i).getId();
			rowData[1]=projections.get(i).getDate_time();
			rowData[2]=projections.get(i).getName_movie();
			rowData[3]=projections.get(i).getName_cinema();
			model.addRow(rowData);
			}
		table.setModel(model);
		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(140);
		columnModel.getColumn(2).setPreferredWidth(150);
		columnModel.getColumn(3).setPreferredWidth(150);
	}
	
	private void showAllCinemas(JTable table, ArrayList<Cinema> cinemas) {
		DefaultTableModel model=new DefaultTableModel(); 
		String[] columnsName=new String[4];
		columnsName[0]="Id";
		columnsName[1]="Name";
		columnsName[2]="Location";
		columnsName[3]="City";
		model.setColumnIdentifiers(columnsName);
		Object[] rowData=new Object[4];
		for(int i=0;i<cinemas.size();i++) {
			rowData[0]=cinemas.get(i).getId();
			rowData[1]=cinemas.get(i).getName();
			rowData[2]=cinemas.get(i).getLocation();
			rowData[3]=cinemas.get(i).getCity();
			model.addRow(rowData);
			}
		table.setModel(model);
		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(140);
		columnModel.getColumn(2).setPreferredWidth(150);
		columnModel.getColumn(3).setPreferredWidth(150);
	}
	
	public static boolean valEmail(String email) {
		String emailRegex="^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
		Pattern p= Pattern.compile(emailRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcher= p.matcher(email);
		return matcher.find();
	}
	
	public void clearFieldsUpdate() {
		textFieldNameUpdate.setText("");
	    textFieldSurnameUpdate.setText("");
	    textFieldAddressUpdate.setText("");
	    textFieldEmailUpdate.setText("");
	    textFieldManagerIdUpdate.setText("");
	    textFieldSalaryUpdate.setText("");
	}
	}

